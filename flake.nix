# Copyright (C) 2022 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

{
  description = "Nix Flake configuration for SAT-Solver";

  inputs.nixpkgs.url = github:NixOS/nixpkgs/nixpkgs-unstable;
  inputs.flake-utils.url = github:numtide/flake-utils;

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system: {
      defaultPackage = self.packages.${system}.default; # Nix < 2.7
      packages.default = # Nix 2.7
        let
          pkgs = import nixpkgs { inherit system; };
        in
          pkgs.stdenv.mkDerivation {
            name = "sat-solver";
            src = self;
            nativeBuildInputs = [ pkgs.cmake ];
            dontConfigure = true; # Makefile calls cmake
            installPhase =
              ''
                mkdir -p $out/bin
                cp build/release/sat-solver build/cputime/cputime $out/bin/
              '';
          };
    });
}
