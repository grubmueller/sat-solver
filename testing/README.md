---
title: "How to use the testing framework"
...

# Basics

In general the testing framework was split into two parts.
You can choose whether to run the tests on one computer with GNU parallel,
or you can use the scripts provided to run the tests distrubuted on the CIP-SLURM-Cluster
of the RGB. See the corresponding sections for further details.

# Common preperations

To combat a nasty error in the SLURM-Configuration of the RGB (changing the directory in the slurm tasks does not work; it always fails)
one must first generate two files `libSAT.sat.files` and `libSAT.unsat.files` each containing the full path to the test data.

This can easily be done by executing the bash script `create_file_list.sh`.
If all went well, one should see two green OK-texts in the console.

Before running any of these testscripts one should compile the sat-solver in release mode as well as debugging mode. cputime should also be build.

```bash
make cputime debug release drat-trim -j4
```

# Running the actual tests

* Warning: These files must be run in the testing directory, not the slurm or parallel subdir. These subdirs are only for a better order inside of the testing directory!

## parallel (on one machine; i.e. the gem-named computers in the CIP-Pool via `ssh` like `topas`)

* Log onto the computer you want to run the tests on and extract the project directory.
* Execute `make debug` to build the debug version of the program.
* Chdir into the `testing` directory.
* Execute `./create_file_list.sh`
* Execute `parallel_unbounded.sh` or `verify_solver.sh` (with included correctness testing via cadical as reference).

This will create the corresponding csv-file containing the cpu-runtimes of the test dataset.

## SLURM

* Log onto any of the CIP-computers via `ssh` or `rdp`
* Extract the program directory anywhere in your home directory (not locally on the disk)
* Execute `make debug` to build the debug version of the program.
* Chdir into the `testing` directory.
* Execute `./create_file_list.sh`
* Execute `sbatch slurm_THEFILEYOUWANT.sh` or `sbatch slurm_THEFILEYOUWANT.sh`

This will create the corresponding csv-file containing the cpu-runtimes of the test dataset, but distributed on the CIP-PCs


# Generating the Cactusplot

* This is a manual process. Use the R scripts provided and modify them to your specific needs. There is no generell method.


# WARNING


* There are also versions of each script, which runs the program with a timeout of one minute (real time). Just use the appropriate scripts like above.
* The one_minute timeout script is no longer up to date. Use the verify_solver scripts instead. These will test the solution regarding correctness as well.
* File names may not reflect, what happens in them. Always check the file contents!
