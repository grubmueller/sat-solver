#! /bin/bash
# Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
# 
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
filepath=$(date +%F-%M)
echo "c, duration, command" > $filepath.csv

parallel --jobs 3 -a $(pwd)/libSAT.sat.files --bar --progress --joblog $(pwd)/joblog_unbounded-cadical-$filepath.sat.txt --delay 0.2 "$(pwd)/verify_solution.sh {1} '{2},{3}' {4}" ">>" $filepath.csv ::: pl nopl ::: up noup ::: dlis dlcs slis slcs jw boehm

parallel --jobs 3 -a $(pwd)/libSAT.unsat.files --bar --progress --joblog $(pwd)/joblog_unbounded-cadical-$filepath.unsat.txt --delay 0.2 "$(pwd)/verify_solution.sh {1} '{2},{3}' {4}" ">>" $filepath.csv ::: pl nopl ::: up noup ::: dlis dlcs slis slcs jw boehm
