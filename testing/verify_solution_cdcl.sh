#! /bin/bash
# Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
# 
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Print output
tmpdir=$(mktemp -d)
filename=$tmpdir/$(basename $1)-$2,$3,$4,$5,$6,$7,$8
#set -x

timeout 2m $(pwd)/../build/cputime/cputime $(pwd)/../build/release/sat-solver -i $1 -o $filename.sol -p $filename.proof -c $2,$5,$3 -b $4 -s $6 -r $7 -x $8

retv=$?


# Exit if we timed out
if [[ $retv == 124  ]]
then
    echo "Command timed out" > /dev/stderr
    exit 124
fi



if [[ $retv == 10 ]]
then
  cadical $1 -s $filename.sol > /dev/null
  retvc=$?
  
  if [[ $retvc != 10 ]]
  then
    cp $filename.sol $(pwd)/log/$(basename $filename).sol.error
    echo "An error (s:sat,c:o) was found in $1. See errorlog in $(pwd)/log/$(basename $filename).sol.error" > /dev/stderr
  fi
else

if [[ $retv == 20 ]]
then
  $(pwd)/../build/drat-trim/drat-trim $1 $filename.proof > $filename.proof.out
  retvc=$?
  
  if [[ $retvc != 0 ]]
  then
    cp $filename.proof.out $(pwd)/log/$(basename $filename).proof.error
    echo "An error (s:unsat,c:o) was found in $1. See verifierlog in $(pwd)/log/$(basename $filename).proof.error" > /dev/stderr
  fi
    
else
    echo "Our solver raised an error." > /dev/stderr
    $(pwd)/../build/release/sat-solver -i $1 -o $filename.sol -p $filename.proof -c $2,$5,$3 -b $4 -s $6 -r $7 -x $8 > /dev/stderr
fi
fi

rm -f $tmpdir/*
rmdir $tmpdir
