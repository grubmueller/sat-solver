#! /bin/bash
#
#SBATCH --job-name=sat-solver_test_verify
#SBATCH --partition=All
#SBATCH --ntasks=6

# Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
# 
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
filepath=$(date +%F-%H:%M)
mkdir -p $(pwd)/log $(pwd)/log/csvs/$filepath/
#echo "c, duration, command" > $(pwd)/log/csvs/$filepath.csv

parallel --jobs 16 --delay 0.1 "echo 'c, duration, command'" ">>" $(pwd)/log/csvs/$filepath/{2},{5},{1},{3},{4},{6},{7}.csv ::: nopl  ::: vmtf vsids ::: nosub ::: ps nops ::: relsat 1uip decision ::: geometric luby io fixed=32 none ::: simple bounded 

parallel --jobs 4 --bar -a $(pwd)/libSAT.files --joblog $(pwd)/log/joblog_cdcl_verify-$filepath.txt --delay 0.1 "$(pwd)/verify_solution_cdcl.sh {1} {2} {4} {3} {5} {6} {7} {8}" ">>" $(pwd)/log/csvs/$filepath/{3},{6},{2},{4},{5},{7},{8}.csv ::: nopl  ::: vmtf vsids ::: nosub ::: ps nops ::: relsat 1uip decision ::: geometric luby io fixed=32 none ::: simple 
