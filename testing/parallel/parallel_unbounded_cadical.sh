#!/bin/bash

echo "c, duration, command" > $(pwd)/unb-cadical-unop.csv
parallel --jobs 2 -a $(pwd)/libSAT.sat.files --bar --progress --joblog $(pwd)/joblog_unbounded-cadical-$(date -Iminutes).sat.txt --delay 0.2 "$(pwd)/../build/cputime/cputime cadical -q -n --plain -P0 {}" ">>" $(pwd)/unb-cadical-unop.csv &

parallel --jobs 2 -a $(pwd)/libSAT.unsat.files  --bar --progress --joblog $(pwd)/joblog_unbounded-$(date -Iminutes).unsat.txt --delay 0.2 "$(pwd)/../build/cputime/cputime cadical -q -n --plain -P0 {}" ">>" $(pwd)/unb-cadical-unop.csv
