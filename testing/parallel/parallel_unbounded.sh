#!/bin/bash
csvname=$(pwd)/unb-$(date -Iminutes).csv

echo "c, duration, dimacs, heuristic, pure_lit, unit_prop, subsumed" > $csvname

parallel --jobs 6 -a $(pwd)/libSAT.sat.files --bar --progress --joblog $(pwd)/joblog_unbounded-$(date -Iminutes).sat.txt --delay 0.2 "bash $(pwd)/no_timeout.sh $(pwd)/../build/debug/sat-solver {} 10 $csvname " 

parallel --jobs 6 -a $(pwd)/libSAT.unsat.files  --bar --progress --joblog $(pwd)/joblog_unbounded-$(date -Iminutes).unsat.txt --delay 0.2 "bash $(pwd)/no_timeout.sh $(pwd)/../build/debug/sat-solver {} 20  $csvname " 
