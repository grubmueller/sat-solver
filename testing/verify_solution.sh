#! /bin/bash
# Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
# 
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Print output
tmpdir=$(mktemp -d)
filename=$tmpdir/$(basename $1)-$2-$3
#set -x

$(pwd)/../build/cputime/cputime $(pwd)/../build/release/sat-solver -i $1 -o $filename.sol  -c $2 -b $3

retv=$?



if [[ $retv == 10 ]]
then
  cadical $1 -s $filename.sol > /dev/null
  retvc=$?
  
  if [[ $retvc != 10 ]]
  then
    cp $filename.sol $(pwd)/$(basename $1).sol.error
    echo "An error (s:sat,c:o) was found in $1. See errorlog in $(pwd)/$(basename $1).sol.error" > /dev/stderr
  fi
else

if [[ $retv == 20 ]]
then
    cadical $1 > /dev/null
    retvc=$?
    
    if [[ $retvc != 20 ]]
    then
        cp $filename.sol $(pwd)/$(basename $1).sol.error
        echo "An error (s:usat,c:o) was found in $1. See errorlog in $(pwd)/$(basename $1).sol.error" > /dev/stderr
    fi
    
else
    echo "Our solver raised an error." > /dev/stderr
fi
fi

rm -f $filename.sol
rmdir $tmpdir
