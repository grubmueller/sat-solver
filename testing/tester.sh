#!/bin/sh

for file in unsat/*
do
    ../build/release/sat-solver -q ${@} ${file} > /dev/null
done

for file in sat/*
do
    ../build/release/sat-solver -q ${@} ${file} > /dev/null
done
