#!/bin/bash

#! /bin/bash

# $1 PATH TO EXECUTABLE
# $2 FILE_PATH OF CNF FILE
# $5 Additional Arguments
# $3 SAT=10 UNSAT=20
# $4 resultpath
# $(pwd)/unb-$(date -Iminutes).csv

/usr/bin/time --quiet $1 -e $5 -i $2 -z $4 

ret_code=$?

if [[ ($ret_code == 10 && $3 == 10) || ($ret_code == 20 && $3 == 20) ]]; then
  echo -e "[   \e[92mOK\e[39m    ]: $2 has correct return value.";
else
  echo -e "[  \e[91mERROR\e[39m  ]: $2 has the wrong return value. The return value was $ret_code, but should have been $3.";
fi


