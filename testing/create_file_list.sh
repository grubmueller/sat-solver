#! /bin/sh
if test -f "libSAT.sat.files"; then
  echo -e "[  \e[33mINFO\e[39m   ]: The libSAT.sat.files file list already exists. It will be deleted prior to recreation."
fi
if test -f "libSAT.unsat.files"; then
  echo -e "[  \e[33mINFO\e[39m   ]: The libSAT.unsat.files file list already exists. It will be deleted prior to recreation."
fi
if test -f "libSAT.files"; then
  echo -e "[  \e[33mINFO\e[39m   ]: The libSAT.unsat.files file list already exists. It will be deleted prior to recreation."
fi
rm -f libSAT*.files 

touch libSAT.sat.files
touch libSAT.unsat.files
touch libSAT.files

find "$(pwd)/sat" -type f > libSAT.sat.files
if [ $? != 0 ]; then
  echo -e "[  \e[91mERROR\e[39m  ]: An error occured, while generating the file list. Please confirm the correct generation."
else

  echo -e "[   \e[92mOK\e[39m    ]: The libSAT.sat.files file list was created."
fi
find "$(pwd)/unsat" -type f >> libSAT.unsat.files

if [ $? != 0 ]; then
  echo -e "[  \e[91mERROR\e[39m  ]: An error occured, while generating the file list. Please confirm the correct generation."
else

  echo -e "[   \e[92mOK\e[39m    ]: The libSAT.unsat.files file list was created."
fi
find "$(pwd)/unsat" -type f >> libSAT.files
find "$(pwd)/sat" -type f >> libSAT.files
if [ $? != 0 ]; then
  echo -e "[  \e[91mERROR\e[39m  ]: An error occured, while generating the file list. Please confirm the correct generation."
else

  echo -e "[   \e[92mOK\e[39m    ]: The libSAT.files file list was created."
fi
