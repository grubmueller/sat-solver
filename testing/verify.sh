#!/bin/sh
[ $(for i in testing/sat/*.cnf; do build/release/sat-solver $@ -qo solution $i;
    cadical -q -s solution $i | grep "^s "; done | wc -l) = 132 ] &&
[ $(for i in testing/unsat/*.cnf; do
build/release/sat-solver $@ -qp - $i | build/drat-trim/drat-trim $i | grep "s VERIFIED"; done | wc -l) = 40 ]
