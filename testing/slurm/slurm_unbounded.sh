#! /bin/bash
#
#SBATCH --job-name=sat-solver_test_unbounded
#SBATCH --partition=All
#SBATCH --ntasks=6

csvname=$(pwd)/unb-$(date -Iminutes).csv


echo "c, duration, dimacs, heuristic, pure_lit, unit_prop, subsumed" > $csvname

parallel --jobs 3 -a $(pwd)/libSAT.sat.files --joblog $(pwd)/joblog_unbounded-$(date -Iminutes).sat.txt --delay 0.2 "srun -N 1 -n 1 -c 1 --cpu-freq=2000000 --exclusive bash $(pwd)/no_timeout.sh $(pwd)/../build/debug/sat-solver {} 10 $csvname " &

parallel --jobs 3 -a $(pwd)/libSAT.unsat.files  --joblog $(pwd)/joblog_unbounded-$(date -Iminutes).unsat.txt --delay 0.2 "srun -N 1 -n 1 -c 1 --cpu-freq=2000000 --exclusive bash $(pwd)/no_timeout.sh $(pwd)/../build/debug/sat-solver {} 20 $csvname " 
