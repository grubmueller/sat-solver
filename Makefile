# Copyright (C) 2020-2022 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Command-line flag to silence nested $(MAKE)
$(VERBOSE)MAKESILENT = -s

.PHONY: default all debug release cputime drat-trim clean

default: release cputime

all: debug release cputime drat-trim

debug: build/debug
	$(MAKE) $(MAKESILENT) -C build/debug

release: build/release
	$(MAKE) $(MAKESILENT) -C build/release

cputime: build/cputime
	$(MAKE) $(MAKESILENT) -C build/cputime

drat-trim: build/drat-trim/drat-trim

clean:
	$(RM) -r build
	$(RM) compile_commands.json

compile_commands.json: build/debug
	ln -fs build/debug/compile_commands.json

build/debug:
	cmake -DCMAKE_BUILD_TYPE=Debug -S src -B build/debug

build/release:
	cmake -DCMAKE_BUILD_TYPE=Release -S src -B build/release

build/cputime:
	cmake -DCMAKE_BUILD_TYPE=Release -S src/cputime -B build/cputime

build/drat-trim:
	mkdir -p build/drat-trim
	wget https://www.cs.utexas.edu/~marijn/drat-trim/drat-trim.c -O build/drat-trim/drat-trim.c

build/drat-trim/drat-trim: build/drat-trim
	$(CC) -O2 -o build/drat-trim/drat-trim build/drat-trim/drat-trim.c
