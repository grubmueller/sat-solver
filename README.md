# SAT-Solver

## Introduction

This piece of software contains satisfiability (SAT) solvers of type CDCL and DPLL. They were created as part of the lecture *SAT Solving* of the winter term 2020/2021 at LMU Munich.

It reads files in the DIMACS CNF format and outputs either a satisfying solution or the fact that the given instance is unsatisfiable.

Optionally in CDCL mode, it can output a DRAT proof verifyable e. g. by [`drat-trim`](https://www.cs.utexas.edu/~marijn/drat-trim/).

The program provides only a CLI and runs on Linux and MacOS (in fact it should run on any POSIX compatible system).
Windows is currently not supported.

The `GitLab` repository is located at [https://gitlab2.cip.ifi.lmu.de/grubmueller/sat-solver](https://gitlab2.cip.ifi.lmu.de/grubmueller/sat-solver).

## Design

`SAT-Solver` is designed according to the UML diagram specified in the file `design.eps`.

## License

This project is licensed under the terms of `GPLv3+`. See `LICENSE` for more details.

## Building SAT-Solver

### Preparation

Make sure you have a working `C++` compiler installed, e. g. `g++` or `clang++`. The minimal required standard is `C++17`. To build the project it is essential to have `cmake` version `3.10` or newer properly configured on your system.

### Download

If you do not already have the project files, you can download them by using `git clone` via `ssh` or `https`:

```sh
$ git clone git@gitlab2.cip.ifi.lmu.de:grubmueller/tents-solver.git
```

or

```sh
$ git clone https://gitlab2.cip.ifi.lmu.de/grubmueller/tents-solver.git
```

Alternatively, download them directly from the [`GitLab` website](https://gitlab2.cip.ifi.lmu.de/grubmueller/tents-solver) by clicking on the *download* button.

### Compilation

With `sat-solver` as your current working directory, simply execute

```sh
$ make release
```

`SAT-Solver` will then be compiled to the executable `build/release/sat-solver`. Note that due to heavy use of compile time optimizations, depending on your hardware this could take several minutes.

## Using SAT-Solver

To use `SAT-Solver` execute the following command:

```sh
$ /path/to/folder/sat-solver [options] [INPUT] [OUTPUT]
```

where `/path/to/folder` is the path to the folder where the executable `sat-solver` is located. For a detailed list of options see the following table:

Option           | Description
-----------------|------------
`-h`             | Display the help text.
`-l`             | Diplay the license of this program including the authors.
`-q`             | Reduce solver output verbosity.
`-f`             | Ignore invalid DIMACS header.
`-i <path>`      | Specify the path from where to load the DIMACS file. This overrides the `INPUT` argument. [default: -]
`-o <path>`      | Write the solution to the file at the specified path. This overrides the `OUTPUT` argument. [default: -] <br> Printing of solver settings and statistics is not affected by this option.
`-p <path>`      | Output DRAT proof to `<path>`.  (CDCL only)
`-t <type>`      | Use the solver type `<type>`.  [default: `cdcl`] [options: `cdcl, dpll`]. <br> Solver type is automatically inferred from the chosen branching heuristic.
`-b <heuristic>` | Use the specified branching heuristic. [default: CDCL: `vmtf`, DPLL: `dlis`]
`-c <config>`    | Use the specified comma-separated list as configuration.
`-s <scheme>`    | Use the specified learning scheme.  (CDCL only) [default: `1uip`]
`-x <strategy>`  | Use the specified deletion strategy.  (CDCL only) [default: `simple`]
`-r <policy>`    | Use the specified restart policy.  (CDCL only) [default: `io`]

### Options `-i` and `-o`

Please note, that instead of

```sh
$ /path/to/folder/sat-solver [options] INPUT OUTPUT
```

you can also use

```sh
$ /path/to/folder/sat-solver [options] -i INPUT -o OUTPUT
```

or any combination of just `-i INPUT`, just `-o OUTPUT` or both.

To read from standard input or write to standard output use `-` (the dash character at `ASCII x2D`).

### Branching heuristics

The following branching heuristics are currently implemented:

Shorthand | Heuristic
----------|----------
`vmtf`    | Variable move to front heuristic (CDCL) [CDCL default]
`vsids`   | Variable state independent decaying sum (CDCL)
`slis`    | Static largest individual sum (DPLL)
`slcs`    | Static largest combined sum (DPLL)
`dlis`    | Dynamic largest individual sum (DPLL) [DPLL default]
`dlcs`    | Dynamic largest combined sum (DPLL)
`boehm`   | Böhm's heuristic (DPLL)
`jw`      | Jeroslaw-Wang's heuristic (DPLL)

The solver type is infered from the branching heuristic if it is not specified, meaning

```sh
$ path/to/folder/sat-solver INPUT -b dlcs
```

is equivalent to

```sh
$ path/to/folder/sat-solver INPUT -t dpll -b dlcs
```

### Configuration suboptions

The following configuration suboptions are supported:

Solver Suboption | Description
-----------------|------------
`ps`             | Phase saving (CDCL only) [default: `off`]
`up`             | Unit propagation. (DPLL only) [default: `on`]
`pl`             | Pure literal elimination. (DPLL only) [default: `on`]

Preprocessing Suboption | Description
------------------------|------------
`sub`                   | Subsumed clause elimination [default: `on`]
`bce`                   | Blocked clause elimination [default: `on`]
`pl`                    | Pure literal elimination [default: `on`] <br> This setting is only effective if `BCE` is deactivated.
`holes`                 | Variable hole elimination [default: `on`]

To (de-) activate suboptions use the `-c` flag with a comma separated list of suboption configurations like the following:

```sh
-c <CONFIG1>,<CONFIG2>,...,<CONFIGN>
```

To _activate_ a suboption use one of the following notations:

```sh
<SUBOPT>
```

or

```sh
<SUBOPT>=<VALUE>
```

with `<VALUE>` one of `yes`,  `true`, `1`, `y` and `t`.

To _deactivate_ a suboption use on of the following notations:

```sh
no<SUBOPT>
```

or

```sh
<SUBOPT>=<VALUE>
```

with `<VALUE>` one of `no`, `false`, `0`, `n` and `f`.

#### Example

```sh
$ /path/to/folder/sat-solver INPUT -c ps,sub=true,pl=1,nobce,holes=f
```

starts the solver with phase saving, subsumed clause elimination and pure literal elimination activated, and blocked clause elimination and variable hole elimination deactivated

### Learning Schemes (CDCL only):

The following learning schemes are supported:

`1uip` [default]

`relsat`

`decision`

### Clause Deletion Strategies (CDCL only):

The following clause deletion strategies are supported:

Deletion Strategy | Description
------------------|------------
`simple=<C>`      | Use the simple deletion strategy with capacity `<C>` [positive integer].
`simple`          | Use the simple deletion strategy with default configuration [=5000].  [default]
`bounded=<K>,<M>` | Use the bounded deletion strategy with small clause threshold `<K>` [positive integer] <br> and unassignment threshold `<M>` [positive integer <= `<K>`].
`bounded`         | Use the bounded deletion strategy with default configuration [=26,4].

#### Simple Clause Deletion Strategy

Try to keep at most `<C>` clauses by removing the oldest clause that is not a reason for any assignment if the threshold is reached while learning a new clause. If the solver is unable to delete a clause, the data structure is enlarged.

### Restart Policies (CDCL only):

The following deletion strategies are supported:

Restart Policies    | Description
--------------------|------------
`io=<C>,<F>`        | Use the inner/outer restart policy with base inner interval `<C>` [positive integer] <br> and factor `<F>` [floating point number > 1].
`io`                | Use the inner/outer restart policy with default configuration [=256,1.95].  [default]
`geometric=<C>,<F>` | Use the geometric restart policy with interval length `<C>` [positive integer] <br> and factor `<F>` [floating point number > 1].
`geometric`         | Use the geometric restart policy with default configuration [=512,1.1].
`luby=<C>`          | Use the Luby restart policy with base interval `<C>` [positive interger].
`luby`              | Use the Luby restart policy with default configuration [=32].
`fixed=<C>`         | Use the fixed restart policy with interval length `<C>` [positive integer].
`fixed`             | Use the fixed restart policy with default configuration [=64].
`none`              | Do not use restarts.

#### Inner/Outer restart policy

After every restart, multiply the (inner) restart threshold (the number of conflicts until the next restart) by the factor `<F>`. When this _inner_ threshold reaches an _outer_ threshold, reset the inner restart threshold to `<C>` and multiply the outer threshold by `<F>`. This ensures an ever increasing restart threshold like with the geometric restart policy, but provides phases of short restart intervals even after long execution times much like the Luby restart policy.

### Configuration examples:

Some examples for configuration of all command line options:

1. CDCL Solver using VMTF heuristic, Bounded deletion strategy with values k=5 and m=4, Luby restart policy, RelSAT learning scheme and deactivated BCE:

    ```sh
    $ path/to/folder/sat-solver -i INPUT -t cdcl -b vmtf -x bounded=5,4 -r luby -s 1uip -c bce=n
    ```

    As the order is irrelevant, the following command will produce an equivalent result:

    ```sh
    $ path/to/folder/sat-solver  -s 1uip -t cdcl -c bce=n  -i INPUT -r luby -b vmtf -x bounded=5,4
    ```

2. CDCL Solver using VSIDS heuristic, Luby restart policy:

    ```sh
    $ path/to/folder/sat-solver INPUT -b vsids -r luby
    ```

    This starts the solver as a CDCL Solver and sets the learning scheme to 1UIP, default configurations and Simple with c=5000 as deletion strategy

3. Not specifying anything/Using the default values:

    ```sh
    $ path/to/folder/sat-solver INPUT
    ```

    Only use default values: solver type CDCL, learning scheme 1UIP, branching heuristic VMTF, deletion strategy Simple with c=5000, restart policy inner/outer with c=256 and f=1.95, no phase saving and all preprocessing options activated.

4. Default DPLL solver:

    ```sh
    $ path/to/folder/sat-solver INPUT -t dpll
    ```

    This infers all defaults for DPLL: branching heuristic DLIS and activated UP as well as PL. Also activates all preprocessing options.

### Output behaviour

By default the solver prints preferences and various statistics to standard output as detailed below:

1. Print solver settings, the solution and statistics to standard output:

    ```sh
    $ /path/to/folder/sat-solver INPUT
    ```

2. Only print the solution to standard output.

    ```sh
    $ /path/to/folder/sat-solver -q INPUT
    ```

3. Print solver settings, the solution and statistics to standard output. Write the solution to `OUTPUT`:

    ```sh
    $ /path/to/folder/sat-solver INPUT OUTPUT
    ```

    In case you also want to write solver settings and statistics to `OUTPUT`, consider using pipes or redirects in combination with the first case. E. g. execute

    ```sh
    $ /path/to/folder/sat-solver INPUT > OUTPUT
    ```

4. Do not print anything to standard output. Write the solution to `OUTPUT`.

    ```sh
    $ /path/to/folder/sat-solver -q INPUT OUTPUT
    ```
