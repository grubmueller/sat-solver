/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream> // std::cout, std::cerr
#include <iomanip> // std::setfill, std::setw
#include <cstdlib> // EXIT_FAILURE
#include <cerrno> // errno
#include <cstring> // std::strerror
#include <unistd.h> // fork, dup2, close, execvp
#include <fcntl.h> // open
#include <sys/stat.h> // open
#include <sys/wait.h> // waitpid
#include <sys/resource.h> // getrusage


int main(int argc, char* argv[]) {
    if (argc < 2) { // No argument given
        std::cerr << "Missing argument\n";
        return EXIT_FAILURE;
    }

    pid_t pid = fork();

    switch (pid) {
    case -1: // error
        std::cerr << "Error forking: " << std::strerror(errno) << '\n';
        return EXIT_FAILURE;
    case 0: { // child
        int const devNull = open("/dev/null", O_WRONLY);

        if (devNull == -1) { // error
            std::cerr << "Error opening /dev/null for output redirection: "
                      << std::strerror(errno) << '\n';
            return EXIT_FAILURE;
        }

        // redirect standard output to /dev/null
        if (dup2(devNull, STDOUT_FILENO) == -1) {
            std::cerr << "Error redirecting standard output: " << std::strerror(errno) << '\n';
            return EXIT_FAILURE;
        }

        close(devNull);

        execvp(argv[1], argv+1); // execute the program

        // if execvp returns, an error has occured
        std::cerr << "Error executing the program: " << std::strerror(errno) << '\n';
        return EXIT_FAILURE;
    }
    default: { // parent
        int wstatus;
        if (waitpid(pid, &wstatus, 0) == -1) { // wait for child
            std::cerr << "Error waiting for the program: " << std::strerror(errno) << '\n';
            return EXIT_FAILURE;
        }

        if (!WIFEXITED(wstatus)) {
            std::cerr << "Program terminated abnormal\n";
            return EXIT_FAILURE;
        }

        struct rusage usage;
        if (getrusage(RUSAGE_CHILDREN, &usage) == -1) { // get usage of child
            std::cerr << "Error getting usage info of the program: "
                      << std::strerror(errno) << '\n';
            return EXIT_FAILURE;
        }

        // add system and user time to cputime
        struct timeval cputime;
        cputime.tv_sec = usage.ru_stime.tv_sec + usage.ru_utime.tv_sec;
        cputime.tv_usec = usage.ru_stime.tv_usec + usage.ru_utime.tv_usec;

        if (cputime.tv_usec >= 1'000'000) { // handle overflow
            ++cputime.tv_sec;
            cputime.tv_usec -= 1'000'000;
        }

        // output cputime
        std::cout << "c, " << cputime.tv_sec << '.'
                  << std::setfill('0') << std::setw(6) << cputime.tv_usec;

        // output arguments
        std::cout << ", \'" << argv[1];

        for (int i = 2; i < argc; ++i)
            std::cout << ' ' << argv[i];

        std::cout << "\'";

        // output memory
        std::cout << ", " << usage.ru_maxrss;

        std::cout << std::endl;

        // return the exit code of the program
        return WEXITSTATUS(wstatus);
    }
    }
}
