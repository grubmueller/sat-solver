/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SOLVER_DPLL_TOOLS_HPP
#define SOLVER_DPLL_TOOLS_HPP

#include "solver_tools.hpp"
#include "config.hpp" // Solver_Type::DPLL


inline constexpr Solver_Type DPLL = Solver_Type::DPLL;

/* Assignent<DPLL>
 * This is a struct deriving from `Assignment_Base` and additionally containing if the assignment
 * was forced or branching.
 */
template <>
struct Assignment<DPLL> : public Assignment_Base {
    bool forced;

    constexpr Assignment(Literal const lit, bool const forced) noexcept :
        Assignment_Base(lit), forced(forced) {}
};

/* Variable<DPLL>
 * This is a struct deriving from `Variable_Base` and additionally containing the occurences of the
 * variable and the number of active occurences.
 */
template <>
struct Variable<DPLL> : public Variable_Base<DPLL> {
    Occurences<DPLL> occ [2] {}; // occ[false]=neg_occ, occ[true]=pos_occ
    Formula::size_type act_occ [2] {}; // act_occ[false]=act_neg_occ, act_occ[true]=act_pos_occ
};

/* Clause<DPLL>
 * This is a struct deriving from `Clause_Base` and additionally containing the number of active
 * literals (i.e. the current clause width) and a `var_t` which says if the clause is
 * unsatisfied/active or satisfied/inactive and if yes by which variable.
 */
template <>
struct Clause<DPLL> : public Clause_Base {
    var_t sat_by {};
    width_t active_lits;

    // precondition: `origin` is neither empty nor a tautology
    explicit Clause(Formula::Clause&& origin) noexcept :
        Clause_Base(std::move(origin)),
        active_lits(static_cast<width_t>(lits.size()))
    {}

    // returns if the clause is currently satisfied
    bool sat() const noexcept {
        return sat_by;
    }

    // returns if the clause is currently empty
    bool empty() const noexcept {
        return active_lits == 0;
    }

    // returns if the clause is currently a unit clause
    bool unit() const noexcept {
        return active_lits == 1;
    }
};

/* Units<DPLL>
 * This is a class deriving from `Units_Base` with `Literal` as `T`.
 */
template <>
class Units<DPLL> : public Units_Base<Literal, DPLL> {
    friend Units_Base<Literal, DPLL>;

public:
    explicit Units(var_t const var_count) :
        Units_Base(var_count) {}

private:
    static Literal t2lit(Literal const l) noexcept {
        return l; // identity
    }

    static bool valid(Literal const l) noexcept {
        return static_cast<bool>(l);
    }
};

/* Pure_lits
 * This is a data structure for pure literals, needed for pure literal elimination.
 * It is intended for the DPLL-Solver.
 */
class Pure_lits {
public:
    // returns true if the data structure is empty, false otherwise
    bool empty() const noexcept {
        return li_.empty();
    }

    /* Get the next pure literal
     * this might return a literal that has already been set
     *
     * returns the next pure literal or an invalid literal if the data structure is empty
     */
    Literal top() const noexcept {
        return li_.empty() ? Literal{} : li_.front();
    }

    /* Remove the top element from the data structure
     *
     * precondition: the data structure is not empty
     */
    void pop() {
        solver_assert(!empty()); // assert precondition

        li_.pop_front();
    }

    /* Push a new pure literal to the data structure
     *
     * precondition: `l` is valid
     */
    Pure_lits& push(Literal const l) {
        solver_assert(l); // assert precondition
        solver_log("new pure literal ", l);

        li_.push_back(l);
        return *this;
    }

    // clear the data structure
    void clear() noexcept {
        li_.clear();
    }

private:
    std::deque<Literal> li_ {};
};


#endif // SOLVER_DPLL_TOOLS_HPP
