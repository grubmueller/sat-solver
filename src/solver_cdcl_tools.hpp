/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SOLVER_CDCL_TOOLS_HPP
#define SOLVER_CDCL_TOOLS_HPP

#include "solver_tools.hpp"
#include "config.hpp" // Solver_Type::CDCL


inline constexpr Solver_Type CDCL = Solver_Type::CDCL;

// branching depth type
using brade_t = var_t;

/* Assignment<CDCL>
 * This is a struct deriving from `Assignment_Base` and additionally containing the branching depth
 * and the reason of the assignment.
 */
template <>
struct Assignment<CDCL> : public Assignment_Base {
    brade_t brade;
    Clause<CDCL> const* reason;

    // precondition: `lit` is valid, `reason` is valid and not a null pointer
    constexpr Assignment(Literal const lit, var_t const brade, Clause<CDCL> const* const reason) noexcept :
        Assignment_Base(lit), brade(brade), reason(reason) {}
};

/* Variable<CDCL>
 * This is a struct deriving from `Variable_Base` and additionally containing the watched occurences
 * of the variable.
 */
template <>
struct Variable<CDCL> : public Variable_Base<CDCL> {
    Occurences<CDCL> wocc [2] {}; // wocc[false]=neg_watched_occ, wocc[true]=pos_watched_occ
};

/* Clause<CDCL>
 * This is a struct deriving from `Clause_Base` and additionally containing two watched literals.
 */
template <>
struct Clause<CDCL> : public Clause_Base {
    Literal wali [2];

    // precondition: `origin` is neither empty nor a tautology
    explicit Clause(Formula::Clause&& origin) noexcept :
        Clause_Base(std::move(origin)), wali{lits.front(), lits.back()} {}

    /* preconditions:
     * * all literals in `lits` are valid
     * * `lits` is sorted and does not contain duplicates
     * * `lits` is neither empty nor a tautology
     * * `wali1` and `wali2` are different (except if `lits.size() == 1`) elements of `lits`
     */
    Clause(Literals lits, Literal const wali1, Literal const wali2) noexcept :
        Clause_Base(std::move(lits)), wali{wali1, wali2} {}
};

/* Units<CDCL>
 * This is a class deriving from `Units_Base` with `Literal_Reason_Pair` as `T`.
 * For each literal, a pointer to the reason is stored.  It also provides the member function
 * `conflict()` which returns the conflict variable and the reasons of both conflict literals.
 */
template <>
class Units<CDCL> : public Units_Base<std::pair<Literal, Clause<CDCL> const*>, CDCL> {
public:
    using Conflict_Pair = std::pair<var_t, Clause<CDCL> const* [2]>;
    using Literal_Reason_Pair = std::pair<Literal, Clause<CDCL> const*>;

    explicit Units(var_t const var_count) :
        Units_Base(var_count) {}

    /* Get the conflict variable and the reasons of both conflict literals
     *
     * precondition: there is a conflict ahead
     */
    Conflict_Pair conflict() const {
        solver_assert(conflict_ahead()); // assert precondition

        auto const [conflict_lit, conflict_reason] = q_.back();
        Conflict_Pair ret;
        ret.first = conflict_lit.var;
        ret.second[conflict_lit.val] = conflict_reason;
        ret.second[!conflict_lit.val] = find_reason(conflict_lit.flip());
        return ret;
    }

    /* Push a new literal with its reason to the data structure
     *
     * preconditions:
     * * there is no conflict ahead
     * * `l` is valid
     * * `reason` is valid and not a null pointer
     */
    using Units_Base<Literal_Reason_Pair, CDCL>::push;

    Units& push(Literal const l, Clause<CDCL> const* const reason) {
        return push({l, reason});
    }

private:
    friend Units_Base<Literal_Reason_Pair, CDCL>;

    static Literal t2lit(Literal_Reason_Pair const lit_reason_pair) noexcept {
        return lit_reason_pair.first;
    }

    static bool valid(Literal_Reason_Pair const lit_reason_pair) noexcept {
        return lit_reason_pair.first && lit_reason_pair.second;
    }

    /* Get the reason of a literal that is stored in the data structure
     *
     * returns the reason of `l` or a null pointer if `l` is not in the data structure
     *
     * precondition: `l` is valid
     */
    Clause<CDCL> const* find_reason(Literal const l) const noexcept {
        solver_assert(l); // assert precondition

        for (auto const [lit,reason] : q_)
            if (lit == l)
                return reason;

        return nullptr;
    }
};

/* Phase_Saving
 * This is a class that stores literal values to implement phase saving between restarts
 */
class Phase_Saving {
public:
    explicit Phase_Saving(var_t const var_count) :
        stored_(var_count + 1), value_(var_count + 1) {}

    // flip literal in branching if it is stored differently
    Literal maybe_flip(Literal const lit) const {
        if (stored_[lit.var] && value_[lit.var] != lit.val) {
            solver_log("Phase Saving: flip ", lit);
            return lit.flip(); // == Literal{value_[lit.var], lit.var}
        } else {
            return lit;
        }
    }

    void store(Literal const lit) {
        stored_[lit.var] = true;
        value_[lit.var] = lit.val;
    }

private:
    std::vector<bool> stored_;
    std::vector<bool> value_;
};


/* Learn a conflict clause according to a learning scheme
 *
 * parameters:
 * * template parameter `Sch` is the learning scheme
 * * `conflict` is the conflict pair of the ahead conflict
 * * `brade` is the current branching depth
 * * `alpha` is the current assignment stack
 * * `vars` is the current variable data structure
 *
 * returns a new asserting conflict clause, which should be added to a strategy
 * the first watched literal is the (only) literal of maximal branching depth, i.e. `brade`
 * the second watched literal is a literal of second largest branching depth, i.e. assertion level
 * if the conflict clause is a unit clause, the second watched literal is the first watched literal
 *
 * defined in learning.cpp
 */
template <Scheme Sch>
Clause<CDCL> learn(Units<CDCL>::Conflict_Pair conflict, brade_t brade,
                   Assignments<CDCL> const& alpha, Variables<CDCL> const& vars);


#endif // SOLVER_CDCL_TOOLS_HPP
