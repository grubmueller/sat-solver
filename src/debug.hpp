/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DEBUG_HPP
#define DEBUG_HPP

/* Macros `solver_log` and `solver_assert`
 * Defines the macros and includes the headers `printing.hpp` and `cassert` only if `NDEBUG` is not
 * defined, otherwise defines them to a no-op
 */

#ifndef NDEBUG // if debug
# include "printing.hpp"
# include <cassert>
# define solver_log(...) solver_debug_output(__VA_ARGS__)
# define solver_assert(...) do {\
        (void) sizeof static_cast<bool>(__VA_ARGS__); /* trigger -Wparentheses */ \
        assert((__VA_ARGS__)); /* avoid problems with commas */ \
    } while (false)
#else // not debug
# define solver_log(...) ((void) 0) // no-op
# define solver_assert(...) ((void) 0) // no-op
#endif // NDEBUG

#endif // DEBUG_HPP
