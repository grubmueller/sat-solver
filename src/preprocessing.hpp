/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PREPROCESSING_HPP
#define PREPROCESSING_HPP

#include "formula.hpp"
#include "solution.hpp"
#include "statistics.hpp"

#include <deque>
#include <vector>
#include <utility> // std::pair

class Proof_Logger;


struct Preprocessor_Data {
    var_t original_var_count {}; // original number of variables
    std::deque<std::pair<Literal, Literals>> blocked {}; // blocked clauses (with literal)
    std::vector<var_t> holes_translation {}; // translation for hole elimination
    Statistics stats {}; // statistics of preprocessing
};

/* Preprocess the formula according to parameters.
 *
 * Possible techniques:
 * * subsumed clause elimination (`subsumed`)
 * * blocked clause elimination (`blocked`)
 * * pure literal elimination (`pure`, only possible if blocked clause elimination isn't active)
 * * hole elimination (`holes`)
 *
 * stores statistics and data for postprocessor into `pre_data`,
 * logs deleted and learned clauses to `pl` if `pl` is not a null pointer.
 *
 * precondition: `f` is not empty and does not contain an empty clause
 */
void preprocess(Formula& f, Preprocessor_Data& pre_data,
                bool subsumed, bool blocked, bool pure, bool holes,
                Proof_Logger* pl = nullptr);

/* Postprocess the solution (only needed in case of SATISFIABLE).
 * Postprocessing is needed for hole elimination and blocked clause elimination to generate
 * a satisfying assignment with the previously deleted clauses and renamed variables.
 *
 * precondition: `pre_data` was not modified between the call to `preprocess` and `postprocess`
 */
void postprocess(Solution& sol, Preprocessor_Data& pre_data);


#endif // PREPROCESSING_HPP
