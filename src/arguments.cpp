/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "arguments.hpp"
#include "printing.hpp"

#include <optional>
#include <string>
#include <iostream> // std::cout
#include <cstring> // std::strcmp
#include <cstdio> // std::sscanf
#include <unistd.h> // getopt
#include <stdlib.h> // getsubopt


struct arguments arguments {};

namespace {
void print_license() {
    std::cout << R"(License of SAT-Solver: GPLv3+
Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.)";
    std::cout << std::endl;
}

void print_help(char const* const program) {
    std::cout << "SAT-Solver"
#ifndef NDEBUG
              << " (debug version)"
#endif
              << ": Solver for DIMACS problems for the lecture SAT Solving at LMU Munich in the winter term 2020/2021\n"
              "For authors and license see " << program << R"( -l

Usage: )" << program << R"( [options] [INPUT] [OUTPUT]

Load the DIMACS file INPUT, solve the CNF and write the solution to OUTPUT.

Options:
-h                 Display this help text.
-l                 Diplay the license of this program including the authors.
-q                 Reduce solver output verbosity.)"
#ifndef NDEBUG
              << '\n' << "-d                 Print debug output."
#endif
              << R"(
-f                 Ignore invalid DIMACS header.
-i <path>          Specify the path from where to load the DIMACS file.  This overrides the INPUT argument.  [default: -]
-o <path>          Write the solution to the file at the specified path.  This overrides the OUTPUT argument.  [default: -]
                   Printing of solver settings and statistics is not affected by this option.
-p <path>          Output DRAT proof to <path>.  (CDCL only)
-t <type>          Use the solver type <type>.  [default: cdcl] [options: cdcl, dpll].
                   Solver type is automatically inferred from the chosen branching heuristic.
-b <heuristic>     Use the specified branching heuristic. [default: CDCL: vmtf, DPLL: dlis]
-c <config>        Use the specified comma-separated list as configuration.
-s <scheme>        Use the specified learning scheme.  (CDCL only) [default: 1uip]
-x <strategy>      Use the specified deletion strategy.  (CDCL only) [default: simple]
-r <policy>        Use the specified restart policy.  (CDCL only) [default: io]


Branching Heuristics:
vmtf               Variable move to front heuristic (CDCL) [CDCL default]
vsids              Variable state independent decaying sum (CDCL)
slis               Static largest individual sum (DPLL)
slcs               Static largest combined sum (DPLL)
dlis               Dynamic largest individual sum (DPLL) [DPLL default]
dlcs               Dynamic largest combined sum (DPLL)
boehm              Böhm's heuristic (DPLL)
jw                 Jeroslaw-Wang's heuristic (DPLL)


Configuration Suboptions:

Usage: -c <CONFIG1>,<CONFIG2>,...,<CONFIGN>

Activate suboption:     <SUBOPT> or <SUBOPT>=<VALUE> with <VALUE> one of yes,  true, 1, y, t
Deactivate suboption: no<SUBOPT> or <SUBOPT>=<VALUE> with <VALUE> one of  no, false, 0, n, f

Solver Suboptions:
ps                 Phase saving (CDCL only) [default: off]
up                 Unit propagation (DPLL only) [default: on]
pl                 Pure literal elimination (DPLL only) [default: on]

Preprocessing Suboptions:
sub                Subsumed clause elimination [default: on]
bce                Blocked clause elimination [default: on]
pl                 Pure literal elimination [default: on]
                   This setting is only effective if BCE is deactivated.
holes              Variable hole elimination [default: on]


Learning Schemes (CDCL only):
1uip               [default]
relsat
decision


Deletion Strategies (CDCL only):
simple=<C>         Use the simple deletion strategy with capacity <C> [positive integer].
                   Try to keep at most <C> clauses.
simple             Use the simple deletion strategy with default configuration [=5000].  [default]
bounded=<K>,<M>    Use the bounded deletion strategy with small clause threshold <K> [positive integer]
                   and unassignment threshold <M> [positive integer <= <K>].
bounded            Use the bounded deletion strategy with default configuration [=26,4].


Restart Policies (CDCL only):
io=<C>,<F>         Use the inner/outer restart policy with base inner interval <C> [positive integer]
                   and factor <F> [floating point number > 1].
io                 Use the inner/outer restart policy with default configuration [=256,1.95].  [default]
geometric=<C>,<F>  Use the geometric restart policy with interval length <C> [positive integer]
                   and factor <F> [floating point number > 1].
geometric          Use the geometric restart policy with default configuration [=512,1.1].
luby=<C>           Use the Luby restart policy with base interval <C> [positive interger].
luby               Use the Luby restart policy with default configuration [=32].
fixed=<C>          Use the fixed restart policy with interval length <C> [positive integer].
fixed              Use the fixed restart policy with default configuration [=64].
none               Do not use restarts.


To read from standard input or write to standard output use '-' (the dash character at ASCII x2D).


By default the solver prints preferences and various statistics to standard output as detailed below:

)" << program << R"( INPUT
    Print solver settings, the solution and statistics to standard output.

)" << program << R"( -q INPUT
    Only print the solution to standard output.

)" << program << R"( INPUT OUTPUT
    Print solver settings, the solution and statistics to standard output.
    Write the solution to OUTPUT.
    In case you also want to write solver settings and statistics to OUTPUT,
    consider using pipes or redirects in combination with the first case.

)" << program << R"( -q INPUT OUTPUT
    Do not print anything to standard output.
    Write the solution to OUTPUT.)" << std::endl;
}

/* Get the value of a configuration suboption
 *
 * returns `std::nullopt` if `txt` is an invalid configuration suboption value (error)
 */
inline std::optional<bool> subopt_bool_val(char const* const txt) {
    if (!txt || std::strcmp(txt, "yes") == 0 || std::strcmp(txt, "true") == 0
            || std::strcmp(txt, "1") == 0 || std::strcmp(txt, "y") == 0 || std::strcmp(txt, "t") == 0
       ) {
        return true;
    } else if (std::strcmp(txt, "no") == 0 || std::strcmp(txt, "false") == 0
            || std::strcmp(txt, "0") == 0 || std::strcmp(txt, "n") == 0 || std::strcmp(txt, "f") == 0
              ) {
        return false;
    } else {
        solver_error_output("invalid configuration suboption value '", txt, '\'');
        solver_error_output_additional("try '-h' for help");
        return std::nullopt;
    }
}

// check if option is `cmp` or `cmp=`, in the latter case returns a pointer to the value
inline std::optional<char const*> optval(char const* const optarg, char const* const cmp) noexcept {
    std::size_t const len = std::strlen(cmp);

    if (std::strncmp(optarg, cmp, len) != 0)
        return std::nullopt;
    else if (optarg[len] == '\0')
        return nullptr;
    else if (optarg[len] == '=')
        return optarg + len + 1;
    else
        return std::nullopt;
}
}

/* parse command line arguments to struct `arguments`
 *
 * return value:
 *   ARGERR     program should exit with EXIT_FAILURE (error occured)
 *   ARGEXIT    program should exit with EXIT_SUCCESS
 *   0          program should continue (start the solver)
 */
int parse_arguments(int argc, char* argv[]) {
    // in case the user specified the option (i/o/t/b), set to true
    bool input {}, output {}, type {}, heuristic {};

    // in case the user specified a solver-specific option, append it
    std::string cdcl_only {}, dpll_only {};

    opterr = 0; // we want to print out our own error messages

    constexpr char const* optstr = "hlq"
#ifndef NDEBUG // if debug
                                       "d"
#endif
                                         "fi:o:p:t:b:c:s:x:r:";

    for (int opt; (opt = getopt(argc, argv, optstr)) != -1;)
        switch (opt) {
        case 'h': // print help
            print_help(argv[0]);
            return ARGEXIT;
        case 'l': // print license
            print_license();
            return ARGEXIT;
        case 'q': // quiet
            arguments.quiet = true;
            break;
#ifndef NDEBUG // if debug
        case 'd': // debug logging
            arguments.logging = true;
            break;
#endif
        case 'f': // ignore invalid DIMACS header
            arguments.ignore_dimacs_header = true;
            break;
        case 'i': // input file
            input = true;
            arguments.input_file = std::strcmp(optarg, "-") == 0 ? nullptr : optarg;
            break;
        case 'o': // output file
            output = true;
            arguments.output_file = std::strcmp(optarg, "-") == 0 ? nullptr : optarg;
            break;
        case 'p': // output proof
            arguments.proof = true;
            arguments.proof_file = std::strcmp(optarg, "-") == 0 ? nullptr : optarg;
            break;
        case 't': // solver type
            type = true;

            if (std::strcmp(optarg, "cdcl") == 0) {
                arguments.solver_type = Solver_Type::CDCL;

                if (!heuristic) // set default heuristic
                    arguments.heuristic = Heuristic::VMTF;
            } else if (std::strcmp(optarg, "dpll") == 0) {
                arguments.solver_type = Solver_Type::DPLL;

                if (!heuristic) // set default heuristic
                    arguments.heuristic = Heuristic::DLIS;
            } else {
                solver_error_output("invalid solver type '", optarg, '\'');
                solver_error_output_additional("try '-h' for help");
                return ARGERR;
            }

            break;
        case 'b': // branching heuristic
            heuristic = true;

            if (std::strcmp(optarg, "vmtf") == 0) {
                arguments.heuristic = Heuristic::VMTF;
            } else if (std::strcmp(optarg, "vsids") == 0) {
                arguments.heuristic = Heuristic::VSIDS;
            } else if (std::strcmp(optarg, "slis") == 0) {
                arguments.heuristic = Heuristic::SLIS;
            } else if (std::strcmp(optarg, "slcs") == 0) {
                arguments.heuristic = Heuristic::SLCS;
            } else if (std::strcmp(optarg, "dlis") == 0) {
                arguments.heuristic = Heuristic::DLIS;
            } else if (std::strcmp(optarg, "dlcs") == 0) {
                arguments.heuristic = Heuristic::DLCS;
            } else if (std::strcmp(optarg, "boehm") == 0 || std::strcmp(optarg, "böhm") == 0) {
                arguments.heuristic = Heuristic::BOEHM;
            } else if (std::strcmp(optarg, "jw") == 0) {
                arguments.heuristic = Heuristic::JW;
            } else {
                solver_error_output("invalid branching heuristic '", optarg, '\'');
                solver_error_output_additional("try '-h' for help");
                return ARGERR;
            }

            if (!type) // infer solver type
                arguments.solver_type = type_from_heuristic(arguments.heuristic);

            break;
        case 'c':  // configuration
            for (char *value, *subopts = optarg; *subopts;) {
                constexpr char const* token[] = {
                    "ps", "nops", "up", "noup", "pl", "nopl",
                    "sub", "nosub", "bce", "nobce", "holes", "noholes"
                };

                switch (getsubopt(&subopts, const_cast<char* const*>(token), &value)) {
                case 0: // ps (phase saving)
                    cdcl_only += "-c ps";

                    if (value)
                        (cdcl_only += '=') += value;

                    cdcl_only += ' ';

                    if (auto const t = subopt_bool_val(value); t)
                        arguments.phase_saving = *t;
                    else
                        return ARGERR;

                    break;
                case 1: // nops
                    cdcl_only += "-c nops ";
                    arguments.phase_saving = false;
                    break;
                case 2: // up (unit propagation)
                    dpll_only = "-c up";

                    if (value)
                        (dpll_only += '=') += value;

                    dpll_only += ' ';

                    if (auto const t = subopt_bool_val(value); t)
                        arguments.unit_prop = *t;
                    else
                        return ARGERR;

                    break;
                case 3: // noup
                    dpll_only += "-c noup ";
                    arguments.unit_prop = false;
                    break;
                case 4: // pl (pure literal elimination)
                    if (auto const t = subopt_bool_val(value); t)
                        arguments.pure_lit_elim = *t;
                    else
                        return ARGERR;

                    break;
                case 5: // nopl
                    arguments.pure_lit_elim = false;
                    break;
                case 6: // sub (subsumed clause elimination)
                    if (auto const t = subopt_bool_val(value); t)
                        arguments.subsumed = *t;
                    else
                        return ARGERR;

                    break;
                case 7: // nosub
                    arguments.subsumed = false;
                    break;
                case 8: // bce (blocked clause elimination)
                    if (auto const t = subopt_bool_val(value); t)
                        arguments.bce = *t;
                    else
                        return ARGERR;

                    break;
                case 9: // nobce
                    arguments.bce = false;
                    break;
                case 10: // holes (hole elimination)
                    if (auto const t = subopt_bool_val(value); t)
                        arguments.holes = *t;
                    else
                        return ARGERR;

                    break;
                case 11: // noholes
                    arguments.holes = false;
                    break;
                default:
                    solver_error_output("invalid configuration suboption '", value, '\'');
                    solver_error_output_additional("try '-h' for help");
                    return ARGERR;
                }
            }

            break;
        case 's': // learning scheme
            ((cdcl_only += "-s ") += optarg) += ' ';

            if (std::strcmp(optarg, "1uip") == 0) {
                arguments.scheme = Scheme::UIP;
            } else if (std::strcmp(optarg, "relsat") == 0) {
                arguments.scheme = Scheme::RELSAT;
            } else if (std::strcmp(optarg, "decision") == 0) {
                arguments.scheme = Scheme::DECISION;
            } else {
                solver_error_output("invalid learning scheme '", optarg, '\'');
                solver_error_output_additional("try '-h' for help");
                return ARGERR;
            }

            break;
        case 'x': // deletion strategy
            ((cdcl_only += "-x ") += optarg) += ' ';

            if (auto val = optval(optarg, "simple"); val) {
                arguments.strategy = Strategy::SIMPLE;

                if (*val) { // check for option value C
                    int const ret = std::sscanf(*val, "%zu", &arguments.strategy_simple_c);

                    if (ret != 1) {
                        solver_error_output("invalid capacity '", *val, "' for the simple deletion strategy");
                        return ARGERR;
                    } else if (arguments.strategy_simple_c == 0) {
                        solver_error_output("invalid capacity '", *val, "' for the simple deletion strategy");
                        solver_error_output_additional("the capacity has to be a positive integer");
                        return ARGERR;
                    }
                }
            } else if ((val = optval(optarg, "bounded"))) {
                arguments.strategy = Strategy::BOUNDED;

                if (*val) { // check for option values K and M
                    int const ret = std::sscanf(*val, "%zu,%zu", &arguments.strategy_bounded_k,
                                                &arguments.strategy_bounded_m);
                    if (ret != 2) {
                        solver_error_output("invalid thresholds '", *val, "' for the bounded deletion strategy");
                        return ARGERR;
                    } else if (arguments.strategy_bounded_k == 0 || arguments.strategy_bounded_m == 0) {
                        solver_error_output("invalid thresholds '", *val, "' for the bounded deletion strategy");
                        solver_error_output_additional("the thresholds have to be positive integers");
                        return ARGERR;
                    } else if (arguments.strategy_bounded_m > arguments.strategy_bounded_k) {
                        solver_error_output("invalid thresholds '", *val, "' for the bounded deletion strategy");
                        solver_error_output_additional("the unassignment threshold has to be less or equal than the small clause threshold");
                        return ARGERR;
                    }
                }
            } else {
                solver_error_output("invalid deletion strategy '", optarg, '\'');
                solver_error_output_additional("try '-h' for help");
                return ARGERR;
            }

            break;
        case 'r': // restart policy
            ((cdcl_only += "-r ") += optarg) += ' ';

            if (auto val = optval(optarg, "io"); val) {
                arguments.policy = Policy::INNER_OUTER;

                if (*val) { // check for option values C and F
                    int const ret = std::sscanf(*val, "%zu,%lf", &arguments.policy_io_c,
                                                &arguments.policy_io_f);
                    if (ret != 2) {
                        solver_error_output("invalid parameters '", *val, "' for the inner/outer restart policy");
                        return ARGERR;
                    } else if (arguments.policy_io_c == 0) {
                        solver_error_output("invalid base inner interval '0' for the inner/outer restart policy");
                        solver_error_output_additional("the base inner interval has to be a positive integer");
                        return ARGERR;
                    } else if (arguments.policy_io_f <= 1.0) {
                        solver_error_output("invalid factor '", arguments.policy_io_f,
                                            "' for the inner/outer restart policy");
                        solver_error_output_additional("the factor has to be a floating point number > 1");
                        return ARGERR;
                    }
                }
            } else if ((val = optval(optarg, "geometric"))) {
                arguments.policy = Policy::GEOMETRIC;

                if (*val) { // check for option values C and F
                    int const ret = std::sscanf(*val, "%zu,%lf", &arguments.policy_geometric_c,
                                                &arguments.policy_geometric_f);
                    if (ret != 2) {
                        solver_error_output("invalid parameters '", *val, "' for the geometric restart policy");
                        return ARGERR;
                    } else if (arguments.policy_geometric_c == 0) {
                        solver_error_output("invalid interval length '0' for the geometric restart policy");
                        solver_error_output_additional("the interval length has to be a positive integer");
                        return ARGERR;
                    } else if (arguments.policy_geometric_f <= 1.0) {
                        solver_error_output("invalid factor '", arguments.policy_geometric_f,
                                            "' for the geometric restart policy");
                        solver_error_output_additional("the factor has to be a floating point number > 1");
                        return ARGERR;
                    }
                }
            } else if ((val = optval(optarg, "luby"))) {
                arguments.policy = Policy::LUBY;

                if (*val) { // check for option value C
                    int const ret = std::sscanf(*val, "%zu", &arguments.policy_luby_c);

                    if (ret != 1) {
                        solver_error_output("invalid base interval '", *val, "' for the Luby restart policy");
                        return ARGERR;
                    } else if (arguments.policy_luby_c == 0) {
                        solver_error_output("invalid base interval '", *val, "' for the Luby restart policy");
                        solver_error_output_additional("the base interval has to be a positive integer");
                        return ARGERR;
                    }
                }
            } else if ((val = optval(optarg, "fixed"))) {
                arguments.policy = Policy::FIXED;

                if (*val) { // check for option value C
                    int const ret = std::sscanf(*val, "%zu", &arguments.policy_fixed_c);

                    if (ret != 1) {
                        solver_error_output("invalid interval length '", *val, "' for the fixed restart policy");
                        return ARGERR;
                    } else if (arguments.policy_fixed_c == 0) {
                        solver_error_output("invalid interval length '", *val, "' for the fixed restart policy");
                        solver_error_output_additional("the interval length has to be a positive integer");
                        return ARGERR;
                    }
                }
            } else if (std::strcmp(optarg, "none") == 0) {
                arguments.policy = Policy::NO;
            } else {
                solver_error_output("invalid restart policy '", optarg, '\'');
                solver_error_output_additional("try '-h' for help");
                return ARGERR;
            }

            break;
        default: // '?', invalid option
            solver_error_output("invalid option '-", static_cast<char>(optopt), '\'');
            solver_error_output_additional("try '-h'for help");
            return ARGERR;
        }

    if (!input && optind < argc) { // use `argv[optind]` as input
        arguments.input_file = std::strcmp(argv[optind], "-") == 0 ? nullptr : argv[optind];
        ++optind;
    }

    if (!output && optind < argc) { // use `argv[optind]` as output
        arguments.output_file = std::strcmp(argv[optind], "-") == 0 ? nullptr : argv[optind];
        ++optind;
    }

    // check for errors

    if (optind < argc) { // unnecessary arguments
        std::string unnecessary {argv[optind++]};

        while (optind < argc)
            (unnecessary += ' ') += argv[optind++];

        solver_non_quiet_output("Warning: Unnecessary arguments, ignoring '", unnecessary, '\'');
        solver_non_quiet_output();
    }

    if (arguments.solver_type != type_from_heuristic(arguments.heuristic)) { // wrong heuristic
        solver_error_output("specified branching heuristic ", heuristic_name(arguments.heuristic),
                            " cannot be used with Solver of type ", type_name(arguments.solver_type));
        solver_error_output_additional("try '-h' for help");
        return ARGERR;
    }

    if (arguments.solver_type == Solver_Type::CDCL && !dpll_only.empty()) { // DPLL-options in CDCL mode
        solver_non_quiet_output("Warning: These options are ignored for CDCL: '", dpll_only, '\'');
        solver_non_quiet_output();
    } else if (arguments.solver_type == Solver_Type::DPLL && !cdcl_only.empty()) { // CDCL-options in DPLL mode
        solver_non_quiet_output("Warning: These options are ignored for DPLL: '", cdcl_only, '\'');
        solver_non_quiet_output();
    }

    return 0;
}
