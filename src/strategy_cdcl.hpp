/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRATEGY_CDCL_HPP
#define STRATEGY_CDCL_HPP

#include "solver_cdcl_tools.hpp"
#include "proof_logging.hpp"

#include <unordered_map>
#include <algorithm> // std::find
#include <iterator> // std::next


/* Strategy_CDCL
 * This is the base class for the clause deletion strategies
 * A strategy stores learned clauses and decides when to delete them
 *
 * The final derived class should be passed as `Derived` (-> CRTP to avoid runtime polymorphism)
 * It should implement the following member functions:
 * * void init(var_t)
 * * * initialise strategy
 * * void update_unset(var_t, Variables<CDCL> const&)
 * * * this function is called after a variable was unset
 * * void clean(F, bool, Variables<CDCL> const&)
 * * * actually deletes the clauses that are to be deleted according to the strategy
 * * Clause<CDCL>& push(Clause<CDCL>&& clause)
 * * * learn a new clause
 */
template <typename Derived>
class Strategy_CDCL {
public:
    // No copies, just move construction
    Strategy_CDCL(Strategy_CDCL const&) = delete;
    Strategy_CDCL(Strategy_CDCL&&) = default;
    Strategy_CDCL& operator = (Strategy_CDCL const&) = delete;

    // get the number of deleted clauses
    std::size_t deleted_counter() const noexcept {
        return deleted_counter_;
    }

    // get the maximal number of clauses in the strategy
    std::size_t max_clause_count() const noexcept {
        return max_clause_count_;
    }

    // get the maximal clause width in the strategy
    width_t max_clause_width() const noexcept {
        return max_clause_width_;
    }

    /* Push a new clause to the strategy
     * This may remove old clauses (calls `clean`)
     * sets the watched occurences of the new clause
     * logs new and deleted clauses to `pl`
     * if `permanent` is true, the clause will be kept until destruction
     *
     * returns a reference to the pushed clause
     */
    Clause<CDCL> const& clean_push(Clause<CDCL>&& clause, bool const permanent,
                                   Variables<CDCL>& vars, Proof_Logger* const pl) {
        // log `clause` (this must be done before cleanup)
        if (pl)
            pl->log(false, clause.lits);

        // cleanup
        if (pl)
            derived().clean([&vars, pl, this] (Clause<CDCL> const& cls) {
                del(cls, vars);
                pl->log(true, cls.lits);
            }, permanent, vars);
        else
            derived().clean([&vars, this] (Clause<CDCL> const& cls) {
                del(cls, vars);
            }, permanent, vars);

        // push `clause`
        Clause<CDCL>& cls {permanent ? (permanent_.push_back(std::move(clause)), permanent_.back())
                                     : derived().push(std::move(clause))};
        set_wocc(cls, vars);

        // update maximal clause width
        if (std::size_t const clause_width = cls.lits.size(); clause_width > max_clause_width_)
            max_clause_width_ = static_cast<width_t>(clause_width);

        // update maximal number of clauses
        if (std::size_t const clause_count = permanent_.size() + learned_.size();
                clause_count > max_clause_count_
           )
            max_clause_count_ = clause_count;

        return cls;
    }

protected:
    // remove `clause` from `occ` (searches backward, `clause` must be in `occ`)
    static void erase_occ(Occurences<CDCL>& occ, Clause<CDCL> const* const clause) {
        occ.erase(std::next(std::find(occ.crbegin(), occ.crend(), clause)).base());
    }

    // check if `clause` is a reason
    static bool reason(Clause<CDCL> const& clause, Variables<CDCL> const& vars) {
        /* clause is a reason
         * => clause is satisfied and was a unit clause
         * => both watched literals are assigned
         * one of the assignments has clause as reason
         */
        return vars[clause.wali[0].var].assignment && vars[clause.wali[1].var].assignment &&
            (vars[clause.wali[0].var].assignment->reason == &clause ||
             vars[clause.wali[1].var].assignment->reason == &clause);
    }


    Clauses<CDCL> learned_ {}; // non-permanent learned clauses

    Strategy_CDCL() = default;

private:
    // set watched occurences
    static void set_wocc(Clause<CDCL>& clause, Variables<CDCL>& vars) {
        vars[clause.wali[0].var].wocc[clause.wali[0].val].push_back(&clause);

        if (clause.wali[0] != clause.wali[1])
            vars[clause.wali[1].var].wocc[clause.wali[1].val].push_back(&clause);
    }

    // remove watched occurences
    static void unset_wocc(Clause<CDCL> const& clause, Variables<CDCL>& vars) {
        erase_occ(vars[clause.wali[0].var].wocc[clause.wali[0].val], &clause);

        if (clause.wali[0] != clause.wali[1])
            erase_occ(vars[clause.wali[1].var].wocc[clause.wali[1].val], &clause);
    }

    Clauses<CDCL> permanent_ {}; // permanent learned clauses
    // counters
    std::size_t deleted_counter_ {};
    std::size_t max_clause_count_ {};
    width_t max_clause_width_ {};

    // convert `this` to object of derived class (CRTP) and return as a reference
    Derived& derived() noexcept {
        return *static_cast<Derived*>(this);
    }

    // delete `clause`, i.e. remove watched occurences and increase `deleted_counter_`
    void del(Clause<CDCL> const& clause, Variables<CDCL>& vars) {
        solver_assert(!reason(clause, vars));
        ++deleted_counter_;
        unset_wocc(clause, vars);
    }
};

/* Strategy_Simple
 * Deletion Strategy implementing the Simple strategy
 *
 * Try to keep only a fixed number of clauses.
 * If the structure is full and all clauses are reasons, the structure is expanded.
 */
class Strategy_Simple : public Strategy_CDCL<Strategy_Simple> {
    friend Strategy_CDCL<Strategy_Simple>;

public:
    static Strategy_Simple from_arguments();

    explicit Strategy_Simple(std::size_t const count) :
        count_(count) {}

    void init(var_t) noexcept {
        // do nothing
    }

    void update_unset(var_t, Variables<CDCL> const&) noexcept {
        // do nothing
    }

private:
    std::size_t count_;
    std::size_t index_ {};

    // delete next clause that is not a reason if structure is full
    template <typename F>
    void clean(F del_func, bool const permanent, Variables<CDCL> const& vars) {
        if (!permanent && learned_.size() == count_) { // delete clause
            // find next clause that is not a reason
            std::size_t i;
            for (i = 0; i < count_ && reason(learned_[(index_ + i) % count_], vars); ++i);

            if (i > 0)
                solver_log("clause was a reason");

            if (i == count_) {
                solver_log("all learned clauses are reasons");
                ++count_;
            } else {
                index_ = (index_ + i) % count_;
                del_func(learned_[index_]);

                // learned[index_] will be overriden in `push()`
            }
        }
    }

    Clause<CDCL>& push(Clause<CDCL>&& clause) {
        if (learned_.size() < count_) { // push new clause
            learned_.push_back(std::move(clause));
            return learned_.back();
        } else { // override old clause
            Clause<CDCL>& cls {learned_[index_]};
            index_ = (index_ + 1) % count_;
            cls = std::move(clause);
            return cls;
        }
    }
};

/* Strategy_Bounded
 * Deletion Strategy implementing a combination of the k-bounded and the m-size relevance based strategies
 *
 * precondition: 1 <= m <= k
 *
 * Delete clauses that have more than m literals unassigned and a width of more than k.
 */
class Strategy_Bounded : public Strategy_CDCL<Strategy_Bounded> {
    friend Strategy_CDCL<Strategy_Bounded>;

public:
    static Strategy_Bounded from_arguments();

    explicit Strategy_Bounded(width_t const k, width_t const m) :
        k_(k), m_(m) {}

    // init strategy: resize `occ_`
    void init(var_t const var_count) {
        occ_.resize(var_count + 1);
    }

    // var was unset -> check if we can push new clauses to `to_delete_`
    void update_unset(var_t const var, Variables<CDCL> const& vars) {
        std::size_t const to_delete_index = to_delete_.size();

        for (Clause<CDCL>* const clause : occ_[var])
            // m >= 1
            if (vars[clause->wali[0].var].free() && vars[clause->wali[1].var].free())
                if (m_ == 1 || std::count_if(clause->lits.cbegin(), clause->lits.cend(),
                    [&vars] (Literal const l) {return vars[l.var].free();}) > m_
                ) { // there are more than `m_` literals unassigned
                    to_delete_.push_back(clause);
                }

        // delete the new `to_delete_` clauses from `occ_`
        for (std::size_t i = to_delete_index; i < to_delete_.size(); ++i)
            if (m_ == 1) // only delete walis
                for (bool const b : bools)
                    erase_occ(occ_[to_delete_[i]->wali[b].var], to_delete_[i]);
            else // delete all literals
                for (Literal const l : to_delete_[i]->lits)
                    erase_occ(occ_[l.var], to_delete_[i]);
    }

private:
    width_t k_, m_; // 1 <= m_ <= k
    // learned occurences with width > `k_` (only watched occurences for `m_` = 1)
    std::vector<Occurences<CDCL>> occ_ {};
    // clauses that are to delete or deleted (and can be overriden)
    std::deque<Clause<CDCL>*> to_delete_ {}, deleted_ {};

    // delete clauses from `to_delete_` and push to `deleted_`
    template <typename F>
    void clean(F del_func, bool, Variables<CDCL> const&) {
        while (!to_delete_.empty()) {
            del_func(*to_delete_.front());
            deleted_.push_back(to_delete_.front());
            to_delete_.pop_front();
        }
    }

    Clause<CDCL>& push(Clause<CDCL>&& clause) {
        Clause<CDCL>* cls;

        if (deleted_.empty()) { // push new clause
            learned_.push_back(std::move(clause));
            cls = &learned_.back();
        } else { // override old clause
            cls = deleted_.front();
            deleted_.pop_front();
            *cls = std::move(clause);
        }

        // insert new clause into `occ_`
        if (cls->lits.size() > k_) {
            if (m_ == 1) // only insert walis
                for (bool const b : bools)
                    occ_[cls->wali[b].var].push_back(cls);
            else // insert all literals
                for (Literal const l : cls->lits)
                    occ_[l.var].push_back(cls);
        }

        return *cls;
    }
};

#endif // STRATEGY_CDCL_HPP
