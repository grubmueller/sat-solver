/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FORMULA_HPP
#define FORMULA_HPP

#include "literal.hpp"

#include <vector>
#include <deque>
#include <memory> // std::unique_ptr
#include <algorithm> // std::sort
#include <utility> // std::move
#include <iosfwd> // std::istream


// clause width type
using width_t = var_t;
using swidth_t = svar_t;

using Literals = std::vector<Literal>;

class Formula {
public:
    // Formula::Clause only represents a set of literals, see also Clause in solver_tools.hpp
    class Clause {
    public:
        using value_type = Literal;
        using size_type = width_t;
        using difference_type = swidth_t;
        using iterator = Literals::const_iterator;
        using const_iterator = Literals::const_iterator;

        /* Construct a clause from a vector of literals
         *
         * precondition: all literals in `lits` are valid
         */
        Clause(Literals lits) :
            lits_(std::move(lits)) {
            if (lits_.empty())
                return;

            std::sort(lits_.begin(), lits_.end()); // sort the literals

            // check if this clause is a tautology and erase duplicates

            Literals::iterator res; // result
            for (auto it = res = lits_.begin(); ++it != lits_.end();) // loop through `lits_`
                if (res->var == it->var) { // duplicate or tautology
                    if (res->val != it->val) { // tautology
                        // change `lits_`: `lits_[0]` is invalid, `lits_[1]` is the highest literal
                        lits_ = {{}, lits_.back()};
                        goto shrink;
                    }

                    // if this is a duplicate, don't increment `res`
                } else if (++res != it) { // first after duplicates (incremet `res`)
                    *res = *it;
                }

            lits_.erase(++res, lits_.end()); // `++res` is the end of the new range

        shrink:
            lits_.shrink_to_fit();
        }

        // returns true if the clause is empty, otherwise false
        bool empty() const noexcept {
            return lits_.empty();
        }

        // returns true if the clause is a tautology, otherwise false
        bool taut() const noexcept {
            return !empty() && !front();
        }

        /* returns the width of the clause
         *
         * returns 0 if the clause is empty or a tautology
         */
        size_type width() const noexcept {
            return front() ? static_cast<size_type>(lits_.size()) : 0;
        }

        /* returns the first/lowest literal of the clause
         *
         * returns an invalid literal if the clause is empty or a tautology
         */
        Literal front() const noexcept {
            return empty() ? Literal{} : lits_.front();
        }

        /* returns the last/highest literal of the clause
         *
         * returns an invalid literal if the clause is empty or a tautology
         */
        Literal back() const noexcept {
            return front() ? lits_.back() : Literal{};
        }

        /* returns the highest variable of the clause
         * this is different from back().var in the case of a tautology
         *
         * returns 0 if the clause is empty
         */
        var_t highest() const noexcept {
            return empty() ? 0 : lits_.back().var;
        }

        /* returns a constant iterator to the first/lowest literal of the clause
         *
         * returns end() if the clause is empty or a tautology
         */
        const_iterator begin() const noexcept {
            return front() ? lits_.cbegin() : end();
        }

        const_iterator cbegin() const noexcept {
            return begin();
        }

        /* returns a constant iterator to the element
         * following the last/highest literal of the clause
         */
        const_iterator end() const noexcept {
            return lits_.cend();
        }

        const_iterator cend() const noexcept {
            return end();
        }

        /* Get the clause as vector of literals (`Literals`)
         *
         * precondition: clause is not a tautology
         */
        Literals const& lits() const& noexcept {
            return lits_;
        }

        Literals lits() && noexcept {
            return std::move(lits_);
        }

    private:
        Literals lits_;
    };

private:
    using Clauses = std::deque<Clause>;

public:
    using value_type = Clause;
    using size_type = Clauses::size_type;
    using difference_type = Clauses::difference_type;
    using iterator = Clauses::iterator;
    using const_iterator = Clauses::const_iterator;

    /* Constructor: set the initial number of variables
     *
     * The number of variables is automatically increased in the `add()` member function
     */
    explicit Formula(var_t const var_count = 0) :
        var_count_(var_count) {}

    // overall number of clauses
    size_type clause_count() const noexcept {
        return clauses_.size();
    }

    // returns true if the formula is empty, otherwise false
    bool empty() const noexcept {
        return clauses_.empty();
    }

    // overall number of variables
    var_t var_count() const noexcept {
        return var_count_;
    }

    // maximal clause width
    width_t max_clause_width() const noexcept {
        return max_clause_width_;
    }

    // returns true if the formula contains an empty clause, otherwise false
    bool contains_empty() const noexcept {
        return contains_empty_;
    }

    // element access
    Clause& front() {
        return clauses_.front();
    }

    Clause const& front() const {
        return clauses_.front();
    }

    Clause& back() {
        return clauses_.back();
    }

    Clause const& back() const {
        return clauses_.back();
    }

    Clause& operator[](size_type const pos) {
        return clauses_[pos];
    }

    Clause const& operator[](size_type const pos) const {
        return clauses_[pos];
    }

    // iterators
    iterator begin() noexcept {
        return clauses_.begin();
    }

    const_iterator begin() const noexcept {
        return clauses_.cbegin();
    }

    const_iterator cbegin() const noexcept {
        return clauses_.cbegin();
    }

    iterator end() noexcept {
        return clauses_.end();
    }

    const_iterator end() const noexcept {
        return clauses_.cend();
    }

    const_iterator cend() const noexcept {
        return clauses_.cend();
    }

    /* Add a clause to the formula
     * tautologic clauses are not added, but the number of variables and the maximal clause width
     * are updated
     */
    void add(Clause clause) {
        if (clause.empty()) // check if the clause is empty
            contains_empty_ = true;
        // check if we added more variables and update `var_count_` accordingly
        else if (var_t const highest = clause.highest(); var_count_ < highest)
            var_count_ = highest;

        // check if we added a wider clause and update `max_clause_width_` accordingly
        if (width_t const width = clause.width(); max_clause_width_ < width)
            max_clause_width_ = width;

        if (!clause.taut()) // do not add tautologic clauses
            clauses_.push_back(std::move(clause));
    }

    /* Remove clause by swapping with the last element
     * does not recompute number of variables and maximal clause width -> see methods below
     */
    void remove(size_type const pos) {
        if (pos < clauses_.size() - 1)
            clauses_[pos] = std::move(back());

        clauses_.pop_back();
    }

    void remove(iterator const it) {
        if (it < end() - 1)
            *it = std::move(back());

        clauses_.pop_back();
    }

    /* Reset number of variables
     *
     * precondition: there are at least `var_count` variables in the formula
     */
    void reset_var_count(var_t const var_count) {
        var_count_ = var_count;
    }

    /* Recompute number of variables
     * this may be necessary after removing clauses
     * may remove information about tautologic clauses (because they are not added)
     */
    void recompute_var_count() {
        var_count_ = 0;

        for (Clause const& clause : clauses_)
            if (var_t const highest = clause.highest(); var_count_ < highest)
                var_count_ = highest;
    }

    /* Recompute maximal clause width
     * this may be necessary after removing clauses
     * may remove information about tautologic clauses (because they are not added)
     */
    void recompute_max_clause_width() {
        max_clause_width_ = 0;

        for (Clause const& clause : clauses_)
            if (width_t const width = clause.width(); max_clause_width_ < width)
                max_clause_width_ = width;
    }

private:
    Clauses clauses_ {};
    var_t var_count_;
    width_t max_clause_width_ {};
    bool contains_empty_ {};
};

/* Read a DIMACS formula
 *
 * input: a `std::istream` containing a DIMACS formula
 *
 * returns a pointer to the formula or a null pointer if an error occured
 *
 * defined in dimacs_io.cpp
 */
std::unique_ptr<Formula> read_dimacs(std::istream& is);

#endif // FORMULA_HPP
