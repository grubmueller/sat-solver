/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LITERAL_HPP
#define LITERAL_HPP

#include <limits> // std::numeric_limits
#include <type_traits> // std::make_signed
#include <functional> // std::hash


/* (unsigned) type of variable index
 * 0 is not a valid variable index (reserved for other purposes)
 */
using var_t = unsigned;

// maximum variable index (1 bit less for svar_t and Literal)
constexpr var_t var_max = std::numeric_limits<var_t>::max() >> 1;
// usable bits for var_t (1 bit less)
constexpr int var_t_bits = std::numeric_limits<var_t>::digits - 1;

/* signed type of variable index
 * one can use svar_t for literals: abs(svar) for var, (svar > 0) for val
 */
using svar_t = std::make_signed_t<var_t>;

/* struct for literal
 * there is an implicit conversion from and to svar_t
 */
struct Literal {
    bool val : 1;
    var_t var : var_t_bits;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion" // ignore -Wconversion (due to bit field)

    // construct a literal from a bool and a var_t
    constexpr Literal(bool const val, var_t const var) noexcept :
        val(val), var(var) {}

    // conversion from svar_t and default constructor
    constexpr Literal(svar_t const svar = 0) noexcept :
        val(svar >= 0), var(val ? svar : -svar) {}

#pragma GCC diagnostic pop

    // conversion to svar_t
    constexpr operator svar_t() const noexcept {
        return val ? static_cast<svar_t>(var) : -static_cast<svar_t>(var);
    }

    // returns if literal is valid (i.e. var != 0)
    constexpr explicit operator bool() const noexcept {
        return var;
    }

    // get the literal with `val` flipped
    constexpr Literal flip() const noexcept {
        return Literal{!val, var};
    }
};

constexpr bool operator==(Literal const lhs, Literal const rhs) noexcept {
    return lhs.var == rhs.var && lhs.val == rhs.val;
}

constexpr bool operator!=(Literal const lhs, Literal const rhs) noexcept {
    return !(lhs == rhs);
}

// lexicographical comparison of literals (var first, val second)

constexpr bool operator<(Literal const lhs, Literal const rhs) noexcept {
    return lhs.var < rhs.var || (lhs.var == rhs.var && lhs.val < rhs.val);
}

constexpr bool operator<=(Literal const lhs, Literal const rhs) noexcept {
    return lhs.var < rhs.var || (lhs.var == rhs.var && lhs.val <= rhs.val);
}

constexpr bool operator>(Literal const lhs, Literal const rhs) noexcept {
    return rhs < lhs;
}

constexpr bool operator>=(Literal const lhs, Literal const rhs) noexcept {
    return rhs <= lhs;
}

// hash for Literal
struct Literal_Hash : public std::hash<svar_t> {
    std::size_t operator()(Literal const l) const noexcept {
        return std::hash<svar_t>::operator()(l);
    }
};


// ensure sizes (if these fail, we are on a weird platform)
static_assert(sizeof(var_t) == sizeof(svar_t) && sizeof(svar_t) == sizeof(Literal));
static_assert(var_max == std::numeric_limits<svar_t>::max());

#endif // LITERAL_HPP
