/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "solve.hpp"
#include "preprocessing.hpp"
#include "proof_logging.hpp"
#include "arguments.hpp"
#include "printing.hpp"


namespace {
/* Check if formula is already trivially satisfiable or trivially unsatisfiable.
 *
 * returns:
 * * exit code of the program (10 or 20) > 0 if formula is trivial, 0 otherwise
 * * an optional solution
 */
inline std::pair<int, std::optional<Solution>> is_trivial(Formula const& f) {
    if (f.empty()) // `f` is empty (trivial SATISFIABLE)
        return {10, Solution{f.var_count()}};
    else if (f.contains_empty()) // `f` contains an empty clause (trivial UNSATISFIABLE)
        return {20, std::nullopt};
    else
        return {0, std::nullopt};
}

/* Solve formula using the algorithms set in `arguments`.
 *
 * returns solution and statistics
 *
 * precondition: `f` is not empty and does not contain an empty clause
 */
inline Solution_Statistics_Pair solve(std::unique_ptr<Formula>&& f, Proof_Logger* const pl) {
    switch (arguments.solver_type) {
    case Solver_Type::DPLL: // DPLL-Solver
        return solve_dpll(std::move(f), arguments.heuristic, arguments.unit_prop, arguments.pure_lit_elim);
    case Solver_Type::CDCL: // CDCL-Solver
        return solve_cdcl(std::move(f), arguments.heuristic,
                          arguments.strategy, arguments.policy,
                          arguments.phase_saving, arguments.scheme, pl);
    }

    return {};
}

// postprocess and print out solution
inline void postprocess_print(std::optional<Solution>& sol, Preprocessor_Data& pre_data,
                              Formatting const& formatting, std::ostream& os) {
    if (sol && (arguments.holes || arguments.bce)) { // solution must be postprocessed
#ifndef NDEBUG // if debug
        if (arguments.logging) // we write debug output to standard output
            print_section_heading(formatting, "postprocessor debug");
#endif

        postprocess(*sol, pre_data);

#ifndef NDEBUG // if debug
        solver_debug_output();
#endif
    }

    print_solution(formatting, os, sol);
}
}

/* Solve formula and print out solution.
 *
 * Does the following:
 * * checks for trivial cases (`f` empty or `f` containing an empty clause)
 * * prints preprocessor settings to standard output
 * * preprocesses `f` using the algorithms set in `arguments`
 * * prints preprocessor statistics to standard output
 * * prints solver settings to standard output
 * * solves `f` using the algorithms set in `arguments`
 * * prints solver statistics to standard output
 * * postprocesses the solution
 * * prints the solution to `os` (and standard output)
 *
 * returns the exit code of the program (10 if SATISFIABLE, 20 if UNSATISFIABLE)
 */
int solve_print(std::unique_ptr<Formula> f, Formatting const& formatting, std::ostream& os,
                Proof_Logger* const proof_logger) {
    // check if formula is trivial
    if (auto const [trivial, sol] = is_trivial(*f); trivial) {
        if (trivial == 10)
            solver_non_quiet_output("Formula is empty or contains only tautologic clauses");
        else // trivial == 20
            solver_non_quiet_output("Formula contains an empty clause");

        solver_non_quiet_output();

        print_solution(formatting, os, sol);

        return trivial;
    }


    // Do we write something (proof/debug) to standard output while solving?
    std::string solve_output {};

#ifndef NDEBUG // if debug
    if (arguments.logging) // we write debug output to standard output
        solve_output = "debug";
    else
#endif
    if (proof_logger && !arguments.proof_file) // we write the proof to standard output
        solve_output = "proof";


    // preprocessor

    Preprocessor_Data pre_data {};

    if (arguments.subsumed || arguments.bce || arguments.pure_lit_elim || arguments.holes) {
        // preprocessing
        print_preprocessor_settings(formatting);

        if (!solve_output.empty())
            print_section_heading(formatting, "preprocessor " + solve_output);

        if (proof_logger && arguments.proof_file) // we write the proof to a file
            proof_logger->comment("preprocessing");

        preprocess(*f, pre_data,
                   arguments.subsumed, arguments.bce, arguments.pure_lit_elim, arguments.holes,
                   proof_logger);

        if (!solve_output.empty())
            solver_non_quiet_output();

        print_statistics(formatting, pre_data.stats, "preprocessor statistics");

        // check if formula is already solved
        if (auto [ret, sol] = is_trivial(*f); ret) {
            solver_non_quiet_output("Preprocessor finished with conclusive result");
            solver_non_quiet_output("Not starting solver");
            solver_non_quiet_output();

            // postprocess and print solution
            postprocess_print(sol, pre_data, formatting, os);

            return ret;
        } else if (proof_logger && arguments.proof_file) { // we write the proof to a file
            proof_logger->comment("solving");
        }
    } else {
        // no preprocessing
        print_no_preprocessor(formatting);
    }


    // solver

    print_solver_settings(formatting);

    if (!solve_output.empty())
        print_section_heading(formatting, solve_output);

    auto [sol, stats] = solve(std::move(f), proof_logger);

    if (!solve_output.empty())
        solver_non_quiet_output();

    print_statistics(formatting, stats, "solver statistics");


    // postprocess and print solution
    postprocess_print(sol, pre_data, formatting, os);

    return sol ? 10 : 20;
}
