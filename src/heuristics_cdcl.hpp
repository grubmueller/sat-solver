/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HEURISTICS_CDCL_HPP
#define HEURISTICS_CDCL_HPP

#include "solver_cdcl_tools.hpp"

#include <vector>
#include <list>
#include <map>
#include <algorithm> // std::sort, std::fill


/* Heuristic_VSIDS
 * CDCL-Heuristic implementing variable state independent decaying sum
 * 
 * The priority s of a literal is initiated as the amount of occurences.
 * The counter r of a literal is initiated as zero.
 * We choose a literal of highest priority for branching and on learning a new clause 
 * increase r for every literal in the new clause.
 * 
 * Every 255 branchings the priority of every literal ist updated with s = s/2 + r
 * and the counter is reset.
 */ 
class Heuristic_VSIDS {
public:
    // Standard constructor (call init!)
    Heuristic_VSIDS() = default;

    // No copies or moves
    Heuristic_VSIDS(Heuristic_VSIDS const&) = delete;
    Heuristic_VSIDS& operator=(Heuristic_VSIDS const&) = delete;

    /* Initialize the VSIDS heuristic
     * 
     * The priority vector `s_` and the counter vector `r_` are initialized with size `var_count`.
     * The priority of all literals is set to their number of occurences and all clauses in
     * `clauses` are emplaced in the heuristic vector `al_` which is then sorted in decenting order
     * and the iterator `it_` points at the begining of `al_`
     */ 
    void init(var_t const var_count, Clauses<CDCL> const& clauses) {
        solver_log("VSIDS: init");

        al_.reserve(2*var_count);
        for (bool const a : bools) {
            r_[a].resize(var_count+1);
            s_[a].resize(var_count+1);
        }

        // populate s
        for (Clause<CDCL> const& c : clauses)
            for (Literal const l : c.lits)
                s_[l.val][l.var]++;

        // populate `al_`
        for (bool const a : bools)
            for (var_t i = 1; i <= var_count; i++)
                al_.emplace_back(Literal{a, i});

        // sort the vector according to the heuristic value of each entry
        std::sort(al_.begin(), al_.end(), [this] (Literal const lhs, Literal const rhs) {
                return s_[lhs.val][lhs.var] > s_[rhs.val][rhs.var];
        });

        it_ = al_.begin();

        solver_log("VSIDS: init done");
    }

    // increase counter for all literals in `clause`, reset `it_` (because of backtracking)
    void update_learned(Clause<CDCL> const& clause) {
        for (Literal const l : clause.lits)
            r_[l.val][l.var]++;

        it_ = al_.begin();
    }

    /* returns the next branching literal out of `vars` and if this is the 255th branching also
     * updates the priority for each literal to the priority divided by two plus the counter and
     * resets this counter
     * returns an invalid literal if we are finished (-> SATISFIABLE)
     * updates `it_`
     */ 
    Literal top(std::vector<Variable<CDCL>> const& vars) {
        // search for the next free literal and store result in Literal t
        Literal t {};
        for (; it_ != al_.end(); ++it_) {
            if (vars[it_->var].free()) {
                t = *it_;
                break;
            }
            // element is set -> search further
        }

        // rebuild index after 255 branchings
        if (++branchings_ == 255) {
            solver_log("VSIDS: rebuilding");
            branchings_ = 0; // reset counter

            // update the global priority s registers
            for (bool const a : bools)
                for (var_t i = 1; i < s_[a].size(); i++)
                    s_[a][i] = s_[a][i]/2 + static_cast<double>(r_[a][i]);

            // reset the counter r
            for (bool const a : bools)
                std::fill(r_[a].begin(), r_[a].end(), 0);

            // resort the vector al_ after updating the heuristic values
            std::sort(al_.begin(), al_.end(), [this] (Literal const lhs, Literal const rhs) {
                    return s_[lhs.val][lhs.var] > s_[rhs.val][rhs.var];
            });

            it_ = al_.begin();

            solver_log("VSIDS: rebuilding done");
        }

        return t;
    }

private:
    std::vector<Literal> al_ {};
    std::vector<Literal>::iterator it_ {};

    // priority of each literal
    std::vector<double> s_[2];
    // counter of each literal
    std::vector<std::size_t> r_[2];

    unsigned short branchings_ = 0;
};

/* Heuristic_VMTF
 * CDCL-Heuristic implementing variable move to front
 * 
 * Each literal has a counter n initialized as the number of occurences.
 * The literals are sorted by decreasing n and we branch at the earliest unassigned literal.
 * 
 * On learning a clause c the counter is incremented for all literals in c and
 * the first 8 (all if c has less than 8 literals) literals are moved to the front of the queue.
 */
class Heuristic_VMTF {
public:
    // Standard constructor (call init!)
    Heuristic_VMTF() = default;

    // No copies or moves
    Heuristic_VMTF(Heuristic_VMTF const&) = delete;
    Heuristic_VMTF& operator=(Heuristic_VMTF const&) = delete;

    /* Initialize the VMTF heuristic
     *
     * Initialize the counter vector `n_` and the vector of itterators on lists `i_` with size `var_count`.
     * The counter of all literals is initialized as their number of occurences, then sort by decreasing order in list `l_`.
     * 
     * `i_` contains iterators that point to the corresponding literal in the list for more efficient deletion.
     * `cur_it` starts at the beginning of `l_`
     */ 
    void init(var_t const var_count, Clauses<CDCL> const& clauses) {
        solver_log("VMTF: init");

        // correctly resize the vectors
        for (bool const a : bools) {
            n_[a].resize(var_count+1);
            i_[a].resize(var_count+1);
        }

        // populate n with h(l)
        for (Clause<CDCL> const& c : clauses)
            for (Literal const l : c.lits)
                n_[l.val][l.var]++;

        std::multimap<std::size_t, Literal> m {};
        // insert h(l)s into map for initial sorting
        for (bool const a : bools)
            for (var_t e = 1; e <= var_count; e++)
                m.emplace(n_[a][e], Literal{a, e});

        // populate `l_` and `i_`
        for (auto const [h,lit] : m)
            i_[lit.val][lit.var] = l_.insert(l_.begin(), lit);

        // set the current iterator to the first element
        cur_it_ = l_.begin();

        solver_log("VMTF: init done");
    }

    /* Increases the counter for each literal in new learned `clause`,
     * then moves the highest min(w(`clause`), 8) literals with the highest counter to the front of `l_`
     * resets `cur_it_` (because of backtracking)
     */ 
    void update_learned(Clause<CDCL> const& clause) {
        // sort the literals after n(a) by using a multimap
        std::multimap<std::size_t, Literal> m {};
        for (Literal const l : clause.lits) {
            n_[l.val][l.var]++;
            m.emplace(n_[l.val][l.var], l);
        }

        // move the highest min(w(clause), 8) literals with the largest n(a) to the top
        unsigned counter = 0;
        for (auto it = m.rbegin(); it != m.rend() && counter < 8; ++it) {
            // remove the existing literal from the list using the stored iterator
            l_.erase(i_[it->second.val][it->second.var]);
            //insert the new element in front and update iterator storage
            i_[it->second.val][it->second.var] = l_.insert(l_.begin(), it->second);
            counter++;
        }

        cur_it_ = l_.begin();
    }

    /* Returns the next branching literal from the list at `cur_it_`
     * updates `cur_it`
     * returns an invalid literal if we are finished (-> SATISFIABLE)
     */
    Literal top(std::vector<Variable<CDCL>> const& vars) {
        // ignore set variables from the list
        Literal lit {};
        for (; cur_it_ != l_.end(); ++cur_it_) {
            if (vars[cur_it_->var].free()) {
                lit = *cur_it_;
                break;
            }
            // element is set -> search further
        }

        return lit;
    }

private:
    std::list<Literal> l_ {};
    std::list<Literal>::iterator cur_it_ {};
    std::vector<std::size_t> n_ [2];
    std::vector<std::list<Literal>::iterator> i_ [2];
};


#endif // HEURISTICS_CDCL_HPP
