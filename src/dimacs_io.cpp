/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "formula.hpp"
#include "solution.hpp"
#include "arguments.hpp"
#include "printing.hpp"

#include <iostream> // std::istream, std::ostream
#include <string>
#include <limits> // std::numeric_limits


namespace {
// ignore all characters in `is` until newline or EOF
inline std::istream& ignore_line(std::istream& is) {
    return is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}
}

/* Read a DIMACS formula
 *
 * input: a `std::istream` containing a DIMACS formula
 *
 * returns a pointer to the formula or a null pointer if an error occured
 */
std::unique_ptr<Formula> read_dimacs(std::istream& is) {
    char bol; // begin of line

    // ignore comments and empty lines
    for (; is >> bol && bol == 'c'; ignore_line(is));


    // parse header

    if (bol != 'p') {
        solver_error_output("parse error: expected 'p'");
        return nullptr;
    }

    if (std::string cnf; !(is >> cnf && cnf == "cnf")) {
        solver_error_output("parse error: expected 'cnf'");
        return nullptr;
    }

    // read number of variables and clauses
    var_t num_var;
    if (!(is >> num_var)) {
        solver_error_output("parse error: expected number of variables");
        return nullptr;
    }

    Formula::size_type num_clauses;
    if (!(is >> num_clauses)) {
        solver_error_output("parse error: expected number of clauses");
        return nullptr;
    }


    // read CNF formula

    std::unique_ptr<Formula> f = std::make_unique<Formula>(arguments.ignore_dimacs_header ? 0 : num_var);

    while (!f->contains_empty() // return if `f` contains an empty clause
            && (arguments.ignore_dimacs_header // !arguments.ignore_dimacs_header =>
                || (f->var_count() <= num_var // return if too many variables
                    && f->clause_count() <= num_clauses)) // return if too many clauses
            && is >> std::ws // delete leading whitespace (return if a read error occured)
            && !is.eof() // return if we are at end of file
          ) {
        if (is.peek() == 'c') { // comment line, ignore
            ignore_line(is);
            continue;
        }

        std::vector<Literal> clause {};

        for (svar_t lit; is >> lit && lit != 0;) // read literals (until read error or 0)
            clause.push_back(lit);

        f->add(std::move(clause));
    }

    if (!arguments.ignore_dimacs_header && f->var_count() > num_var) {
        solver_error_output("too many variables");
        return nullptr;
    } else if (!arguments.ignore_dimacs_header && f->clause_count() > num_clauses) {
        solver_error_output("too many clauses");
        return nullptr;
    } else if (!f->contains_empty() && !is.eof()) { // a read error occured
        solver_error_output("unable to read clauses");
        return nullptr;
    }

    return f;
}

/* Output a solution
 *
 * prints the v-block excluding the starting 'v' but including the ending '0'
 */
std::ostream& operator<<(std::ostream& os, Solution const& s) {
    for (var_t var = 1; var <= s.var_count(); ++var)
        os << s.lit(var) << ' ';

    return os << '0';
}
