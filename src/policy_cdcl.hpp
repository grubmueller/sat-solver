/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef POLICY_CDCL_HPP
#define POLICY_CDCL_HPP

#include "solver_cdcl_tools.hpp"


/* Policy_No
 * Dummy class for no restart policy
 */ 
class Policy_No {
public:
    static constexpr bool report_conflict() noexcept {
        return false;
    }
    static constexpr std::size_t restart_counter() noexcept {
        return 0;
    }
};

/* Policy_Yes
 * This is the base class for all actual Policies
 * 
 * The final derived class should be passed as `Derived` (-> CRTP to avoid runtime polymorphism)
 * It should implement the following member functions:
 * * void update_threshold()
 * * * This function should update the restart threshold according to the restart policy
 */ 
template <typename Derived>
class Policy_Yes {
public:
    // No copies, just move construction
    Policy_Yes(Policy_Yes const&) = delete;
    Policy_Yes(Policy_Yes&&) = default;
    Policy_Yes& operator = (Policy_Yes const&) = delete;

    std::size_t restart_counter() const noexcept {
        return restart_counter_;
    }

    // call at conflict, returns true if should restart, false otherwise
    bool report_conflict() noexcept {
        if (++conflicts_since_last_restart_ < threshold_) { // no restart
            return false;
        } else { // restart
            conflicts_since_last_restart_ = 0;
            ++restart_counter_;
            derived().update_threshold();
            return true;
        }
    }

protected:
    std::size_t threshold_;

    explicit Policy_Yes(std::size_t const threshold) noexcept :
        threshold_(threshold) {}

private:
    std::size_t conflicts_since_last_restart_ {};
    std::size_t restart_counter_ {};

    // convert `this` to object of derived class (CRTP) and return as a reference
    Derived& derived() noexcept {
        return *static_cast<Derived*>(this);
    }
};

/* Policy_Geometric
 * Restart Policy implementing the Geometric Policy
 * 
 * The threshold starts at a specified starting point.
 * On restart the threshold gets multiplied by a specified factor.
 */ 
class Policy_Geometric : public Policy_Yes<Policy_Geometric> {
    friend Policy_Yes<Policy_Geometric>;

public:
    static Policy_Geometric Fixed_from_arguments();
    static Policy_Geometric from_arguments();

    explicit Policy_Geometric(std::size_t const c, double const f = 1) noexcept :
        Policy_Yes<Policy_Geometric>(c), f_(f) {}

private:
    double const f_;

    // multiply current threshold by specified factor
    void update_threshold() noexcept {
        this->threshold_ = static_cast<std::size_t>(static_cast<double>(this->threshold_) * f_);
    }
};

/* Policy_Luby
 * Restart Policy implementing the Luby Policy
 * 
 * The threshold is set as a fixed value and multiplied by the next element of the Luby_Iterator subclass.
 */ 
class Policy_Luby : public Policy_Yes<Policy_Luby> {
    friend Policy_Yes<Policy_Luby>;

public:
    static Policy_Luby from_arguments();

    explicit Policy_Luby(std::size_t const c) noexcept :
        Policy_Yes<Policy_Luby>(c), c_(c) {}

private:
    /* Luby sequence iterator using reluctant doubling
     * source: https://baldur.iti.kit.edu/sat/files/2018/l06.pdf, p. 17
     * initially devised by D. Knuth
     */
    class Luby_Iterator {
    public:
        // go to next element and return it
        std::size_t next_elem() {
            if ((u_ & -u_) == v_) {
                ++u_;
                v_ = 1;
            } else {
                v_ *= 2;
            }

            return v_;
        }

    private:
        std::size_t u_ {};
        std::size_t v_ {};
    } luby_ {};

    std::size_t const c_;

    // set threshold to predetermined constant multiplied by the next element of the luby sequence
    void update_threshold() noexcept {
        this->threshold_ = c_ * luby_.next_elem();
    }
};

/* Inner/Outer restart policy
 * conflicts until next restart (inner threshold) increase until an outer threshold is reached
 * afterwards reset the inner threshold and increase the outer threshold
 * source: PicoSAT Essentials, A. Biere; http://fmv.jku.at/papers/Biere-JSAT08.pdf
 */
class Policy_Inner_Outer : public Policy_Yes<Policy_Inner_Outer> {
    friend Policy_Yes<Policy_Inner_Outer>;

public:
    static Policy_Inner_Outer from_arguments();

    explicit Policy_Inner_Outer(std::size_t const c, double const f) noexcept :
        Policy_Yes<Policy_Inner_Outer>(c), c_(c), f_(f), outer_threshold_(c) {}

private:
    std::size_t const c_;
    double const f_;
    std::size_t outer_threshold_;

    /* if current `threshold_` is bigger than the `outer_threshold_` increase the latter by the the
     * predetermined factor and reset the `threshold_` to the predetermined constant, otherwise
     * increase the `threshold_` by the factor instead.
     */ 
    void update_threshold() noexcept {
        if (this->threshold_ > outer_threshold_) {
            outer_threshold_ = static_cast<std::size_t>(static_cast<double>(outer_threshold_) * f_);
            this->threshold_ = c_;
        } else {
            this->threshold_ = static_cast<std::size_t>(static_cast<double>(this->threshold_) * f_);
        }
    }
};

#endif // POLICY_CDCL_HPP
