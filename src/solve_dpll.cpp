/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "solve.hpp"
#include "solver_dpll.hpp"
#include "heuristics_dpll.hpp"


namespace { // helper functions to set template parameters
template <typename Heu, bool UP, bool PL>
inline auto solve_dpll_helper(std::unique_ptr<Formula>&& f) {
    return solve_do<Solver_DPLL<Heu, UP, PL>>(std::move(f));
}

template <typename Heu, bool UP>
inline auto solve_dpll_helper_PL(std::unique_ptr<Formula>&& f, bool const pure_lit) {
    return pure_lit ? solve_dpll_helper<Heu, UP, true>(std::move(f))
           : solve_dpll_helper<Heu, UP, false>(std::move(f));
}

template <typename Heu>
inline auto solve_dpll_helper_UP(std::unique_ptr<Formula>&& f, bool const unit_prop,
                                 bool const pure_lit_elim) {
    return unit_prop ? solve_dpll_helper_PL<Heu, true>(std::move(f), pure_lit_elim)
           : solve_dpll_helper_PL<Heu, false>(std::move(f), pure_lit_elim);
}
}

/* Solve formula using the DPLL-Solver
 *
 * returns solution and statistics
 *
 * preconditions:
 * * `f` is not empty and does not contain an empty clause
 * * `heuristic` is a valid DPLL-Heuristic
 */
Solution_Statistics_Pair solve_dpll(std::unique_ptr<Formula>&& f, Heuristic const heuristic,
                                    bool const unit_prop, bool const pure_lit_elim) {
    switch (heuristic) {
    case Heuristic::SLIS:
        return solve_dpll_helper_UP<Heuristic_SLIS>(std::move(f), unit_prop, pure_lit_elim);
    case Heuristic::SLCS:
        return solve_dpll_helper_UP<Heuristic_SLCS>(std::move(f), unit_prop, pure_lit_elim);
    case Heuristic::DLIS:
        return solve_dpll_helper_UP<Heuristic_DLIS>(std::move(f), unit_prop, pure_lit_elim);
    case Heuristic::DLCS:
        return solve_dpll_helper_UP<Heuristic_DLCS>(std::move(f), unit_prop, pure_lit_elim);
    case Heuristic::BOEHM:
        return solve_dpll_helper_UP<Heuristic_Boehm>(std::move(f), unit_prop, pure_lit_elim);
    case Heuristic::JW:
        return solve_dpll_helper_UP<Heuristic_JW>(std::move(f), unit_prop, pure_lit_elim);
    default:
        return {};
    }
}
