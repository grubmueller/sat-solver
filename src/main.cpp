/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "arguments.hpp"
#include "solve.hpp"
#include "proof_logging.hpp"
#include "printing.hpp"

#include <iostream> // std::cin, std::cout
#include <fstream> // std::ifstream, std::ofstream
#include <new> // std::bad_alloc
#include <cstdlib> // EXIT_SUCCESS, EXIT_FAILURE
#include <cerrno> // errno
#include <cstring> // std:strerror
#include <unistd.h> // isatty, STDOUT_FILENO, STDIN_FILENO


namespace {
/* Helper function for solving, creates the proof logger if activated.
 * calls `solve_print` with either a pointer to the generated proof logger or nullptr
 *
 * returns the exit code of the program (EXIT_FAILURE in case of error)
 */
int solve_help(std::unique_ptr<Formula>&& f, Formatting const& formatting, std::ostream& os) {
    if (arguments.proof) { // write proof
        if (arguments.proof_file) { // write proof to arguments.proof_file
            std::ofstream pr_os {arguments.proof_file};

            if (!pr_os) {
                solver_error_output("failed to open file '", arguments.proof_file, '\'');
                solver_error_output_additional(std::strerror(errno));
                return EXIT_FAILURE;
            }

            Proof_Logger pl {pr_os};
            return solve_print(std::move(f), formatting, os, &pl);
        } else { // write proof to standard output
            Proof_Logger pl {std::cout};
            return solve_print(std::move(f), formatting, os, &pl);
        }
    } else { // do not write proof
        return solve_print(std::move(f), formatting, os);
    }
}
}

int main(int argc, char* argv[]) {
    switch (parse_arguments(argc, argv)) { // parse command line arguments
    case ARGERR:
        return EXIT_FAILURE;
    case ARGEXIT:
        return EXIT_SUCCESS;
    }

    bool const stdout_terminal = isatty(STDOUT_FILENO); // is standard output a terminal?

    /// use colour formatting if standard output is a terminal
    Formatting formatting {stdout_terminal ? "\033[1;36m" : "", // colour cyan
                           stdout_terminal ? "\033[0m" : "", 80}; // heading width: 80 characters

    print_general(formatting);

    try {
        std::unique_ptr<Formula> f;

        if (arguments.input_file) { // read formula from arguments.input_file
            std::ifstream is {arguments.input_file};

            if (!is) {
                solver_error_output("failed to open file '", arguments.input_file, '\'');
                solver_error_output_additional(std::strerror(errno));
                goto exit_failure;
            }

            f = read_dimacs(is);
        } else { // read formula from standard input
            bool const input_header = stdout_terminal && isatty(STDIN_FILENO) ;

            // print input header if standard output and standard input are terminals
            if (input_header)
                print_section_heading(formatting, "input");

            f = read_dimacs(std::cin);

            if (input_header)
                solver_non_quiet_output();
        }

        if (!f) // error reading f
            goto exit_failure;

        int ret;

        if (arguments.output_file) { // write solution to arguments.output_file
            std::ofstream os {arguments.output_file};

            if (!os) {
                solver_error_output("failed to open file '", arguments.output_file, '\'');
                solver_error_output_additional(std::strerror(errno));
                goto exit_failure;
            }

            ret = solve_help(std::move(f), formatting, os);
        } else { // write solution to standard output
            ret = solve_help(std::move(f), formatting, std::cout);
        }

        print_exit(formatting, ret);
        return ret;

    } catch (std::bad_alloc const&) {
        solver_error_output("unable to allocate memory");
        solver_error_output_additional("consider upgrading your hardware");
        goto exit_failure;
    }

exit_failure:
    print_exit(formatting, EXIT_FAILURE);
    return EXIT_FAILURE;
}
