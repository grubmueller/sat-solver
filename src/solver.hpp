/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SOLVER_HPP
#define SOLVER_HPP

#include "solver_tools.hpp"


/* Abstract class Solver, intended as a base class for Solver_DPLL and Solver_CDCL.
 * Contains the assignments, variables and clauses as protected members.
 */
template <Solver_Type ST>
class Solver {
public:
    // No copies or moves
    Solver(Solver const&) = delete;
    Solver& operator=(Solver const&) = delete;

    // Get the assignment stack
    Assignments<ST> const& alpha() const noexcept {
        return alpha_;
    }

    // Get the overall number of variables
    var_t var_count() const noexcept {
        return static_cast<var_t>(vars_.size()) - 1;
    }

    // Get the (original) number of clauses
    std::size_t clause_count() const noexcept {
        return clauses_.size();
    }

    // Get the (original) maximal clause width
    width_t max_clause_width() const noexcept {
        return max_clause_width_;
    }

protected:
    Assignments<ST> alpha_;
    Variables<ST> vars_;
    Clauses<ST> clauses_ {};
    width_t max_clause_width_;

    Solver(Formula&& f) :
        alpha_(f.var_count()),
        vars_(f.var_count() + 1), // resize according to original formula
        max_clause_width_(f.max_clause_width())
    {
        // loop through original formula and move clauses
        for (Formula::Clause& c : f)
            clauses_.emplace_back(std::move(c));
    }
};

#endif // SOLVER_HPP
