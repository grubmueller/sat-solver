/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "printing.hpp"
#include "config.hpp"
#include "statistics.hpp"
#include "solution.hpp"

#include <iomanip> // std::setw, std::left, std::right


namespace {
template <typename... Args>
inline void print_use(bool const use, Args const&... args) {
    solver_non_quiet_output(use ? "Using" : "Not using", ' ', args...);
}

template <typename... Args>
inline void print_use(Args const&... args) {
    solver_non_quiet_output("Using ", args...);
}

template <typename... Args>
inline void print_nuse(Args const&... args) {
    solver_non_quiet_output("Not using ", args...);
}

inline unsigned count_digits10(std::size_t i) noexcept {
    unsigned res = 1;

    for (; i >= 10; i /= 10)
        ++res;

    return res;
}
}

// print a heading in the style of "c ———— < heading > ————————————————"
void print_section_heading(Formatting const& f, std::string_view const heading) {
    std::size_t const hwidth = heading.length() + 16; // "c ———— < heading > ————"
    std::size_t const wwidth = f.section_header_width <= hwidth ? 4 : 4 + f.section_header_width - hwidth;

    std::string dashstr {};
    dashstr.reserve((sizeof("—") - 1) * wwidth);

    for (std::size_t i = 0; i < wwidth; ++i)
        dashstr += "—";

    solver_non_quiet_output(f.colour_marker_section_begin,
                            "———— < ", heading, " > ", dashstr,
                            f.colour_marker_section_end);
    solver_non_quiet_output();
}

// print general settings (input, output, proof)
void print_general(Formatting const& f) {
    print_section_heading(f, "general");

    if (arguments.input_file)
        solver_non_quiet_output("Loading DIMACS from '", arguments.input_file, '\'');
    else
        solver_non_quiet_output("Loading DIMACS from standard input");

    if (arguments.output_file)
        solver_non_quiet_output("Writing solution to '", arguments.output_file, '\'');
    else
        solver_non_quiet_output("Writing solution to standard output");

    if (arguments.solver_type == Solver_Type::CDCL) {
        if (arguments.proof) {
            if (arguments.proof_file)
                solver_non_quiet_output("Writing proof to '", arguments.proof_file, '\'');
            else
                solver_non_quiet_output("Writing proof to standard output");
        } else {
            solver_non_quiet_output("Not writing a proof");
        }
    }

    solver_non_quiet_output();
}

void print_no_preprocessor(Formatting const& f) {
    print_section_heading(f, "preprocessor settings");
    solver_non_quiet_output("Not using any preprocessing");
    solver_non_quiet_output();
}

// print preprocessor settings
void print_preprocessor_settings(Formatting const& f) {
    print_section_heading(f, "preprocessor settings");

    print_use(arguments.subsumed, "subsumed clause elimination");

    if (arguments.bce) {
        print_use("blocked clause elimination");
    } else {
        print_nuse("blocked clause elimination");
        print_use(arguments.pure_lit_elim, "pure literal elimination");
    }

    print_use(arguments.holes, "hole elimination");

    solver_non_quiet_output();
}

// print solver settings
void print_solver_settings(Formatting const& f) {
    print_section_heading(f, "solver settings");

    print_use("Solver of type ", type_name(arguments.solver_type));
    print_use("branching heuristic ", heuristic_name(arguments.heuristic));

    switch (arguments.solver_type) {
    case Solver_Type::DPLL:
        print_use(arguments.unit_prop, "unit propagation");
        print_use(arguments.pure_lit_elim, "pure literal elimination");
        break;
    case Solver_Type::CDCL:
        // learning scheme
        print_use("learning scheme ", scheme_name(arguments.scheme));

        // clause deletion strategy
        std::string const strategy_str = std::string{"clause deletion strategy "}
                                         + strategy_name(arguments.strategy);
        switch (arguments.strategy) {
        case Strategy::SIMPLE:
            print_use(strategy_str, " with capacity ", arguments.strategy_simple_c);
            break;
        case Strategy::BOUNDED:
            print_use(strategy_str, " with small clause threshold ", arguments.strategy_bounded_k);
            solver_non_quiet_output("      and unassignment threshold ", arguments.strategy_bounded_m);
        }

        // restart policy
        std::string const policy_str = std::string{"restart policy "}
                                       + policy_name(arguments.policy);
        switch (arguments.policy) {
        case Policy::FIXED:
            print_use(policy_str, " with interval length ", arguments.policy_fixed_c);
            break;
        case Policy::GEOMETRIC:
            print_use(policy_str, " with interval length ", arguments.policy_geometric_c,
                      " and factor ", arguments.policy_geometric_f);
            break;
        case Policy::LUBY:
            print_use(policy_str, " with base interval ", arguments.policy_luby_c);
            break;
        case Policy::INNER_OUTER:
            print_use(policy_str, " with base inner interval ", arguments.policy_io_c,
                      " and factor ", arguments.policy_io_f);
            break;
        case Policy::NO:
            print_nuse("restarts");
        }

        // phase saving
        print_use(arguments.phase_saving, "phase saving");
    }

    solver_non_quiet_output();
}

// print statistics
void print_statistics(Formatting const& f, Statistics const& stats, std::string_view const& heading) {
    std::size_t maxwidth_descr = 0;
    unsigned maxwidth_value = 0;

    for (Statistics_pair const& stat : stats) {
        if (std::size_t const temp = stat.first.size(); temp > maxwidth_descr)
            maxwidth_descr = temp;

        if (unsigned const temp = count_digits10(stat.second); temp > maxwidth_value)
            maxwidth_value = temp;
    }

    ++maxwidth_descr;

    print_section_heading(f, heading);

    for (auto const& [descr,value]  : stats)
        solver_non_quiet_output(std::setw(static_cast<int>(maxwidth_descr)), std::left,
                                descr.empty() ? "" : descr + ':', "  ",
                                std::setw(static_cast<int>(maxwidth_value)), std::right, value);

    solver_non_quiet_output();
}


// print solution (print s- and v-line)
void print_solution(Formatting const& f, std::ostream& os, std::optional<Solution> const& sol) {
    print_section_heading(f, "solution");

    if (sol) {
        solver_non_quiet_output_char('s', "SATISFIABLE");
        solver_non_quiet_output_char('v', *sol);

        if (arguments.output_file || arguments.quiet) // if quiet is activated but no output file is specified, print result to std::cout (== os in this case)
            os << "s SATISFIABLE\n"
               << 'v' << ' ' << *sol << '\n';
    } else {
        solver_non_quiet_output_char('s', "UNSATISFIABLE");

        if (arguments.output_file || arguments.quiet) // see comment above
            os << "s UNSATISFIABLE\n";
    }

    solver_non_quiet_output();
}

void print_exit(Formatting const& f, int const ret) {
    print_section_heading(f, "exit");

    solver_non_quiet_output("return with exit code ", ret);
}
