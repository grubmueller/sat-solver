/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "preprocessing.hpp"
#include "proof_logging.hpp"
#include "debug.hpp"

#include <queue>
#include <set>
#include <map>
#include <numeric> // std::accumulate
#include <algorithm> // std::includes, std::move
#include <iterator> // std::back_inserter
#include <cstdint> // std::uint64_t


using Indices = std::set<std::size_t, std::greater<std::size_t>>;
using Occurences = std::vector<Indices> [2];

namespace {
// array of bools so one can loop over `bools`: for (bool const b : bools)
constexpr bool bools[2] {false, true};

/* Delete a clause
 * logs `f[cls]` as deleted, marks `f[cls]` as deleted (insert into `to_delete`) and removes
 * occurences of `f[cls]`
 *
 * precondition: `f[cls]` was not marked as deleted
 */
inline void del(Formula const& f, std::size_t const cls, Occurences& occ, Indices& to_delete,
                Proof_Logger* const pl) {
    solver_assert(!to_delete.count(cls)); // assert precondition

    if (pl) // log `f[cls]` as deleted
        pl->log(true, f[cls].lits());

    to_delete.insert(cls); // mark `f[cls]` as deleted

    for (Literal const l : f[cls]) // remove occurences of `f[cls]`
        occ[l.val][l.var].erase(cls);
}

/* Subsumed clause elimination
 * Preprocessing technique that eliminates subsumed clauses, meaning clauses that are a superset of
 * another clause in the formula.
 *
 * deletes all subsumed clauses
 * does not need postprocessing
 *
 * returns the number of eliminated clauses.
 */
std::size_t sce(Formula const& f, Occurences& occ, Indices& to_delete, Proof_Logger* const pl) {
    std::size_t counter {};

    std::deque<std::uint64_t> sig {};

    for (Formula::Clause const& c : f)
        // fill `sig` with bitwise or of 2^{l.var % 64} for all literals `l` in the clause
        sig.push_back(std::accumulate(c.cbegin(), c.cend(), 0ull,
                      [] (std::uint64_t const sig, Literal const l) {
                        return sig | (1ull << (l.var % 64));
                   }));

    std::vector<std::size_t> inter_occ; // intersection of `front_occ` and `back_occ`, see below

    for (std::size_t i = 0; i < f.clause_count(); ++i)
        if (!to_delete.count(i)) { // if `f[i]` is not marked as deleted
            Indices& front_occ {occ[f[i].front().val][f[i].front().var]};
            Indices& back_occ {occ[f[i].back().val][f[i].back().var]};

            // compute intersection of `front_occ` and `back_occ` and write it to `inter_occ`
            std::set_intersection(front_occ.cbegin(), front_occ.cend(),
                                  back_occ.cbegin(), back_occ.cend(),
                                  std::back_inserter(inter_occ), std::greater<std::size_t>{});


            // loop through occurences of the first and last literal of `f[i]`
            for (std::size_t const j : inter_occ)
                if (i != j && (sig[i] & ~sig[j]) == 0ull
                        && std::includes(f[j].cbegin(), f[j].cend(), f[i].cbegin(), f[i].cend())
                   ) { // `f[j]` is subsumed by `f[i]` -> delete `f[j]`
                    ++counter;
                    solver_log("found subsumed clause ", j);

                    del(f, j, occ, to_delete, pl);
                }

            inter_occ.clear();
        }

    return counter;
}

/* Pure literal elimination
 * Preprocessing technique that eliminates pure literals, meaning literals that only occur positive
 * or negative in the formula.
 *
 * deletes all clauses with the pure literal
 * adds a unit clause with the pure literal
 * does not need postprocessing
 *
 * returns the number of eliminated literals
 */
std::size_t ple(Formula& f, Occurences& occ, Indices& to_delete, Proof_Logger* const pl) {
    std::size_t counter {};

    // pure literal queue
    std::queue<Literal> pure {};

    // collect pure literals
    for (var_t var = 1; var <= f.var_count(); ++var)
        for (bool const b : bools)
            if (!occ[b][var].empty() && occ[!b][var].empty())
                pure.push(Literal{b, var});

    while (!pure.empty()) {
        ++counter;

        Literal const lit = pure.front();
        pure.pop();

        solver_log("found pure literal ", lit);

        // add unit clause with `lit`
        Literals unit {lit};

        if (pl)
            pl->log(false, unit);

        f.add(std::move(unit));

        // delete clauses
        while (!occ[lit.val][lit.var].empty()) {
            std::size_t const i = *occ[lit.val][lit.var].cbegin();
            del(f, i, occ, to_delete, pl);

            // collect new pure literals
            for (Literal const l : f[i])
                if (occ[l.val][l.var].empty() && !occ[!l.val][l.var].empty())
                    // new pure literal
                    pure.push(l.flip());
        }

        occ[lit.val][lit.var].insert(f.clause_count() - 1); // add unit clause occurence
    }

    return counter;
}

/* Check if a clause is blocked by a literal
 *
 * returns true if `f[cls]` is blocked by `lit`, false otherwise
 */
inline bool blocked_by(Formula const& f, std::size_t const cls, Literal const lit,
                       Occurences const& occ) {
    // `f[cls]` blocked by `lit` => resolution with all `occ[!lit.val][lit.var]` is a tautology
    return std::all_of(occ[!lit.val][lit.var].cbegin(), occ[!lit.val][lit.var].cend(),
        [&f, cls, lit] (std::size_t const i) {
            // check if resolution of `f[cls]` and `f[i]` by `lit` is a tautology
            // => there is a literal `l` != `lit` in `f[i]` that occurs flipped in `f[cls]`
            auto lb = f[cls].cbegin(); // lower bound

            for (Literal const l : f[i]) // loop through literals in `f[i]` (`f[i]` is sorted)
                if (Literal const flipped = l.flip(); lit != flipped) {
                    // get the lower bound (i.e. the first literal >= `flipped`)
                    lb = std::lower_bound(lb, f[cls].cend(), flipped);

                    if (lb == f[cls].cend()) // there is no literal >= `flipped`
                        return false; // => resolution cannot be a tautology
                    else if (*lb == flipped) // found `flipped`
                        return true; // => resolution is a tautology
                }

            // no such literal `l` found => resolution is not a tautology
            return false;
        }
    );
}

/* Blocked clause elimination
 * Preprocessing technique that eliminates blocked clauses, meaning clauses that are a tautology
 * after resolution with all other clauses in the formula that contain the inverse of the blocking
 * literal.
 *
 * deletes all blocked clauses, moves them into `blocked` (with the blocking literal)
 * needs postprocessing with `blocked`
 *
 * returns the number of blocked clauses eliminated
 *
 * Reference: Matti Järvisalo, Armin Biere, and Marijn Heule: Blocked Clause Elimination
 * http://fmv.jku.at/papers/JarvisaloBiereHeule-TACAS10.pdf
 * Implementation, page 11f.
 */
std::size_t bce(Formula const& f, Occurences& occ, Indices& to_delete, Proof_Logger* const pl,
                std::deque<std::pair<Literal, Literals>>& blocked) {
    std::size_t counter {};

    std::set<Literal> touched {}; // set of literals to consider

    // loop through all literals
    for (var_t var = 1; var <= f.var_count(); ++var)
        for (bool const b : bools) {
            Literal const lit {b, var};

            for (auto occ_it = occ[b][var].cbegin(); occ_it != occ[b][var].cend();)
                if (std::size_t const i = *occ_it; blocked_by(f, i, lit, occ)) {
                    ++counter;
                    solver_log("found blocked clause ", i, " (blocked by literal ", lit, ')');

                    // touch flipped literals
                    for (Literal const l : f[i])
                        if (l.flip() < lit)
                            touched.insert(l.flip());

                    // delete `f[i]`
                    occ_it = occ[b][var].erase(occ_it);
                    del(f, i, occ, to_delete, pl);

                    // save blocked clause for postprocessing
                    blocked.emplace_back(lit, std::move(f[i]).lits());
                } else {
                    ++occ_it;
                }
        }

    // loop through all touched literals
    while (!touched.empty()) {
        Literal const lit = *touched.cbegin();
        touched.erase(touched.cbegin());

        for (auto occ_it = occ[lit.val][lit.var].cbegin(); occ_it != occ[lit.val][lit.var].cend();)
            if (std::size_t const i = *occ_it; blocked_by(f, i, lit, occ)) {
                ++counter;
                solver_log("found blocked clause ", i, " (blocked by literal ", lit, ')');

                // touch flipped literals
                for (Literal const l : f[i])
                    touched.insert(l.flip());

                // delete `f[i]`
                occ_it = occ[lit.val][lit.var].erase(occ_it);
                del(f, i, occ, to_delete, pl);

                // save blocked clause for postprocessing
                blocked.emplace_back(lit, std::move(f[i]).lits());
            } else {
                ++occ_it;
            }
    }

    return counter;
}

/* Hole elimination
 * Preprocessing technique that fills holes, meaning variables that are not used and thereby
 * irrelevant to finding the solution but could appear within the range specified in the header,
 * in the formula.
 *
 * if there are holes, write the translation from new to old into `translation`
 * needs postprocessing with `translation`
 *
 * returns the number of holes filled
 */
std::size_t hole_elimination(Formula& f, Occurences const& occ, Proof_Logger* const pl,
                             std::vector<var_t>& translation) {
    var_t new_var_count = f.var_count();
    std::map<var_t, var_t> substitution {}; // old -> new

    // find last non-hole
    while (new_var_count && occ[false][new_var_count].empty() && occ[true][new_var_count].empty()) {
        solver_log("found hole ", new_var_count);
        --new_var_count;
    }

    // populate substitution

    for (var_t var = 1; var < new_var_count; ++var)
        if (occ[false][var].empty() && occ[true][var].empty()) {
            solver_log("found hole ", var);
            substitution.emplace(new_var_count--, var);

            // decrease `new_var_count` until non-hole
            while (new_var_count > var
                    && occ[false][new_var_count].empty() && occ[true][new_var_count].empty()
                  ) {
                solver_log("found hole ", new_var_count);
                --new_var_count;
            }
        }

    // check if there was a hole
    if (new_var_count == f.var_count()) {
        solver_assert(substitution.empty());
        solver_log("no hole found");

        return 0;
    }

    // init translation data structure (new -> old)

    translation.reserve(new_var_count + 1);
    translation.resize(1);

    for (var_t var = 1; var <= new_var_count; ++var)
        translation.push_back(var);


    Indices marked {}; // marked clauses

    // loop through all substitutions
    for (auto const [old_var, new_var] : substitution) {
        translation[new_var] = old_var; // populate `translation`

        // update occurences of `old_var` (to `new_var`)
        for (bool const b : bools)
            for (std::size_t const i : occ[b][old_var])
                if (marked.insert(i).second) { // if `i` was not marked before
                    Literals cls = std::move(f[i]).lits();

                    // substitute literals in clause
                    for (Literal& l : cls)
                        if (auto const it = substitution.find(l.var); it != substitution.end())
                            l = Literal{l.val, it->second};

                    f[i] = std::move(cls); // implicit conversion Literals -> Formula::Clause
                }
    }

    if (pl) // proof logger must log old variables
        pl->apply_translation(translation);

    std::size_t const counter = f.var_count() - new_var_count;
    f.reset_var_count(new_var_count);
    return counter;
}
}

/* Preprocess the formula according to parameters.
 *
 * Possible techniques:
 * * subsumed clause elimination (`subsumed`)
 * * blocked clause elimination (`blocked`)
 * * pure literal elimination (`pure`, only possible if blocked clause elimination isn't active)
 * * hole elimination (`holes`)
 *
 * stores statistics and data for postprocessor into `pre_data`,
 * logs deleted and learned clauses to `pl` if `pl` is not a null pointer.
 *
 * precondition: `f` is not empty and does not contain an empty clause
 */
void preprocess(Formula& f, Preprocessor_Data& pre_data,
                bool const subsumed, bool const blocked, bool const pure, bool const holes,
                Proof_Logger* const pl) {
    pre_data.original_var_count = f.var_count();
    width_t const original_max_clause_width = f.max_clause_width();

    // collect occurences
    Occurences occ {};

    for (bool const b : bools)
        occ[b].resize(f.var_count() + 1);

    for (std::size_t i = 0; i < f.clause_count(); ++i)
        for (Literal const l : f[i])
            occ[l.val][l.var].insert(i);

    Indices to_delete {};

    // subsumed clause elimination
    std::size_t sce_counter {0};

    if (subsumed)
        sce_counter += sce(f, occ, to_delete, pl);

    // blocked clause elmination and pure literal elimination
    std::size_t bce_counter {0};
    std::size_t ple_counter {0};

    if (blocked)
        bce_counter += bce(f, occ, to_delete, pl, pre_data.blocked);
    else if (pure)
        ple_counter += ple(f, occ, to_delete, pl);

    // hole elimination
    std::size_t hole_counter {0};

    if (holes)
        hole_counter += hole_elimination(f, occ, pl, pre_data.holes_translation);

    // remove deleted clauses
    for (std::size_t const i : to_delete)
        f.remove(i);

    // recompute maximal clause width if clauses were deleted
    if (!to_delete.empty())
        f.recompute_max_clause_width();

    solver_log("Preprocessing: DONE");


    // track statistics

    if (subsumed)
        pre_data.stats.emplace_back("Subsumed clauses eliminated", sce_counter);

    if (blocked)
        pre_data.stats.emplace_back("Blocked clauses eliminated", bce_counter);
    else if (pure)
        pre_data.stats.emplace_back("Pure literals eliminated", ple_counter);

    if (holes)
        pre_data.stats.emplace_back("Holes filled", hole_counter);

    pre_data.stats.emplace_back("Total eliminated clauses", to_delete.size());
    pre_data.stats.emplace_back("Reduction of maximal clause width", original_max_clause_width - f.max_clause_width());
}

/* Postprocess the solution (only needed in case of SATISFIABLE).
 * Postprocessing is needed for hole elimination and blocked clause elimination to generate
 * a satisfying assignment with the previously deleted clauses and renamed variables.
 *
 * precondition: `pre_data` was not modified between the call to `preprocess` and `postprocess`
 */
void postprocess(Solution& sol, Preprocessor_Data& pre_data) {
    // hole elimination postprocessing (restoring original variables)
    if (auto& translation {pre_data.holes_translation}; !translation.empty()) {
        solver_log("restoring original variables");

        Solution sol_new {pre_data.original_var_count};

        for (var_t var = 1; var <= sol.var_count(); ++var)
            sol_new.set(translation[var], sol.val(var));

        sol = std::move(sol_new);
    }

    /* Blocked clause elimination postprocessing
     *
     * Reference: Marijn Heule, Matti Järvisalo and Armin Biere:
     *      Clause Elimination Procedures for CNF Formulas
     * https://www.cs.utexas.edu/~marijn/publications/LPAR17.pdf
     * Reconstructing Solutions, page 12
     */
    for (auto& blocked {pre_data.blocked}; !blocked.empty(); blocked.pop_back())
        if (auto const& [lit,cls] = pre_data.blocked.back();
                !std::any_of(cls.cbegin(), cls.cend(),
                    [&sol] (Literal const l) {
                        return sol.val(l.var) == l.val;
                    })
           ) { // `sol` does not satisfy `cls` -> flip value of `lit.var`
            solver_log("flip ", sol.lit(lit.var));
            solver_assert(sol.lit(lit.var) == lit.flip());

            sol.set(lit);
        }

    solver_log("Postprocessing: DONE");
}
