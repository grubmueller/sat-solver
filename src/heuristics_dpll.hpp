/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HEURISTICS_DPLL_HPP
#define HEURISTICS_DPLL_HPP

#include "solver_dpll_tools.hpp"

#include <set> // std::set, std::multiset
#include <algorithm> // std::max
#include <cmath> // std::pow


/* Heuristic_eval
 * This is a pair of a literal and its heuristic value (of template parameter type T)
 */
template <typename T = std::size_t>
struct Heuristic_eval {
    Literal lit;
    T h;
};

template <typename T>
constexpr bool operator<(Heuristic_eval<T> const& l, Heuristic_eval<T> const& r) {
    return l.h < r.h;
}

template <typename T>
constexpr bool operator<=(Heuristic_eval<T> const& l, Heuristic_eval<T> const& r) {
    return l.h <= r.h;
}

template <typename T>
constexpr bool operator>(Heuristic_eval<T> const& l, Heuristic_eval<T> const& r) {
    return l.h > r.h;
}

template <typename T>
constexpr bool operator>=(Heuristic_eval<T> const& l, Heuristic_eval<T> const& r) {
    return l.h >= r.h;
}

/* Heuristic_DPLL
 * This is the base class for all DPLL-Heuristics
 *
 * If `Combined` is true `Elem` is the type `var_t`, otherwise `Elem` is the type `Literal`.
 * This allows distinction between literal and variable based heuristics
 *
 * `T` is the type of the heuristic
 * If `T` is convertible to `bool`, true should mean valid and false should mean invalid heuristic
 *
 * The final derived class should be passed as `Derived` (-> CRTP to avoid runtime polymorphism)
 * It should implement the following member functions:
 * * void collect_changed(Literal, Changed&, Variables const&)
 * * * This function should insert all elements (of type `Elem`) into the container of type
 * * * `Changed`, which heuristics could have been changed by changing the value of the literal
 * * Heuristic_eval compute_heu(Elem, Variables const&)
 * * * This function should compute the heuristic for the given element (of type `Elem`)
 * * bool is_valid_heu(T const&)
 * * * This function should only be implemented if `T` is not convertible to `bool`.
 * * * This function should return true if the heuristic is valid and false otherwise
 */
template <bool Combined, typename Derived, typename T = std::size_t>
class Heuristic_DPLL {
protected:
    using Heuristic_eval = ::Heuristic_eval<T>;
    using Variables = ::Variables<DPLL>;
    using Elem = std::conditional_t<Combined, var_t, Literal>;
    using Changed = std::set<Elem>;

private:
    using Heuristics = std::multiset<Heuristic_eval, std::greater<Heuristic_eval>>;
    using Var2Heu = std::conditional_t<Combined, std::vector<typename Heuristics::iterator>,
                                                 std::vector<typename Heuristics::iterator>[2]>;
public:
    // Standard constructor (call init!)
    Heuristic_DPLL() = default;

    // No copies or moves
    Heuristic_DPLL(Heuristic_DPLL const&) = delete;
    Heuristic_DPLL& operator=(Heuristic_DPLL const&) = delete;

    /* Get the literal with the highest heuristic
     *
     * returns an invalid literal if all variables are set
     */
    Literal top() const {
        return heu_.empty() ? Literal{} : heu_.begin()->lit;
    }

    /* Check if solver is finished
     *
     * returns true if all variables are set or all literals have invalid heuristic, otherwise false
     */
    bool finished() const {
        return heu_.empty() || !valid_heu(heu_.begin()->h);
    }

    /* Initialize the heuristic
     *
     * precondition: init() is called only once after construction
     */
    void init(Variables const& vars) {
        // resize to `vars.size()`
        if constexpr (Combined)
            var2heu_.resize(vars.size());
        else
            for (bool const b : bools)
                var2heu_[b].resize(vars.size());

        for (var_t i = 1; i < vars.size(); ++i)
            insert(i, vars); // insert heuristic
    }

    /* Update heuristics
     * Call this function after changing variables and before calling top() or finished()
     *
     * preconditions:
     * * for each literal `l` in `va_set`:
     *      `l` is valid and `l` was not set before or `l.flip()` is in `va_unset`
     * * for each literal `l` in `va_unset`:
     *      `l` is valid and `l` was set before
     */
    template <typename Literals>
    void update(Literals const& va_set, Literals const& va_unset, Variables const& vars) {
        Changed changed {};

        for (Literal const a : va_unset) {
            solver_assert(a); // assert precondition: `a` is valid
            solver_assert(is_set(a.var)); // assert precondition: `a` was set before

            insert(a.var, vars); // insert heuristic
            derived().collect_changed(a, changed, vars);
        }

        for (Literal const a : va_set) {
            solver_assert(a); // assert precondition: `a` is valid
            // assert precondition: `a` was not set before or `a.flip()` is in `va_unset`
            solver_assert(is_unset(a.var));

            erase(a.var); // erase heuristic
            derived().collect_changed(a, changed, vars);
        }

        update_changed(changed, vars); // update changed heuristics
    }

protected:
    // helper function, returns `l` or `l.var` depending on `Combined`
    static Elem lit2elem(Literal const l) noexcept {
        if constexpr (Combined)
            return l.var;
        else
            return l;
    }

    // helper function, returns `elem` or `elem.var` depending on `Combined`
    static var_t elem2var(Elem const elem) noexcept {
        if constexpr (Combined)
            return elem;
        else
            return elem.var;
    }

    // helper function, returns true if the heuristic is valid and false otherwise
    bool valid_heu(T const& h) const {
        if constexpr (std::is_convertible_v<T, bool>)
            return h;
        else
            return derived().is_valid_heu(h);
    }

private:
    Heuristics heu_ {};
    Var2Heu var2heu_ {};

    // convert `this` to object of derived class (CRTP) and return as a reference
    Derived& derived() noexcept {
        return *static_cast<Derived*>(this);
    }

    // convert `this` to object of derived class (CRTP) and return as a constant reference
    Derived const& derived() const noexcept {
        return *static_cast<Derived const*>(this);
    }

    /* erase the heuristic for `var`
     *
     * precondition: `var` was not set before
     */
    void erase(var_t const var) {
        solver_assert(is_unset(var)); // assert precondition

        if constexpr (Combined) {
            heu_.erase(var2heu_[var]);
            var2heu_[var] = heu_.end();
        } else {
            for (bool const b : bools) {
                heu_.erase(var2heu_[b][var]);
                var2heu_[b][var] = heu_.end();
            }
        }
    }

    // insert the heuristic for `var`
    void insert(var_t const var, Variables const& vars) {
        // no `solver_assert(is_set(var))` because `insert()` is called in `init()`

        if constexpr (Combined)
            var2heu_[var] = heu_.insert(derived().compute_heu(var, vars));
        else
            for (bool const b : bools)
                var2heu_[b][var] = heu_.insert(derived().compute_heu(Literal{b, var}, vars));
    }

    /* update the heuristic for the element `elem`
     *
     * precondition: `elem` is not set
     */
    void update_elem(Elem const elem, Variables const& vars) {
        solver_assert(is_unset(elem2var(elem))); // assert precondition

        if constexpr (Combined) {
            heu_.erase(var2heu_[elem]);
            var2heu_[elem] = heu_.insert(derived().compute_heu(elem, vars));
        } else {
            heu_.erase(var2heu_[elem.val][elem.var]);
            var2heu_[elem.val][elem.var] = heu_.insert(derived().compute_heu(elem, vars));
        }
    }

    /* update the heuristic for all elements in `changed`
     *
     * precondition: all elements in `changed` are not set
     */
    void update_changed(Changed& changed, Variables const& vars) {
        for (Elem const elem : changed)
            update_elem(elem, vars);
    }

    // returns if `var` is set (for assertions)
    bool is_set(var_t const var) const noexcept {
        if constexpr (Combined)
            return var2heu_[var] == heu_.end();
        else
            return var2heu_[false][var] == heu_.end() && var2heu_[true][var] == heu_.end();
    }

    // returns if `var` is unset (for assertions)
    bool is_unset(var_t const var) const noexcept {
        if constexpr (Combined)
            return var2heu_[var] != heu_.end();
        else
            return var2heu_[false][var] != heu_.end() && var2heu_[true][var] != heu_.end();
    }
};

/* Heuristic_Collect_Nothing
 * This is the base class for DPLL-Heuristics where the heuristics of other elements don't change
 */
template <bool Combined, typename Derived, typename T = std::size_t>
class Heuristic_Collect_Nothing : public Heuristic_DPLL<Combined, Derived, T> {
protected:
    using Heuristic_base = Heuristic_DPLL<Combined, Derived, T>;
    using Heuristic_eval = typename Heuristic_base::Heuristic_eval;
    using Variables = typename Heuristic_base::Variables;
    using Elem = typename Heuristic_base::Elem;
    using Changed = typename Heuristic_base::Changed;
    using Heuristic_base::lit2elem;

protected:
    static void collect_changed(Literal, Changed&, Variables const&) {
        // do nothing
    }
};

/* Heuristic_Collect_Occ_Lit
 * This ist the base class for DPLL-Heuristics where only the heuristics in a clause containing the
 * changed literal can change
 */
template <bool Combined, typename Derived, typename T = std::size_t>
class Heuristic_Collect_Occ_Lit : public Heuristic_DPLL<Combined, Derived, T> {
protected:
    using Heuristic_base = Heuristic_DPLL<Combined, Derived, T>;
    using Heuristic_eval = typename Heuristic_base::Heuristic_eval;
    using Variables = typename Heuristic_base::Variables;
    using Elem = typename Heuristic_base::Elem;
    using Changed = typename Heuristic_base::Changed;
    using Heuristic_base::lit2elem;

protected:
    static void collect_changed(Literal const a, Changed& changed, Variables const& vars) {
        // loop through all clauses that contain `a`
        for (Clause<DPLL> const* const clause : vars[a.var].occ[a.val])
            // loop through all free literals in `clause` and add them to `changed`
            for (Literal const l : clause->lits)
                // check that `l` is free and not `a`
                if (vars[l.var].free() && l != a)
                    changed.insert(lit2elem(l));
    }
};

/* Heuristic_Collect_Occ_Var
 * This is the base class for DPLL-Heuristics where the heuristics in a clause containing either the
 * changed literal or the flipped literal can change
 */
template <bool Combined, typename Derived, typename T = std::size_t>
class Heuristic_Collect_Occ_Var : public Heuristic_DPLL<Combined, Derived, T> {
protected:
    using Heuristic_base = Heuristic_DPLL<Combined, Derived, T>;
    using Heuristic_eval = typename Heuristic_base::Heuristic_eval;
    using Variables = typename Heuristic_base::Variables;
    using Elem = typename Heuristic_base::Elem;
    using Changed = typename Heuristic_base::Changed;
    using Heuristic_base::lit2elem;

protected:
    static void collect_changed(Literal const a, Changed& changed, Variables const& vars) {
        for (bool const b : bools)
            // loop through all clauses that contain `a.var`
            for (Clause<DPLL> const* const clause : vars[a.var].occ[b])
                // loop through all free literals in `clause` and add them to `changed`
                for (Literal const l : clause->lits)
                    // check that `l.var` is free and not `a.var`
                    if (vars[l.var].free() && l.var != a.var)
                        changed.insert(lit2elem(l));
    }
};

/* Heuristic_SLIS
 * DPLL-Heuristic implementing Static largest individual sum
 *
 * The heuristic of a literal is the number of occurences in the original formula (thus static).
 *
 * The heuristic of a literal never changes, so we can derive from `Heuristic_Collect_Nothing`.
 */
class Heuristic_SLIS : public Heuristic_Collect_Nothing<false, Heuristic_SLIS> {
    using Heuristic_base = Heuristic_Collect_Nothing<false, Heuristic_SLIS>;
    using Heuristic_eval = typename Heuristic_base::Heuristic_eval;
    using Variables = typename Heuristic_base::Variables;

    friend typename Heuristic_base::Heuristic_base;

    static Heuristic_eval compute_heu(Literal const lit, Variables const& vars) {
        // number of occurences in the original formula
        return {lit, vars[lit.var].occ[lit.val].size()};
    }
};

/* Heuristic_SLCS
 * DPLL-Heuristic implementing Static largest combined sum
 *
 * The heuristic of a variable is the sum of occurences in the original formula (thus static).
 * We set the variable to true if the number of positive occurences is larger than the number of
 * negative occurences and false otherwise.
 *
 * The heuristic of a variable never changes, so we can derive from `Heuristic_Collect_Nothing`.
 */
class Heuristic_SLCS : public Heuristic_Collect_Nothing<true, Heuristic_SLCS> {
    using Heuristic_base = Heuristic_Collect_Nothing<true, Heuristic_SLCS>;
    using Heuristic_eval = typename Heuristic_base::Heuristic_eval;
    using Variables = typename Heuristic_base::Variables;

    friend typename Heuristic_base::Heuristic_base;

    static Heuristic_eval compute_heu(var_t const var, Variables const& vars) {
        auto const neg_occ = vars[var].occ[false].size(), // number of negative occurences
                   pos_occ = vars[var].occ[true].size();  // number of positive occurences

        return {Literal{pos_occ > neg_occ, var}, neg_occ + pos_occ};
    }
};

/* Heuristic_DLIS
 * DPLL-Heuristic implementing Dynamic largest individual sum
 *
 * The heuristic of a literal is the number of active occurences (thus dynamic).
 *
 * Only the heuristic of literals in a clause with changed literal can change (for this clause is
 * now inactive or active again), so we can derive from `Heuristic_Collect_Occ_Lit`.
 */
class Heuristic_DLIS : public Heuristic_Collect_Occ_Lit<false, Heuristic_DLIS> {
    using Heuristic_base = Heuristic_Collect_Occ_Lit<false, Heuristic_DLIS>;
    using Heuristic_eval = typename Heuristic_base::Heuristic_eval;
    using Variables = typename Heuristic_base::Variables;

    friend typename Heuristic_base::Heuristic_base;

    static Heuristic_eval compute_heu(Literal const lit, Variables const& vars) {
        // number of active occurences
        return {lit, vars[lit.var].act_occ[lit.val]};
    }
};


/* Heuristic_DLCS
 * DPLL-Heuristic implementing Dynamic largest combined sum
 *
 * The heuristic of a variable is the sum of active occurences (thus dynamic and combined).
 * We set the variable to true if the number of positive active occurences is larger than the number
 * of negative active occurences and false otherwise.
 *
 * Only the heuristic of variables in a clause with a changed literal can change (for this
 * clause is now inactive or active again), so we can derive from `Heuristic_Collect_Occ_Lit`.
 */
class Heuristic_DLCS : public Heuristic_Collect_Occ_Lit<true, Heuristic_DLCS> {
    using Heuristic_base = Heuristic_Collect_Occ_Lit<true, Heuristic_DLCS>;
    using Heuristic_eval = typename Heuristic_base::Heuristic_eval;
    using Variables = typename Heuristic_base::Variables;

    friend typename Heuristic_base::Heuristic_base;

    static Heuristic_eval compute_heu(var_t const var, Variables const& vars) {
        std::size_t const act_neg_occ = vars[var].act_occ[false], // number of negative occurences
                          act_pos_occ = vars[var].act_occ[true];  // number of positive occurences

        return {Literal{act_pos_occ > act_neg_occ, var}, act_neg_occ + act_pos_occ};
    }
};

/* Heuristic_Boehm
 * DPLL-Heuristic implementing Böhm's heuristic
 *
 * The heuristic of a variable is a vector v with the following property:
 * * {v[2*(i-1)], v[2*(i-1)+1]} is the number of active occurences of both literals with width i
 * * v[2*(i-1)] is the maximum, v[2*(i-1)+1] the minimum of the two
 * The next set variable is decided by a lexicographic ordering
 * We set the variable to the value for which the number of active occurences with width i is
 * greater, with i being the first width such that the two numbers are different
 * (i.e. the spirit of Böhm is also present in the choice of the branching direction)
 *
 * The heuristic of a variable changes if a clause C is now inactive or active again (the number of
 * active occurences changes) and if the width of a clause changes, so we have to derive from
 * `Heuristic_Collect_Occ_Var`.
 */
class Heuristic_Boehm : public Heuristic_Collect_Occ_Var<true, Heuristic_Boehm, std::vector<std::size_t>> {
    using Heuristic_base = Heuristic_Collect_Occ_Var<true, Heuristic_Boehm, std::vector<std::size_t>>;
    using Heuristic_eval = typename Heuristic_base::Heuristic_eval;
    using Variables = typename Heuristic_base::Variables;

    friend typename Heuristic_base::Heuristic_base;

    static Heuristic_eval compute_heu(var_t const var, Variables const& vars) {
        /* collect number of active occurences
         * `hs[b][i-1]` is the number of active occurences of `Literal{b, var}` with width `i`
         */
        std::vector<std::size_t> hs [2] {};

        for (bool const b : bools)
            // loop through all active clauses that contain `Literal{b, var}`
            for (Clause<DPLL> const* const clause : vars[var].occ[b])
                if (!clause->sat()) {
                    if (clause->active_lits > hs[b].size()) // resize `hs[b]` if necessary
                        hs[b].resize(clause->active_lits);

                    ++hs[b][clause->active_lits-1];
                }

        // get and set size of `hs[false]` and `hs[true]`
        std::size_t const hs_size = std::max(hs[false].size(), hs[true].size());
        hs[false].resize(hs_size);
        hs[true].resize(hs_size);


        /* compute the value
         * use the spirit of Böhm
         */
        bool val = false;

        for (std::size_t i = 0; i < hs_size; ++i)
            // check if the number of active occurences is different for the current width `i`
            if (hs[false][i] != hs[true][i]) {
                // get the value for which the number of active occurences is different
                val = hs[true][i] > hs[false][i];
                break;
            }


        // compute the heuristic
        std::vector<std::size_t> res {};
        res.reserve(2 * hs_size);

        for (std::size_t i = 0; i < hs_size; ++i) {
            // check for which value the number of active occurences with width `i` is greater
            bool const greater = hs[true][i] > hs[false][i];
            res.push_back(hs[greater][i]); // push the maximum
            res.push_back(hs[!greater][i]); // push the minimum
        }

        return {Literal{val, var}, std::move(res)};
    }

    static bool is_valid_heu(std::vector<std::size_t> const& h) noexcept {
        /* the heuristic is valid if there is an active occurence
         * in this case the heuristic cannot be empty
         */
        return !h.empty();
    }
};


/* Heuristic_JW
 * DPLL-Heuristic implementing Jeroslaw-Wang's heuristic
 *
 * The heuristic of a literal a is the sum of 2^{-w(C)} for all active clauses C with a in C.
 *
 * The heuristic of a literal changes if a clause is now inactive or active again (the sum will be
 * longer or shorter) and if the width of a clause changes (w(C) is different), so we have to derive
 * from `Heuristic_Collect_Occ_Var`.
 */
class Heuristic_JW : public Heuristic_Collect_Occ_Var<false, Heuristic_JW, double> {
    using Heuristic_base = Heuristic_Collect_Occ_Var<false, Heuristic_JW, double>;
    using Heuristic_eval = typename Heuristic_base::Heuristic_eval;
    using Variables = typename Heuristic_base::Variables;

    friend typename Heuristic_base::Heuristic_base;

    static Heuristic_eval compute_heu(Literal const lit, Variables const& vars) {
        double res {};

        // loop through all active clauses that contain `lit`
        for (Clause<DPLL> const* const clause : vars[lit.var].occ[lit.val])
            if (!clause->sat())
                // add 2^{-w(C)}
                res += std::pow(2, -static_cast<swidth_t>(clause->active_lits));

        return {lit, res};
    }
};


#endif //HEURISTICS_DPLL_HPP
