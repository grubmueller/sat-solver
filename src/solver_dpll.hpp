/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SOLVER_DPLL_HPP
#define SOLVER_DPLL_HPP

#include "solver.hpp"
#include "solver_dpll_tools.hpp"
#include "statistics.hpp"

#include <unordered_set>


template <typename Heu, bool UP, bool PL>
class Solver_DPLL : public Solver<DPLL>,
                    private Maybe<Units<DPLL>, UP>,
                    private Maybe<Pure_lits, PL>
{
    using Solver<DPLL>::alpha_;
    using Solver<DPLL>::vars_;
    using Solver<DPLL>::clauses_;
    using Unit = Maybe<Units<DPLL>, UP>;
    using Pure = Maybe<Pure_lits, PL>;

public:
    /* Construct a DPLL Solver from a formula
     *
     * precondition: `f` is not empty and does not contain an empty clause
     */
    explicit Solver_DPLL(Formula&& f) :
        Solver<DPLL>(std::move(f)),
        Unit(this->var_count())
    {
        // loop through clauses to set occurences
        for (Clause<DPLL>& clause : clauses_) {
            for (Literal const l : clause.lits) {
                vars_[l.var].occ[l.val].push_back(&clause); // add clause to list of occurences
                ++vars_[l.var].act_occ[l.val]; // increase active occurences counter
            }

            if constexpr (UP)
                if (clause.unit()) { // add unit literal
                    // we didn't assign anything yet, so clause.lits contains only one literal
                    Unit::data_.push(clause.lits.front());

                    // abort construction if there is already a conflict ahead
                    if (Unit::data_.conflict_ahead()) {
                        solver_log("initial unit conflict");
                        return;
                    }
                }
        }

        // init heuristic
        heuristic_.init(vars_);

        // initial pure literals are eliminated in preprocessing
    }

    // No copies or moves
    Solver_DPLL(Solver_DPLL const&) = delete;
    Solver_DPLL& operator=(Solver_DPLL const&) = delete;

    /* Solve the formula
     *
     * returns true if SATISFIABLE, false if UNSATISFIABLE
     */
    bool solve() {
        if constexpr (UP)
            if (Unit::data_.conflict_ahead()) // initial unit conflict
                return false;

        while (true) {
            bool conflict = false;

            // forced assignments (if activated)

            if constexpr (UP || PL) {
                bool forced_algorithm_executed = true;

                while (forced_algorithm_executed && !conflict) {
                    forced_algorithm_executed = false;

                    if constexpr (UP) {
                        while (!Unit::data_.empty() && !conflict) {
                            Literal const lit = Unit::data_.top();
                            solver_log("found unit literal ", lit);
                            Unit::data_.pop();

                            if (set(lit, true))
                                conflict = true;

                            forced_algorithm_executed = true;
                            ++Unit::counter_;
                        }
                    }

                    if constexpr (PL) {
                        if (!conflict) {
                            // discard top element until free variable is found
                            while (!Pure::data_.empty() && !vars_[Pure::data_.top().var].free())
                                Pure::data_.pop();

                            if (Literal const l = Pure::data_.top(); l) {
                                solver_log("found pure literal ", l);
                                Pure::data_.pop();

                                if (set(l, true))
                                    conflict = true;

                                forced_algorithm_executed = true;
                                ++Pure::counter_;
                            }
                        }
                    }
                }
            }

            if (!conflict) {
                // branching

                // update heuristic
                heuristic_update();
                // at this point `heuristic_` is up to date

                // formula already satisfied
                if (heuristic_.finished())
                    return true; // -> SATISFIABLE

                Literal const l = heuristic_.top();

                if (set(l, false)) // conflict setting `l`
                    if (set(l.flip(), true)) // also conflict setting `l.flip()`
                        conflict = true;
            }

            if (conflict && backtrack()) // backtracking returns true, i.e. conflict was final
                return false; // —> UNSATISFIABLE
        }
    }


    // get statistics
    Statistics stats() const noexcept {
        Statistics st {};

        st.emplace_back("number of variables", this->var_count());
        st.emplace_back("number of clauses", this->clause_count());
        st.emplace_back("maximal clause width", this->max_clause_width());
        st.emplace_back("branchings", branch_counter());
        st.emplace_back("backtrackings", backtrack_counter());
        st.emplace_back("unsets", unset_counter());

        if constexpr (UP)
            st.emplace_back("unit propagations", unit_prop_counter());

        if constexpr (PL)
            st.emplace_back("pure literal eliminations", pure_lit_elim_counter());

        return st;
    }

    std::size_t branch_counter() const noexcept {
        return branch_counter_;
    }

    std::size_t backtrack_counter() const noexcept {
        return backtrack_counter_;
    }

    std::size_t unset_counter() const noexcept {
        return unset_counter_;
    }

    std::size_t unit_prop_counter() const noexcept {
        if constexpr (UP)
            return Unit::counter_;
        else
            return 0;
    }

    std::size_t pure_lit_elim_counter() const noexcept {
        if constexpr (PL)
            return Pure::counter_;
        else
            return 0;
    }

private:
    Heu heuristic_ {};
    std::unordered_set<Literal, Literal_Hash> heuristic_va_set {}, heuristic_va_unset {};
    std::size_t branch_counter_ {}, backtrack_counter_ {}, unset_counter_ {};

    /* Set a literal
     * sets the literal `lit` and save if it was forced or branching
     *
     * returns true if there is an immediate conflict or a conflict ahead (UP only), otherwise false
     */
    bool set(Literal const lit, bool const forced) {
        solver_assert(lit);
        solver_log("set ", lit, forced ? " (forced)" : " (branching)");

        if (!forced)
            ++branch_counter_;

        Variable<DPLL>& variable {vars_[lit.var]};
        bool const val = lit.val;

        solver_assert(variable.free());

        variable.assignment = &alpha_.emplace(lit, forced);

        bool conflict = false;

        // update `clauses_`

        // clauses where `lit.flip()` will be removed; new conflict possible
        std::vector<Clause<DPLL>*>::iterator nval_cls_it;
        for (nval_cls_it = variable.occ[!val].begin();
                !conflict && nval_cls_it != variable.occ[!val].end(); ++nval_cls_it
            ) {
            if (Clause<DPLL>& clause {**nval_cls_it}; !clause.sat()) { // clause is not yet satisfied
                --clause.active_lits;

                if (clause.empty()) // immediate conflict
                    conflict = true;
                else if constexpr (UP)
                    if (clause.unit())
                        for (Literal const l : clause.lits)
                            if (vars_[l.var].free()) { // if l.var is free
                                if (Unit::data_.push(l).conflict_ahead())
                                    conflict = true; // conflict ahead

                                break; // found unit literal
                            }
            }
        }

        if (conflict) { // handle conflict
            // undo changes
            std::vector<Clause<DPLL>*>::reverse_iterator nval_cls_rit {nval_cls_it};
            for (; nval_cls_rit != variable.occ[!val].rend(); ++nval_cls_rit)
                if (Clause<DPLL>* const clause = *nval_cls_rit; !clause->sat())
                    ++clause->active_lits;

            // unset assignment
            alpha_.pop();
            variable.assignment = nullptr;

            // clear unit_ and pure_
            if constexpr (UP)
                Unit::data_.clear();

            if constexpr (PL)
                Pure::data_.clear();

            return true; // —> conflict
        }

        // from here on no new conflict possible

        // clauses that would be satisfied by the assignment; no new conflict possible
        for (Clause<DPLL>* const clause : variable.occ[val])
            if (!clause->sat()) { // clause is not yet satisfied
                clause->sat_by = lit.var;

                // Loop through all original literals in the clause and decrement `act_occ`
                for (Literal const l : clause->lits) {
                    auto const act_occ = --vars_[l.var].act_occ[l.val];

                    if constexpr (PL)
                        if (vars_[l.var].free() && act_occ == 0
                                && vars_[l.var].act_occ[!l.val] > 0) // `l.flip()` is pure
                            Pure::data_.push(l.flip());
                }
            }


        heuristic_set(lit);

        return false; // —> no conflict
    }

    /* Undo the last assignment
     * unsets the literal `alpha_.top().lit`
     */
    void unset() {
        Literal const lit = alpha_.top().lit;

        solver_log("unset ", lit);

        ++unset_counter_;

        Variable<DPLL>& variable {vars_[lit.var]};
        bool const val = lit.val;

        // unset assignment
        alpha_.pop();
        variable.assignment = nullptr;

        // loop through all clauses that contain `lit`
        for (Clause<DPLL>* const clause : variable.occ[val])
            if (clause->sat_by == lit.var) {
                clause->sat_by = 0;

                // Loop through all original literals in the clause and increment `act_occ`
                for (Literal const l : clause->lits)
                    ++vars_[l.var].act_occ[l.val];
            }

        // loop through all unsatisfied clauses that contain `lit.flip()` and increment `active_lits`
        for (Clause<DPLL>* const clause : variable.occ[!val])
            if (!clause->sat())
                ++clause->active_lits;

        heuristic_unset(lit);
    }

    /* Backtrack, i.e. undo assignments until `alpha_` is empty or a branched assignment was
     * successfully flipped
     *
     * returns true if the conflict is final (-> UNSATISFIABLE), otherwise false
     *
     * precondition: there is a conflict
     *
     * postcondition if backtrack() returns false: there is no conflict
     */
    bool backtrack() {
        ++backtrack_counter_;

        solver_log("backtrack");

        while (!alpha_.empty()) {
            if (alpha_.top().forced) { // this assignment was forced -> unset
                unset();
            } else { // this assignment was not forced -> try to set the flipped literal
                Literal const lit = alpha_.top().lit;
                unset();

                if (!set(lit.flip(), true)) // there was no conflict setting the flipped literal
                    return false; // no conflict
                // else (conflict setting flipped literal) continue loop
            }
        }

        return true; // final conflict (-> UNSATISFIABLE)
    }

    // Heuristic

    /* Remember for heuristic that `lit` was set
     * if `lit` is in `heuristic_va_unset`, erase it
     * if not, insert into `heuristic_va_set`
     */
    void heuristic_set(Literal const lit) {
        if (auto const it = heuristic_va_unset.find(lit); it != heuristic_va_unset.end())
            // lit is in `heuristic_va_unset` -> erase from `heuristic_va_unset`
            heuristic_va_unset.erase(it);
        else // lit is not in `heuristic_va_unset` -> insert into `heuristic_va_set`
            heuristic_va_set.insert(lit);
    }

    /* Remember for heuristic that `lit` was unset
     * if `lit` is in `heuristic_va_set`, erase it
     * if not, insert into `heuristic_va_unset`
     */
    void heuristic_unset(Literal const lit) {
        if (auto const it = heuristic_va_set.find(lit); it != heuristic_va_set.end())
            // lit is in `heuristic_va_set` -> erase from `heuristic_va_set`
            heuristic_va_set.erase(it);
        else // lit is not in `heuristic_va_set` -> insert `into heuristic_va_unset`
            heuristic_va_unset.insert(lit);
    }

    /* Update the heuristic
     * this function should be called after changing variables and before calling `heuristic_.top()`
     * or `heuristic_.finished()`
     * calls `heuristic_.update()`, clears `heuristic_va_set` and `heuristic_va_unset`
     */
    void heuristic_update() {
        heuristic_.update(heuristic_va_set, heuristic_va_unset, vars_);
        heuristic_va_set.clear();
        heuristic_va_unset.clear();
    }
};

#endif // SOLVER_DPLL_HPP
