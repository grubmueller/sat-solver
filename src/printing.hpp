/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PRINTING_HPP
#define PRINTING_HPP

#include "arguments.hpp" // arguments.logging, arguments.quiet

#include <iostream> // std::cout, std::cerr
#include <string>
#include <string_view>


#ifndef NDEBUG // if debug

// Print "c ", all `args` and a newline to stdout if arguments.logging is set
template <typename... Args>
inline void solver_debug_output(Args const&... args) {
    if (arguments.logging)
        (std::cout << 'c' << ' ' << ... << args) << std::endl;
}

// Print 'c' and a newline to stdout if arguments.logging is set
inline void solver_debug_output() {
    if (arguments.logging)
        std::cout << 'c' << std::endl;
}

#endif


// Print an arbitrary character, all `args` and a newline to stdout if arguments.quiet is not set
template <typename... Args>
inline void solver_non_quiet_output_char(char const ch, Args const&... args) {
    if (!arguments.quiet)
        (std::cout << ch << ' ' << ... << args) << std::endl;
}

// Print an arbitrary character and a newline to stdout if arguments.quiet is not set
inline void solver_non_quiet_output_char(char const ch) {
    if (!arguments.quiet)
        std::cout << ch << std::endl;
}

// Print "c ", all `args` and a newline to stdout if arguments.quiet is not set
template <typename... Args>
inline void solver_non_quiet_output(Args const&... args) {
    solver_non_quiet_output_char('c', args...);
}

// Print an error message with all `args` on stderr
template <typename... Args>
inline void solver_error_output(Args const&... args) {
    std::cerr << "*** error :(\n";
    (std::cerr << "**** " << ... << args) << '\n';
}

/* Print an additional error message with all `args` on stderr
 * this should be called after `solver_error_output`
 */
template <typename... Args>
inline void solver_error_output_additional(Args const&... args) {
    (std::cerr << "**** " << ... << args) << '\n';
}


// formatting for printing functions
struct Formatting {
    std::string colour_marker_section_begin, colour_marker_section_end;
    std::size_t section_header_width;
};

// printing functions
void print_section_heading(Formatting const& f, std::string_view heading);
void print_general(Formatting const& f);
void print_no_preprocessor(Formatting const& f);
void print_preprocessor_settings(Formatting const& f);
void print_solver_settings(Formatting const& f);
void print_exit(Formatting const& f, int const ret);


#endif //PRINTING_HPP
