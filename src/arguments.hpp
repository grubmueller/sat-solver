/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARGUMENTS_HPP
#define ARGUMENTS_HPP

#include "config.hpp" // Solver_Type, Heuristic, Scheme, Strategy, Policy

#include <cstddef> // std::size_t


struct arguments {
#ifndef NDEBUG // if debug
    bool logging = false;
#endif
    // general
    bool quiet = false;
    bool ignore_dimacs_header = false;
    // input and output
    char const* input_file = nullptr; // null pointer means reading from stdin
    char const* output_file = nullptr; // null pointer means writing to stdout
    char const* proof_file = nullptr; // null pointer means writing to stdout
    bool proof = false;
    // solver type and branching heuristic
    Solver_Type solver_type = Solver_Type::CDCL;
    Heuristic heuristic = Heuristic::VMTF;
    // configuration
    bool phase_saving = false;
    bool unit_prop = true;
    bool pure_lit_elim = true;
    bool subsumed = true;
    bool bce = true;
    bool holes = true;
    // learning scheme
    Scheme scheme = Scheme::UIP;
    // deletion strategy
    Strategy strategy = Strategy::SIMPLE;
    std::size_t strategy_simple_c = 5000;
    std::size_t strategy_bounded_k = 26;
    std::size_t strategy_bounded_m = 4;
    // restart policy
    Policy policy = Policy::INNER_OUTER;
    std::size_t policy_io_c = 256;
    double policy_io_f = 1.95;
    std::size_t policy_geometric_c = 512;
    double policy_geometric_f = 1.1;
    std::size_t policy_luby_c = 32;
    std::size_t policy_fixed_c = 64;
};

extern struct arguments arguments;


#define ARGERR -1
#define ARGEXIT 1

/* parse command line arguments to struct `arguments`
 *
 * return value:
 *   ARGERR     program should exit with EXIT_FAILURE (error occured)
 *   ARGEXIT    program should exit with EXIT_SUCCESS
 *   0          program should continue (start the solver)
 */
int parse_arguments(int argc, char* argv[]);


#endif // ARGUMENTS_HPP
