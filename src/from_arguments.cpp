/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "arguments.hpp"
#include "strategy_cdcl.hpp"
#include "policy_cdcl.hpp"

// get parameters for strategies and policies from `arguments`

Strategy_Simple Strategy_Simple::from_arguments() {
    return Strategy_Simple{arguments.strategy_simple_c};
}

Strategy_Bounded Strategy_Bounded::from_arguments() {
    return Strategy_Bounded{static_cast<width_t>(arguments.strategy_bounded_k),
                            static_cast<width_t>(arguments.strategy_bounded_m)};
}

Policy_Geometric Policy_Geometric::Fixed_from_arguments() {
    return Policy_Geometric{arguments.policy_fixed_c};
}

Policy_Geometric Policy_Geometric::from_arguments() {
    return Policy_Geometric{arguments.policy_geometric_c, arguments.policy_geometric_f};
}

Policy_Luby Policy_Luby::from_arguments() {
    return Policy_Luby{arguments.policy_luby_c};
}

Policy_Inner_Outer Policy_Inner_Outer::from_arguments() {
    return Policy_Inner_Outer{arguments.policy_io_c, arguments.policy_io_f};
}
