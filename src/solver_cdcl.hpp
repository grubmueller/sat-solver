/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SOLVER_CDCL_HPP
#define SOLVER_CDCL_HPP

#include "solver.hpp"
#include "solver_cdcl_tools.hpp"
#include "proof_logging.hpp"
#include "statistics.hpp"

#include <algorithm> // std::max


template <typename Heu, typename Str, typename Pol, bool PS, Scheme Sch>
class Solver_CDCL : public Solver<CDCL>,
                    private Maybe<Phase_Saving, PS, false>
{
    using Solver<CDCL>::alpha_;
    using Solver<CDCL>::vars_;
    using Solver<CDCL>::clauses_;
    using Phase = Maybe<Phase_Saving, PS, false>;

public:
    /* Construct a CDCL Solver from a formula, strategy and policy
     *
     * precondition: `f` is not empty and does not contain an empty clause
     */
    explicit Solver_CDCL(Formula&& f, Proof_Logger* const pl, Str&& strategy, Pol&& policy) :
        Solver<CDCL>(std::move(f)),
        Phase(this->var_count()),
        units_(this->var_count()),
        strategy_(std::move(strategy)),
        policy_(std::move(policy)),
        logger_(pl)
    {
        // loop through clauses to set watched occurences
        for (Clause<CDCL>& clause : clauses_) {
            vars_[clause.wali[0].var].wocc[clause.wali[0].val].push_back(&clause);

            if (clause.wali[0] == clause.wali[1]) { // unit clause
                units_.push(clause.wali[0], &clause);

                // abort initialisation if there is already a conflict ahead
                if (units_.conflict_ahead()) {
                    solver_log("initial unit conflict");
                    return;
                }
            } else { // no unit clause, also set `clause.wali[1]`
                vars_[clause.wali[1].var].wocc[clause.wali[1].val].push_back(&clause);
            }
        }

        // init heuristic
        heuristic_.init(this->var_count(), clauses_);

        // init strategy
        strategy_.init(this->var_count());
    }

    // No copies or moves
    Solver_CDCL(Solver_CDCL const&) = delete;
    Solver_CDCL& operator=(Solver_CDCL const&) = delete;

    /* Solve the formula
     *
     * returns true if SATISFIABLE, false if UNSATISFIABLE
     */
    bool solve() {
        if (units_.conflict_ahead()) // initial unit conflict
            return false;

        while (true) {
            bool conflict = false;

            while (!units_.empty() && !conflict) {
                // unit propagation
                ++unit_prop_counter_;

                auto const [lit,reason] = units_.top();
                solver_log("found unit literal ", lit);
                units_.pop();

                if (set(lit, reason))
                    conflict = true;
            }

            if (!conflict) {
                // branching
                ++brade_;

                Literal lit = heuristic_.top(vars_);

                if (!lit) // heuristic returns invalid literal -> we are finished
                    return true; // -> SATISFIABLE

                if constexpr (PS) // phase saving, use the value of the last restart
                    lit = Phase::data_.maybe_flip(lit);

                if (set(lit, nullptr))
                    conflict = true;

                ++branch_counter_;
            }

            if (conflict) {
                // there is a conflict ahead
                ++conflict_counter_;

                if (brade_ == 0) { // conflict is final
                    if (logger_) // log empty clause
                        logger_->log(false, {});

                    return false; // -> UNSATISFIABLE
                }

                // conflict analysis
                Clause<CDCL> clause = learn<Sch>(units_.conflict(), brade_, alpha_, vars_);
                units_.clear();

                Clause<CDCL> const* cls;

                if (policy_.report_conflict()) { // restart
                    solver_log("restart");

                    brade_ = 0;
                    backtrack<PS>();

                    // clauses learned at restart are kept permanently
                    cls = &strategy_.clean_push(std::move(clause), true, vars_, logger_);

                    // if `cls` is a unit clause or the assertion level is 0
                    if (cls->wali[0] == cls->wali[1] || !vars_[cls->wali[1].var].free())
                        units_.push(cls->wali[0], cls);
                } else { // non-chronological backtracking
                    brade_ = clause.wali[0] == clause.wali[1] ? 0
                             : vars_[clause.wali[1].var].assignment->brade;
                    backtrack();

                    // may remove old clauses to push this clause
                    cls = &strategy_.clean_push(std::move(clause), false, vars_, logger_);

                    units_.push(cls->wali[0], cls);
                }

                // update heuristic
                heuristic_.update_learned(*cls);
            }
        }
    }

    // get statistics
    Statistics stats() const {
        Statistics stats {};

        stats.emplace_back("Number of variables", this->var_count());
        stats.emplace_back("Number of clauses (original)", this->clause_count());
        stats.emplace_back("Number of clauses (maximal)", this->clause_count() + strategy_.max_clause_count());
        stats.emplace_back("Maximal clause width (original)", this->max_clause_width());
        stats.emplace_back("Maximal clause width (total)", std::max(this->max_clause_width(), strategy_.max_clause_width()));
        stats.emplace_back("Unit propagations", this->unit_prop_counter_);
        stats.emplace_back("Branchings", this->branch_counter_);
        stats.emplace_back("Conflicts", this->conflict_counter_);
        stats.emplace_back("Unsets", this->unset_counter_);
        stats.emplace_back("Restarts", policy_.restart_counter());
        stats.emplace_back("Deleted clauses", strategy_.deleted_counter());

        return stats;
    }

private:
    Units<CDCL> units_;
    brade_t brade_ {};
    Heu heuristic_ {};
    Str strategy_;
    Pol policy_;
    Proof_Logger* logger_;
    std::size_t unit_prop_counter_ {}, branch_counter_ {}, conflict_counter_ {}, unset_counter_ {};

    /* Set a Literal
     * sets the literal `lit` with `reason` as it's reason
     *
     * returns true if there is a conflict ahead, otherwise false
     */
    bool set(Literal const lit, Clause<CDCL> const* const reason) {
        solver_assert(lit);
        solver_log("set ", lit, reason ? " (forced" : " (branching",
                   ", branching depth ", brade_, ')');

        Variable<CDCL>& variable {vars_[lit.var]};
        bool const val = lit.val;

        solver_assert(variable.free());

        variable.assignment = &alpha_.emplace(lit, brade_, reason);

        // update `clauses_`

        // clauses where `lit.flip()` is watched; new conflict possible
        for (auto nval_cls_it = variable.wocc[!val].begin(); nval_cls_it != variable.wocc[!val].end();) {
            Clause<CDCL>& clause {**nval_cls_it};
            // reference to watched literal `lit.flip()` (lit.flip() == wali[1] ? wali[1] : wali[0])
            Literal& lit_wali = clause.wali[lit.flip() == clause.wali[1]];
            // other watched literal (lit.flip() == wali[0] ? wali[1] : wali[0])
            Literal const other_wali = clause.wali[lit.flip() == clause.wali[0]];

            if (!vars_[other_wali.var].free()) { // clause already satisfied
                solver_assert(vars_[other_wali.var].assignment->lit == other_wali);
                ++nval_cls_it;
                continue;
            }

            // find new watched literal `l`
            for (Literal const l : clause.lits)
                if (l != other_wali && (vars_[l.var].free() || vars_[l.var].assignment->lit == l)) {
                    // `l` is unassigned or set (= clause already satisfied)
                    // mark `l` as watched
                    lit_wali = l;
                    vars_[l.var].wocc[l.val].push_back(&clause);
                    nval_cls_it = variable.wocc[!val].erase(nval_cls_it);
                    break; // new watched literal found
                }

            if (lit_wali == lit.flip()) { // no unassigned literal found
                // only `other_wali` is unassigned -> `other_wali` is unit literal
                if (units_.push(other_wali, &clause).conflict_ahead())
                    return true; // conflict ahead

                ++nval_cls_it;
            }
        }

        return false;
    }

    /* Undo the last assignment
     * unsets the literal `alpha_.top().lit`
     */
    void unset() {
        Literal const lit = alpha_.top().lit;

        solver_log("unset ", lit);

        ++unset_counter_;

        // unset assignment
        alpha_.pop();
        vars_[lit.var].assignment = nullptr;

        // update strategy
        strategy_.update_unset(lit.var, vars_);
    }

    /* Backtrack, i.e. undo all assignments of branching depth greater than `brade_`
     *
     * If Store_PS is true, store values in phase saving data structure
     */
    template <bool Store_PS = false>
    void backtrack() {
        static_assert(!Store_PS || PS); // Store_PS => PS

        solver_log("backtrack");

        while (!alpha_.empty() && alpha_.top().brade > brade_) {
            if constexpr (Store_PS) // phase saving, store the value of this literal
                Phase::data_.store(alpha_.top().lit);

            unset();
        }
    }
};

#endif // SOLVER_CDCL_HPP
