/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SOLVE_HPP
#define SOLVE_HPP

#include "config.hpp" // Heuristic, Scheme, Strategy, Policy
#include "formula.hpp"
#include "solution.hpp"
#include "statistics.hpp"

#include <utility> // std::pair, std::move, std::forward

class Proof_Logger;


using Solution_Statistics_Pair = std::pair<std::optional<Solution>, Statistics>;

/* Call a solver
 * constructs an object of type `Solver` with `f` and all `args`,
 * calls the member functions `solve()` and `stats()`,
 * in the case of SATISFIABLE, construct a `Solution`
 *
 * returns solution and statistics
 *
 * precondition: `f` is not empty and does not contain an empty clause
 */
template <typename Solver, typename... Args>
Solution_Statistics_Pair solve_do(std::unique_ptr<Formula> f, Args&&... args) {
    Solver solv {std::move(*f), std::forward<Args>(args)...};

    f.reset(); // `f` was moved and is no longer needed

    if (!solv.solve()) // UNSATISFIABLE
        return {std::nullopt, solv.stats()};

    // SATISFIABLE
    Solution sol {solv.var_count()};

    for (auto const& assignment : solv.alpha())
        sol.set(assignment.lit);

    return {std::move(sol), solv.stats()};
}

/* Solve formula using the DPLL-Solver
 *
 * returns solution and statistics
 *
 * preconditions:
 * * `f` is not empty and does not contain an empty clause
 * * `heuristic` is a valid DPLL-Heuristic
 *
 * defined in solve_dpll.cpp
 */
Solution_Statistics_Pair solve_dpll(std::unique_ptr<Formula>&& f, Heuristic heuristic,
                                    bool unit_prop, bool pure_lit_elim);

/* Solve formula using the CDCL-Solver
 *
 * returns solution and statistics
 *
 * preconditions:
 * * `f` is not empty and does not contain an empty clause
 * * `heuristic` is a valid CDCL-Heuristic
 *
 * defined in solve_cdcl.cpp
 */
Solution_Statistics_Pair solve_cdcl(std::unique_ptr<Formula>&& f, Heuristic heuristic,
                                    Strategy strategy, Policy policy, bool phase_saving,
                                    Scheme scheme, Proof_Logger* proof_logger);

/* Solve formula and print out solution.
 *
 * Does the following:
 * * checks for trivial cases (`f` empty or `f` containing an empty clause)
 * * prints preprocessor settings to standard output
 * * preprocesses `f` using the algorithms set in `arguments`
 * * prints preprocessor statistics to standard output
 * * prints solver settings to standard output
 * * solves `f` using the algorithms set in `arguments`
 * * prints solver statistics to standard output
 * * postprocesses the solution
 * * prints the solution to `os` (and standard output)
 *
 * returns the exit code of the program (10 if SATISFIABLE, 20 if UNSATISFIABLE)
 *
 * defined in solve.cpp
 */
int solve_print(std::unique_ptr<Formula> f, Formatting const& formatting, std::ostream& os,
                Proof_Logger* proof_logger = nullptr);

#endif // SOLVE_HPP
