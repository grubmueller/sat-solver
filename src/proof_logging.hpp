/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROOF_LOGGING_HPP
#define PROOF_LOGGING_HPP

#include "formula.hpp" // Literals

#include <ostream> // std::ostream, std::endl
#include <string_view>


/* Proof_Logger
 * Class for logging a proof
 */
class Proof_Logger {
public:
    // Constructor: `os` must be valid until destruction
    explicit Proof_Logger(std::ostream& os) noexcept :
        os_(os) {}

    // log a clause, if `del` is true write a "d " before
    void log(bool const del, Literals const& clause) {
        if (del)
            os_ << "d ";

        for (Literal const lit : clause)
            os_ << (translation_ ? Literal {lit.val, (*translation_)[lit.var]} : lit) << ' ';

        os_ << '0' << std::endl;
    }

    // log a comment (beginning with "c ")
    void comment(std::string_view const str) {
        os_ << "c " << str << std::endl;
    }

    /* Translate variables, needed for hole elimination
     * `translation` must be valid until destruction or reset
     */
    void apply_translation(std::vector<var_t> const& translation) {
        translation_ = &translation;
    }

private:
    std::ostream& os_;
    std::vector<var_t> const* translation_ {};
};

#endif // PROOF_LOGGING_HPP
