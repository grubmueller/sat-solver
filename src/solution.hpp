/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SOLUTION_HPP
#define SOLUTION_HPP

#include "literal.hpp"

#include <vector>
#include <optional>
#include <iosfwd> // std::ostream


class Solution {
public:
    /* Constructor: set the number of variables
     *
     * Each variable gets the value `false`, use `set` to change the value
     */
    explicit Solution(var_t const var_count) :
        alpha_(var_count + 1) {}

    // number of variables
    var_t var_count() const noexcept {
        return static_cast<var_t>(alpha_.size()) - 1;
    }

    // get the value of a variable
    bool val(var_t const var) const {
        return alpha_[var];
    }

    // get the value of a variable as literal with the variable
    Literal lit(var_t const var) const {
        return {val(var), var};
    }

    // set the value of a variable
    void set(var_t const var, bool const val) {
        alpha_[var] = val;
    }

    // set a literal
    void set(Literal const lit) {
        set(lit.var, lit.val);
    }

private:
    std::vector<bool> alpha_;
};


/* Output a solution
 *
 * prints the v-block excluding the starting 'v' but including the ending '0'
 *
 * defined in dimacs_io.cpp
 */
std::ostream& operator<<(std::ostream& os, Solution const& s);


// print solution (print s- and v-line) (printing.cpp)
struct Formatting;
void print_solution(Formatting const& f, std::ostream& os, std::optional<Solution> const& sol);


#endif // SOLUTION_HPP
