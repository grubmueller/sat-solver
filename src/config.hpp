/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONFIG_HPP
#define CONFIG_HPP


enum class Solver_Type {
    DPLL, CDCL
};

// converts Solver_Type to a String
constexpr char const* type_name(Solver_Type const type) noexcept {
    switch (type) {
    case Solver_Type::DPLL:
        return "DPLL";
    case Solver_Type::CDCL:
        return "CDCL";
    }

    return "";
}

enum class Heuristic {
    SLIS, SLCS, DLIS, DLCS, BOEHM, JW, VSIDS, VMTF
};

// converts Heuristic to a String
constexpr char const* heuristic_name(Heuristic const heuristic) noexcept {
    switch (heuristic) {
    case Heuristic::SLIS:
        return "SLIS";
    case Heuristic::SLCS:
        return "SLCS";
    case Heuristic::DLIS:
        return "DLIS";
    case Heuristic::DLCS:
        return "DLCS";
    case Heuristic::BOEHM:
        return "Böhm";
    case Heuristic::JW:
        return "Jeroslaw-Wang";
    case Heuristic::VSIDS:
        return "VSIDS";
    case Heuristic::VMTF:
        return "VMTF";
    }

    return "";
}

// casts a Heuristic to it's associated Solver_Type
constexpr Solver_Type type_from_heuristic(Heuristic const heuristic) noexcept {
    switch (heuristic) {
    case Heuristic::SLIS:
    case Heuristic::SLCS:
    case Heuristic::DLIS:
    case Heuristic::DLCS:
    case Heuristic::BOEHM:
    case Heuristic::JW:
        return Solver_Type::DPLL;
    case Heuristic::VSIDS:
    case Heuristic::VMTF:
        return Solver_Type::CDCL;
    }

    return static_cast<Solver_Type>(-1);
}

enum class Scheme {
    RELSAT, DECISION, UIP
};

// converts Scheme to a String
constexpr char const* scheme_name(Scheme const scheme) noexcept {
    switch (scheme) {
    case Scheme::RELSAT:
        return "RelSAT";
    case Scheme::DECISION:
        return "Decision";
    case Scheme::UIP:
        return "1UIP";
    }

    return "";
}

enum class Strategy {
    SIMPLE, BOUNDED
};

// converts Strategy to a String
constexpr char const* strategy_name(Strategy const strategy) noexcept {
    switch (strategy) {
    case Strategy::SIMPLE:
        return "Simple";
    case Strategy::BOUNDED:
        return "Bounded";
    }

    return "";
}

enum class Policy {
    FIXED, GEOMETRIC, LUBY, INNER_OUTER, NO
};

// converts Policy to a String
constexpr char const* policy_name(Policy const policy) noexcept {
    switch (policy) {
    case Policy::FIXED:
        return "Fixed";
    case Policy::GEOMETRIC:
        return "Geometric";
    case Policy::LUBY:
        return "Luby";
    case Policy::INNER_OUTER:
        return "Inner/Outer";
    case Policy::NO:
        return "None";
    }

    return "";
}

#endif // CONFIG_HPP
