/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SOLVER_STATISTICS_HPP
#define SOLVER_STATISTICS_HPP

#include <vector>
#include <string>
#include <utility> // std::pair
#include <cstddef> // std::size_t


using Statistics_pair = std::pair<std::string const, std::size_t const>;

using Statistics = std::vector<Statistics_pair>;


// print statistics (printing.cpp)
struct Formatting;
void print_statistics(Formatting const& f, Statistics const& stats, std::string_view const& heading);


#endif // SOLVER_STATISTICS_HPP
