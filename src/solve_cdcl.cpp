/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "solve.hpp"
#include "solver_cdcl.hpp"
#include "heuristics_cdcl.hpp"
#include "strategy_cdcl.hpp"
#include "policy_cdcl.hpp"


namespace { // helper functions to set template parameters
template <typename Heu, typename Str, typename Pol, bool PS, Scheme Sch, typename... Args>
inline auto solve_cdcl_helper(Args&&... args) {
    return solve_do<Solver_CDCL<Heu, Str, Pol, PS, Sch>>(std::forward<Args>(args)...);
}

template <typename Heu, typename Str, typename Pol, bool PS, typename... Args>
inline Solution_Statistics_Pair solve_cdcl_helper_Sch(Scheme const scheme, Args&&... args) {
    switch (scheme) {
    case Scheme::RELSAT:
        return solve_cdcl_helper<Heu, Str, Pol, PS, Scheme::RELSAT>(std::forward<Args>(args)...);
    case Scheme::DECISION:
        return solve_cdcl_helper<Heu, Str, Pol, PS, Scheme::DECISION>(std::forward<Args>(args)...);
    case Scheme::UIP:
        return solve_cdcl_helper<Heu, Str, Pol, PS, Scheme::UIP>(std::forward<Args>(args)...);
    }

    return {};
}

template <typename Heu, typename Str, typename Pol, typename... Args>
inline auto solve_cdcl_helper_PS(bool const phase_saving, Args&&... args) {
    return phase_saving ? solve_cdcl_helper_Sch<Heu, Str, Pol, true>(std::forward<Args>(args)...)
           : solve_cdcl_helper_Sch<Heu, Str, Pol, false>(std::forward<Args>(args)...);
}

template <typename Heu, typename Str, typename... Args>
inline Solution_Statistics_Pair solve_cdcl_helper_Pol(Policy const policy, bool const phase_saving,
                                                      Args&&... args) {
    switch (policy) {
    case Policy::FIXED:
        return solve_cdcl_helper_PS<Heu, Str, Policy_Geometric>
               (phase_saving, std::forward<Args>(args)..., Policy_Geometric::Fixed_from_arguments());
    case Policy::GEOMETRIC:
        return solve_cdcl_helper_PS<Heu, Str, Policy_Geometric>
               (phase_saving, std::forward<Args>(args)..., Policy_Geometric::from_arguments());
    case Policy::LUBY:
        return solve_cdcl_helper_PS<Heu, Str, Policy_Luby>
               (phase_saving, std::forward<Args>(args)..., Policy_Luby::from_arguments());
    case Policy::INNER_OUTER:
        return solve_cdcl_helper_PS<Heu, Str, Policy_Inner_Outer>
               (phase_saving, std::forward<Args>(args)..., Policy_Inner_Outer::from_arguments());
    case Policy::NO: // phase_saving is always false
        return solve_cdcl_helper_Sch<Heu, Str, Policy_No, false>
               (std::forward<Args>(args)..., Policy_No{});
    }

    return {};
}

template <typename Heu, typename... Args>
inline Solution_Statistics_Pair solve_cdcl_helper_Str(Strategy const strategy, Args&&... args) {
    switch (strategy) {
    case Strategy::SIMPLE:
        return solve_cdcl_helper_Pol<Heu, Strategy_Simple>
               (std::forward<Args>(args)..., Strategy_Simple::from_arguments());
    case Strategy::BOUNDED:
        return solve_cdcl_helper_Pol<Heu, Strategy_Bounded>
               (std::forward<Args>(args)..., Strategy_Bounded::from_arguments());
    }

    return {};
}

template <typename... Args>
inline Solution_Statistics_Pair solve_cdcl_helper_Heu(Heuristic const heuristic, Args&&... args) {
    switch (heuristic) {
    case Heuristic::VSIDS:
        return solve_cdcl_helper_Str<Heuristic_VSIDS>(std::forward<Args>(args)...);
    case Heuristic::VMTF:
        return solve_cdcl_helper_Str<Heuristic_VMTF>(std::forward<Args>(args)...);
    default:
        return {};
    }
}
}

/* Solve formula using the CDCL-Solver
 *
 * returns solution and statistics
 *
 * preconditions:
 * * `f` is not empty and does not contain an empty clause
 * * `heuristic` is a valid CDCL-Heuristic
 */
Solution_Statistics_Pair solve_cdcl(std::unique_ptr<Formula>&& f, Heuristic const heuristic,
                                    Strategy const strategy, Policy const policy,
                                    bool const phase_saving, Scheme const scheme,
                                    Proof_Logger* const proof_logger) {
    return solve_cdcl_helper_Heu(heuristic, strategy, policy, phase_saving, scheme,
                                 std::move(f), proof_logger);
}
