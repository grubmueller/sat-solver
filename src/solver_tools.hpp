/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SOLVER_TOOLS_HPP
#define SOLVER_TOOLS_HPP

#include "formula.hpp"
#include "debug.hpp"

#include <stack>
#include <vector>
#include <deque>
#include <utility> // std::move, std::forward
#include <type_traits> // std::std::is_nothrow_constructible
#include <cstddef> // std::size_t

enum class Solver_Type;


// array of bools so one can loop over `bools`: for (bool const b : bools)
inline constexpr bool bools[2] {false, true};


// Forward Declaration of solver tools

template <Solver_Type ST>
struct Assignment;

template <Solver_Type ST>
struct Variable;

template <Solver_Type ST>
struct Clause;

template <Solver_Type ST>
class Units;


// Container for solver tools

template <Solver_Type ST>
class Assignments : public std::stack<Assignment<ST>, std::vector<Assignment<ST>>> {
public:
    explicit Assignments(var_t const var_count) {
        this->c.reserve(var_count);
    }

    auto begin() const noexcept {
        return this->c.rbegin();
    }

    auto cbegin() const noexcept {
        return begin();
    }

    auto end() const noexcept {
        return this->c.rend();
    }

    auto cend() const noexcept {
        return end();
    }
};

template <Solver_Type ST>
using Variables = std::vector<Variable<ST>>;

template <Solver_Type ST>
using Clauses = std::deque<Clause<ST>>;

template <Solver_Type ST>
using Occurences = std::vector<Clause<ST>*>;


// Base classes of solver tools

/* Assignment_Base
 * This is a struct containing the assigned literal.
 * It is inteded as a base class for all Assignment<ST>.
 */
struct Assignment_Base {
    Literal lit;

protected:
    // precondition: `lit` is valid
    constexpr Assignment_Base(Literal const lit) noexcept :
        lit(lit) {}
};

/* Variable_Base
 * This is a struct containing a pointer to the current assignment (null pointer means free).
 * It is intended as a base class for all Variable<ST>.
 */
template <Solver_Type ST>
struct Variable_Base {
    Assignment<ST> const* assignment {};

    // returns true if the variable is free/unassigned, false otherwise
    constexpr bool free() const noexcept {
        return !assignment;
    }

protected:
    Variable_Base() = default;
};

/* Clause_Base
 * This is a struct containing the original literals.
 * It is intended as a base class for all Clause<ST>.
 */
struct Clause_Base {
    Literals lits;

protected:
    // precondition: `origin` is neither empty nor a tautology
    Clause_Base(Formula::Clause&& origin) noexcept :
        lits(std::move(origin).lits()) {}

    /* preconditions:
     * * all literals in `lits` are valid
     * * `lits` is sorted and does not contain duplicates
     * * `lits` is neither empty nor a tautology
     */
    Clause_Base(Literals lits) noexcept :
        lits(std::move(lits)) {}
};

/* Units_Base
 * This is a data structure for unit literals (resp. elements of type `T`), needed for unit
 * propagation.  The data structure can detect if there is a conflict ahead, i.e. if a literal and
 * the flipped literal are both in the data structure (so they are both unit literals).
 * It is intended as a base class for all Units<ST>.
 *
 * `T` should be default constructible.  The default constructor of `T` should construct an invalid
 * element.  The derived class `Units<ST>` should implement the member function `t2lit`, which
 * should convert an element to a literal, and the member function `valid`, which should check if an
 * element is valid.
 */
template <typename T, Solver_Type ST>
class Units_Base {
public:
    // returns true if the data structure is empty, false otherwise
    bool empty() const noexcept {
        return q_.empty();
    }

    // returns true if there is a conflict ahead, false otherwise
    bool conflict_ahead() const noexcept {
        return conflict_ahead_;
    }

    /* Get the next element
     *
     * returns the next element or an invalid element if the data structure is empty
     */
    T top() const noexcept {
        return empty() ? T{} : q_.front();
    }

    /* Remove the top element from the data structure
     *
     * preconditions: the data structure is not empty, there is no conflict ahead
     */
    void pop() noexcept {
        solver_assert(!empty() && !conflict_ahead()); // assert preconditions

        Literal const top_l = derived().t2lit(q_.front());
        in_q_[top_l.val][top_l.var] = false;
        q_.pop_front();
    }

    /* Push a new element to the data structure
     *
     * preconditions: there is no conflict ahead, `t` is valid
     */
    Units<ST>& push(T const t) {
        solver_assert(!conflict_ahead() && derived().valid(t)); // assert preconditions

        Literal const l = derived().t2lit(t);

        if (!in_q_[l.val][l.var]) { // `l` was not in `q_` before
            solver_log("new unit literal ", l);

            q_.push_back(t);
            in_q_[l.val][l.var] = true;

            if (in_q_[!l.val][l.var]) { // `l.flip()` is in `q_` => conflict ahead
                conflict_ahead_ = true;

                solver_log("conflict ahead with conflict variable ", l.var);
            }
        }

        return derived();
    }

    // clear the data structure
    void clear() noexcept {
        conflict_ahead_ = false;

        while (!empty())
            pop();
    }

protected:
    std::deque<T> q_ {};

    Units_Base(var_t const var_count) :
        in_q_{std::vector<bool>(var_count + 1), std::vector<bool>(var_count + 1)} {}

private:
    std::vector<bool> in_q_ [2];
    bool conflict_ahead_ {};

    // convert `this` to object of derived class (CRTP) and return as a reference
    Units<ST>& derived() noexcept {
        return *static_cast<Units<ST>*>(this);
    }

    // convert `this` to object of derived class (CRTP) and return as a constant reference
    Units<ST> const& derived() const noexcept {
        return *static_cast<Units<ST> const*>(this);
    }
};


/* TMP: contains object of type T if and only if ACT is true
 * in case container is unused (ACT == false), remove overhead
 * contains counter_ if and only if ACT and CNT are true
 */
template <typename T, bool ACT, bool CNT = true>
struct Maybe {
    template <typename... Args>
    Maybe(Args&&...) noexcept {} // do nothing, ignore arguments
};

template <typename T, bool CNT>
struct Maybe<T, true, CNT> {
    T data_;

    template <typename... Args>
    Maybe(Args&&... args) noexcept(std::is_nothrow_constructible_v<T, Args...>) :
        data_(std::forward<Args>(args)...) {}
};

template <typename T>
struct Maybe<T, true, true> {
    T data_;
    std::size_t counter_ {};

    template <typename... Args>
    Maybe(Args&&... args) noexcept(std::is_nothrow_constructible_v<T, Args...>) :
        data_(std::forward<Args>(args)...) {}
};


#endif // SOLVER_TOOLS_HPP
