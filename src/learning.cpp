/* Copyright (C) 2020-2021 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "solver_cdcl_tools.hpp"

#include <set>
#include <algorithm> // std::all_of
#include <iterator> // std::next


namespace {
/* Helper function for the RelSAT and Decision schemes
 *
 * returns true if `assignment.lit.flip()` should be added, false otherwise
 */
template <Scheme Sch>
inline bool should_add(Assignment<CDCL> const& assignment, brade_t const brade) {
    static_assert(Sch == Scheme::RELSAT || Sch == Scheme::DECISION);

    if (!assignment.reason) // assignment was a branching assignment -> add
        return true;

    if constexpr (Sch == Scheme::RELSAT)
        if (assignment.brade < brade) // Scheme is RelSAT and assignment has a lesser depth -> add
            return true;

    return false;
}

/* Recursively find and add the correct entries for the RelSAT and Decision schemes
 * These elements constitute the learned clause
 *
 * parameters:
 * * `sv` is the variable we are coming from (conflict variable for the root call)
 * * `clause` is the reason clause we are looking at (reason of one of the `sv` literals)
 * * `brade` is the current branching depth
 * * `learned` is the set of literals we are adding to (-> conflict clause)
 * * `wali1` will be the (only) literal of maximal branching depth, i.e. `brade` (output parameter)
 * * `marked` is the set of clauses already marked, i.e. visited
 * * `vars` is the current variable data structure
 */
template <Scheme Sch>
void add_learned(var_t const sv, Clause<CDCL> const& clause, brade_t const brade,
                 std::set<Literal>& learned, Literal& wali1,
                 std::set<Clause<CDCL> const*>& marked, Variables<CDCL> const& vars) {
    static_assert(Sch == Scheme::RELSAT || Sch == Scheme::DECISION);

    for (Literal const lit : clause.lits)
        if (var_t const var = lit.var; var != sv) { // skip the variable we are just coming from
            if (Assignment<CDCL> const& assignment = *vars[var].assignment;
                    should_add<Sch>(assignment, brade)
               ) { // add `lit`
                solver_assert(lit == assignment.lit.flip());

                learned.insert(lit);

                if (assignment.brade == brade)
                    // found literal of branching depth `brade` (= maximal branching depth)
                    wali1 = lit;
            } else if (marked.insert(assignment.reason).second) {
                // recursive call if `assignment.reason` was not marked
                add_learned<Sch>(var, *assignment.reason, brade, learned, wali1, marked, vars);
            }
        }
}

/* Check if `learned` is asserting
 *
 * if `learned` is asserting, returns the (only) literal of maximal branching depth, i.e. `brade`
 * if `learned` is not asserting, returns an invalid literal
 */
inline Literal asserting(std::set<Literal> const& learned, brade_t const brade,
                         Variables<CDCL> const& vars) {
    Literal ret {};

    for (Literal const l : learned)
        if (vars[l.var].assignment->brade == brade) {
            if (ret)
                return Literal{};
            else
                ret = l;
        }

    return ret;
}

/* Find the first UIP
 * resolves the reason of the next assignment from `alpha` until `learned` is asserting
 *
 * returns the (only) literal of maximal branching depth, i.e. `brade`
 */
Literal find_uip(brade_t const brade, std::set<Literal>& learned, Assignments<CDCL> const& alpha,
                 Variables<CDCL> const& vars) {
    Literal ret;

    for (auto assignment = alpha.begin(); !(ret = asserting(learned, brade, vars)); ++assignment) {
        solver_assert(assignment != alpha.end());

        if (learned.erase(assignment->lit.flip())) {
            // if `assignment->lit.flip()` could be removed from `learned`
            // => this assignment can be resolved
            auto hint = learned.cbegin();

            for (Literal const l : assignment->reason->lits)
                if (l != assignment->lit)
                    hint = std::next(learned.insert(hint, l));
        }
    }

    return ret; // return value from `asserting` -> literal of maximal branching depth
}

/* Conflict Clause Minimisation
 *
 * returns true if `assignment.lit.flip()` can be removed from `learned`, false otherwise
 */
inline bool ccm(Assignment<CDCL> const& assignment, std::set<Literal> const& learned) {
    Clause<CDCL> const* const reason = assignment.reason;
    return reason && std::all_of(reason->lits.begin(), reason->lits.end(),
        [&learned, lit = assignment.lit] (Literal const l) {
            return l == lit || learned.count(l);
        }
    );
}
}

/* Learn a conflict clause according to a learning scheme
 *
 * parameters:
 * * template parameter `Sch` is the learning scheme
 * * `conflict` is the conflict pair of the ahead conflict
 * * `brade` is the current branching depth
 * * `alpha` is the current assignment stack
 * * `vars` is the current variable data structure
 *
 * returns a new asserting conflict clause, which should be added to a strategy
 * the first watched literal is the (only) literal of maximal branching depth, i.e. `brade`
 * the second watched literal is a literal of second largest branching depth, i.e. assertion level
 * if the conflict clause is a unit clause, the second watched literal is the first watched literal
 */
template <Scheme Sch>
Clause<CDCL> learn(Units<CDCL>::Conflict_Pair const conflict, brade_t const brade,
                   Assignments<CDCL> const& alpha, Variables<CDCL> const& vars) {
    std::set<Literal> learned {}; // learned literals
    Literal wali1 {}; // literal of branching depth `brade` (= maximal branching depth)

    if constexpr (Sch == Scheme::UIP) {
        auto hint = learned.cbegin();

        // resolution of the conflict pair
        for (bool const b : bools)
            for (Literal const l : conflict.second[b]->lits)
                if (l.var != conflict.first)
                    hint = std::next(learned.insert(hint, l));

        wali1 = find_uip(brade, learned, alpha, vars);
    } else { // RelSAT or Decision
        std::set<Clause<CDCL> const*> marked;

        for (bool const b : bools)
            add_learned<Sch>(conflict.first, *conflict.second[b], brade, learned, wali1, marked, vars);
    }

    Literal wali2 {}; // literal of second largest branching depth (= assertion level)

    // find `wali2` and minimise conflict clause
    for (auto it = learned.cbegin(); it != learned.cend();) {
        if (Literal const lit = *it; lit != wali1) {
            Assignment<CDCL> const& assignment = *vars[lit.var].assignment;

            if (ccm(assignment, learned)) {
                solver_log("Conflict Clause Minimisation: remove ", lit);
                it = learned.erase(it);
                continue;
            }

            if (!wali2 || assignment.brade > vars[wali2.var].assignment->brade)
                wali2 = lit;
        }

        ++it;
    }

    solver_assert(wali1);
    solver_assert(wali1 != wali2);

    if (!wali2) { // unit clause
        solver_assert(learned.size() == 1);
        wali2 = wali1;
    }

    return {{learned.begin(), learned.end()}, wali1, wali2};
}

template Clause<CDCL> learn<Scheme::RELSAT>(Units<CDCL>::Conflict_Pair, brade_t,
        Assignments<CDCL> const&, Variables<CDCL> const&);
template Clause<CDCL> learn<Scheme::DECISION>(Units<CDCL>::Conflict_Pair, brade_t,
        Assignments<CDCL> const&, Variables<CDCL> const&);
template Clause<CDCL> learn<Scheme::UIP>(Units<CDCL>::Conflict_Pair, brade_t,
        Assignments<CDCL> const&, Variables<CDCL> const&);
